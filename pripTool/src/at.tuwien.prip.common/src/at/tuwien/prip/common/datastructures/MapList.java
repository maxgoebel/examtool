package at.tuwien.prip.common.datastructures;

import java.util.Collection;
import java.util.List;
import java.util.Map;

public interface MapList<A, B> extends Map<A, List<B>> {

    public List<B> putmore(A key, B value);
    public List<B> putmore(A key, Collection<B> value);
    public List<B> getsafe(A key);

}
