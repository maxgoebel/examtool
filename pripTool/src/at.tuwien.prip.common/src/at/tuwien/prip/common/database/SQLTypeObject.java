package at.tuwien.prip.common.database;


public interface SQLTypeObject {

	public SQLType getSQLType();
	public Object getValue();
}
