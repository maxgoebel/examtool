package at.tuwien.prip.common.datastructures;

/**
 * 
 * 
 * KeyValue.java
 *
 *
 *
 * Created: Jun 23, 2009 11:11:29 PM
 *
 * @author mcg <goebel@gmail.com>
 * @version 1.0
 */
public class KeyValue extends Pair<String,String> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1193012518530863485L;

	public KeyValue(String a, String b) {
		super(a, b);
	}

	public String getKey() {
		return getFirst();
	}

	public String getValue() {
		return getSecond();
	}
	
	@Override
	public String toString() {
		return a + ":" + b;
	}

}
