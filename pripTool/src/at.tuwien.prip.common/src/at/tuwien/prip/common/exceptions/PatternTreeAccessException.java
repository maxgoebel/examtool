package at.tuwien.prip.common.exceptions;

public class PatternTreeAccessException extends Exception {


		private static final long serialVersionUID = -2346226577650329847L;

		public PatternTreeAccessException() {
			super();
		}

		public PatternTreeAccessException(String message, Throwable cause) {
			super(message, cause);
		}

		public PatternTreeAccessException(String message) {
			super(message);
		}

		public PatternTreeAccessException(Throwable cause) {
			super(cause);
		}
	}