package at.tuwien.prip.common.utils;

/**
 * Interface of strong reference
 * 
 * @author ceresna
 */
public interface IRef<T> 
    extends IRORef<T> 
{

    public void set(T t);
    
}
