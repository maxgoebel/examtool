package at.tuwien.prip.common.datastructures;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

public interface MapSet<A, B> extends Map<A, Set<B>> {

    public Set<B> putmore(A key, B value);
    public Set<B> putmore(A key, Collection<B> value);
    public Set<B> getsafe(A key);

}
