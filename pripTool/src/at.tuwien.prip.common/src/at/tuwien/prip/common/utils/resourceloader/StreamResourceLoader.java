package at.tuwien.prip.common.utils.resourceloader;

/**
 * StreamResourceLoader.java
 *
 * Finds a resource and opens a stream
 * for reading from the resource
 *
 * Created: Fri Dec 14 20:02:33 2001
 *
 * @author Michal Ceresna
 * @version
 */
class StreamResourceLoader extends CommonResourceLoader {

  public StreamResourceLoader(String path_to_resource,
                              ClassLoader loader) {
    super(path_to_resource, loader);
  }

  /**
   * opens a stream to the given resource
   */
  public Object run() {
    return getLoader().getResourceAsStream(getPathToResource());
  }

}// StreamResourceLoader
