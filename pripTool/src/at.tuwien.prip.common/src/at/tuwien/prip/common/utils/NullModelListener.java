package at.tuwien.prip.common.utils;

public class NullModelListener implements IModelListener {
	protected static NullModelListener soleInstance = new NullModelListener();
	public static NullModelListener getSoleInstance() {
		return soleInstance;
	}

	/*
	 * @see IModelListener#add(ModelEvent)
	 */
	public void add(ModelEvent event) {}

	/*
	 * @see IModelListener#remove(ModelEvent)
	 */
	public void remove(ModelEvent event) {}

	/*
	 * @see IModelListener#remove(ModelEvent)
	 */
	public void update(ModelEvent event) {}

}//NullModelListener
