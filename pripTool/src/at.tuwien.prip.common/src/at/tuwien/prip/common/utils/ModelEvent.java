package at.tuwien.prip.common.utils;

/**
 *
 * A simple event denoting changes in the underlying model.
 *
 * @author max
 *
 */
public class ModelEvent {
	protected Object actedUpon;

	public ModelEvent(Object receiver) {
		actedUpon = receiver;
	}

	public Object receiver() {
		return actedUpon;
	}
}
