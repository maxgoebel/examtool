package at.tuwien.prip.common.exceptions;

public class LoadFailureException extends Exception {


	private static final long serialVersionUID = -2346226577650329847L;

	public LoadFailureException() {
		super();
	}

	public LoadFailureException(String message, Throwable cause) {
		super(message, cause);
	}

	public LoadFailureException(String message) {
		super(message);
	}

	public LoadFailureException(Throwable cause) {
		super(cause);
	}
}