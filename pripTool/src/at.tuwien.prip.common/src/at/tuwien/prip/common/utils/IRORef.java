package at.tuwien.prip.common.utils;

/**
 * Interface of readonly strong reference
 * 
 * @author ceresna
 */
public interface IRORef<T> {

    public T get();
    
}
