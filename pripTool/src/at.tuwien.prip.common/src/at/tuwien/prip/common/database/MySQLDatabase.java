package at.tuwien.prip.common.database;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

import at.tuwien.prip.common.exceptions.DatabaseConnectionException;
import at.tuwien.prip.common.exceptions.PropertyLoadException;
import at.tuwien.prip.common.utils.BasePropertiesLoader;


/**
 * 
 * MySQLDatabase.java
 *
 * MySQL database connector.
 *
 * Created: Aug 29, 2009 7:32:23 PM
 *
 * @author mcg <goebel@gmail.com>
 * @version 1.0
 */
public class MySQLDatabase extends Database {

	protected boolean initialized = false;

	protected boolean connected = false;

	protected Properties mysqlProperties;

	private String host;

	private String database;

	private String user;

	private String password;

	private String port;

	/**
	 * 
	 * Constructor.
	 * 		
	 * Try to initialize database parameters from
	 * properties file.
	 * @throws DatabaseConnectionException 
	 *
	 */
	public MySQLDatabase () throws DatabaseConnectionException 
	{
		init();
	}
	
	/**
	 * 
	 * Constructor.
	 *
	 *@param db_name, the name of the database.
	 * @throws DatabaseConnectionException 
	 */
	public MySQLDatabase(String db_name) throws DatabaseConnectionException 
	{
		this();
		this.database = db_name;
	}

	/**
	 * 
	 * Constructor.
	 * 
	 * @param user
	 * @param password
	 * @param database
	 * @param host
	 * @param port
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public MySQLDatabase(
			String user, String password, String database, String host, String port)
	throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException
	{
		Driver driver=(Driver)Class.forName("com.mysql.jdbc.Driver").newInstance();
		DriverManager.registerDriver( driver );
		if(host==null) host="localhost";
		if(database==null) database="test";
		if(port==null) port="";
		else port=":"+port;
		connection = DriverManager.getConnection(
				"jdbc:mysql://"+host+port+"/"+database+"?user="+user+"&password="+password);
		connection.setAutoCommit( true );
	}

	/**
	 * 
	 * Connect to database.
	 * 
	 * @param db_name, the name of the database
	 * @throws DatabaseConnectionException
	 */
	public Connection connect () throws DatabaseConnectionException {
		if (!initialized) {
			try {
				init();
			} catch (DatabaseConnectionException e) {
				e.printStackTrace();
			}
		}
		if (connected)
			disconnect();

		try {
			connection = DriverManager.getConnection(
					"jdbc:mysql://"+host+port+"/"+database+"?user="+user+"&password="+password);
			connection.setAutoCommit( true );
		} catch (SQLException e) {
			throw new DatabaseConnectionException("Cannot connect to database...");
		}

		connected = true;

		return connection;
	}

	/**
	 * 
	 * Disconnect the database.
	 * 
	 */
	public void disconnect () {
		close();
		connected = false;
	}

	/**
	 * Initialize mysqlProperties.
	 * @throws DatabaseConnectionException
	 *
	 */
	public void init () throws DatabaseConnectionException {
		
		/*
		 * Load project properties
		 */
		try {
			mysqlProperties = BasePropertiesLoader.loadAllProperties();
		} catch (PropertyLoadException e1) {
			e1.printStackTrace();
		}

		/*
		 * Assign class variables for database connection
		 */
		this.user = mysqlProperties.getProperty("user");
		this.password = mysqlProperties.getProperty("password");
		this.database =mysqlProperties.getProperty("database");
		this.host = mysqlProperties.getProperty("host");
		this.port = mysqlProperties.getProperty("port");
		//defaults
		if(host==null) host="localhost";
		if(database==null) database="test";//default
		if(port==null) port="";
		else port=":"+port;

		/*
		 * Try loading and registering driver
		 */
		Driver driver;
		try {
			driver = (Driver)Class.forName("com.mysql.jdbc.Driver").newInstance();
			DriverManager.registerDriver( driver );
		} catch (InstantiationException e) {
			throw new DatabaseConnectionException("Cannot connect to database...");
		} catch (IllegalAccessException e) {
			throw new DatabaseConnectionException("Cannot connect to database...");
		} catch (ClassNotFoundException e) {
			throw new DatabaseConnectionException("Cannot connect to database...");
		} catch (SQLException e) {
			throw new DatabaseConnectionException("Cannot connect to database...");
		}

		initialized = true;
	}

	public static void main(String[] args) {
		
		try 
		{
			MySQLDatabase test = new MySQLDatabase("test");
			test.connect();
		} catch (DatabaseConnectionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.err.println();
	}
}


