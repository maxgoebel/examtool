package at.tuwien.prip.common.datastructures;


public class StringPair extends Pair<String, String> {

    private static final long serialVersionUID = -6129301962872962365L;

    public StringPair(String a, String b) {
        super(a, b);
    }

    public StringPair(String a, char b) {
        this(a, ""+b);
    }

}

