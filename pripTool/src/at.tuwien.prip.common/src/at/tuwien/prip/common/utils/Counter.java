package at.tuwien.prip.common.utils;

/**
 * Counter.java
 *
 * This class implements simple, thread-safe
 * counter
 *
 * Created: Mon Oct 15 17:33:21 2001
 *
 * @author Michal Ceresna
 * @version
 */

public class Counter {

    private long count=0;

    public Counter() {
    }

    public Counter(int start) {
        count = start;
    }

    public synchronized long get() {
        long c = count;
        count++;
        return c;
    }

}// Counter
