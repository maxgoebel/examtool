package at.tuwien.prip.common.datastructures;

import java.util.Collection;
import java.util.Set;


public interface Map2Set<A, B, C> extends Map2<A, B, Set<C>> {

    public Set<C> putmore(A key1, B key2, C value);
    public Set<C> putmore(A key1, B key2, Collection<C> values);
    public Set<C> getsafe(A key1, B key2);

}
