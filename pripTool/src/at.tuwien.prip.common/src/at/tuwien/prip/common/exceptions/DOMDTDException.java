package at.tuwien.prip.common.exceptions;

/**
 * DOMDTDException.java
 *
 *
 * Created: Thu Mar 13 10:07:40 2003
 *
 * @author Michal Ceresna
 * @version 1.0
 */
public class DOMDTDException extends Exception
{

    private static final long serialVersionUID = -4148141813546665766L;

    public DOMDTDException () {
    }

    public DOMDTDException (Throwable parentex) {
        super(parentex);
    }

} // DOMDTDException
