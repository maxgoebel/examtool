package at.tuwien.prip.common.database;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import at.tuwien.prip.common.datastructures.Map2;
import at.tuwien.prip.common.exceptions.DatabaseConnectionException;
import at.tuwien.prip.common.log.ErrorDump;
import au.com.bytecode.opencsv.CSVReader;


/**
 * 
 * DatabaseEngine.java
 *
 *
 * Interface to mysql database.
 *
 * Created: Aug 29, 2009 7:32:47 PM
 *
 * @author mcg <goebel@gmail.com>
 * @version 1.0
 */
public class DatabaseEngine {

	/* the database connection */
	protected Connection conn;

	/* the database */
	protected MySQLDatabase db;

	/**
	 * 
	 * Constructor.
	 *
	 */
	public DatabaseEngine() {
		try {
			db = new MySQLDatabase();
			conn = db.connect();
		} catch (DatabaseConnectionException e) {

		}
	}

	/**
	 * 
	 * Run a sql statement.
	 * 
	 * @param sqlStatement
	 * @return
	 */
	public ResultSet runSqlStatement (String sqlStatement) {
		
		ResultSet result = null;
		try {
			result = db.query(sqlStatement);
		} catch (SQLException e) {
			return null;
		}
		return result;
	}
	
	/**
	 * 
	 * Insert tuples from a csv file into a table.
	 * 
	 * @param fileName
	 * @param tableName
	 * @param createNew
	 * @param cols
	 */
	@SuppressWarnings("unchecked")
	public void insertFromCSV (
			String fileName, String tableName, boolean createNew, int ... cols) 
	{

		StringBuilder sb;


		String[] columnNames = {"id", "name", "alternativeNames", "originalNames", "type", "population",
				"latitude", "longitude", "country", "admin1", "admin2", "admin3"};

		try {
			/* Create table? */
			if (createNew) {
				/* drop possible existing tables and create new */
				Object[] attributes = new String[columnNames.length*2];
				int i=0;
				for (String colName : columnNames) {
					attributes[i] = colName;
					attributes[i+1] = "VARCHAR(200)";
					i+=2;
				}
				db.createTable(tableName, attributes);
			} else {
				/* check for existence and compatibility */
				Statement statement = conn.createStatement();
				statement.execute("SELECT * FROM "+tableName);
				statement.getResultSet();
			}

			/* Read in tuples from cvs */
			List<String[]> tuples = new LinkedList<String[]>();
			CSVReader csvReader;
			try {
				csvReader = new CSVReader(
						new BufferedReader(new InputStreamReader
								(new FileInputStream(fileName),Charset.forName("UTF-8").newDecoder())) ,'\t');
				ErrorDump.debug(this, "Reading in tuples from "+fileName+" (this may take some time)...");
				tuples = csvReader.readAll();
				ErrorDump.debug(this, " [done] with "+tuples.size()+" tuples");
			} catch (FileNotFoundException e1) {
				e1.printStackTrace();
			}

			/* Insert statements into table */
			Statement st = conn.createStatement();

			for (String[] tuple : tuples) {
				if (tuple.length!=columnNames.length) {
					System.err.println();
					continue;
				}
				sb = new StringBuilder("INSERT INTO ").append(tableName).append(" (");
				for (String colName : columnNames) {
					sb.append(colName).append(",");
				}
				sb.setLength(sb.length() - 1);
				sb.append(") VALUES ("); 
				for (String t : tuple) {
					sb.append("'"+t.replaceAll("\\,|\\'", " /")+"'").append(",");
				}
				sb.setLength(sb.length() - 1);
				sb.append(");");
				st.execute(sb.toString());
			}
		} catch (IOException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	/**
	 * 
	 * @param map
	 * @throws SQLException 
	 */
	public void insertFromHashMap2 (
			Map2<String,String,Object> map, 
			String tableName, 
			boolean createNew) 
	throws SQLException 
	{
		
		List<String> cols = map.getAllSecondKeys();
		for (int i=0; i<cols.size(); i++) {
			cols.set(i, cols.get(i).replaceAll("[\\s|\\!|\\-|\\?]", "_"));
		}
		Iterator<String> it = cols.iterator();
		while (it.hasNext() ) {
			String col = it.next();
			if (/*col.length()>3 || */col.equalsIgnoreCase("div"))
				it.remove();
			
		}
		List<String> rows = map.getAllFirstKeys();
		for (int i=0; i<rows.size(); i++) {
			rows.set(i, rows.get(i).replaceAll("[\\s|\\!|\\-|\\?]", "_"));
		}
		
		/* Create table? */
		if (createNew) {
			/* drop possible existing tables and create new */
			Object[] attributes = new String[cols.size()*2];
			int i=0;
			for (String colName : cols) {
				attributes[i] = colName;
				attributes[i+1] = "TEXT";
				i+=2;
			}
			db.createTable(tableName, attributes);
		} else {
			/* check for existence and compatibility */
			Statement statement = conn.createStatement();
			statement.execute("SELECT * FROM "+tableName);
			statement.getResultSet();
		}

		/* Insert statements into table */
		Statement st = conn.createStatement();
		for (String key1 : rows) {
			
			StringBuilder sb = new StringBuilder("INSERT INTO ").append(tableName).append(" (");
			for (String colName : cols) {
				sb.append(colName).append(",");
			}
			sb.setLength(sb.length() - 1);
			sb.append(") VALUES ("); 
			for (String key2 : cols) {
				String t="";
				Object value = map.get(key1, key2);
				if (value!=null) {
					t=value.toString();
				}
				sb.append("'"+t.replaceAll("\\,|\\'", " /")+"'").append(",");
			}
			sb.setLength(sb.length() - 1);
			sb.append(')');
			st.execute(sb.toString());
		}
	}

	/**
	 * 
	 * Driver.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		DatabaseEngine dbEngine = new DatabaseEngine();
		dbEngine.insertFromCSV(args[0], args[1], true, 1 , 2);
	}

}//DatabaseEngine
