package at.tuwien.prip.common.utils;


public interface IModelListener {

	/*
	 *
	 */
	public void add(ModelEvent event);

	/*
	 *
	 */
	public void remove(ModelEvent event);

	/*
	 *
	 */
	public void update(ModelEvent event);
}
