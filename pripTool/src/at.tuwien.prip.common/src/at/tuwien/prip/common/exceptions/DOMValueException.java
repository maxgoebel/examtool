package at.tuwien.prip.common.exceptions;

/**
 * DOMValueException.java
 *
 * thrown when a value is missing or invalid
 *
 * Created: Thu Mar 13 10:07:40 2003
 *
 * @author Michal Ceresna
 * @version 1.0
 */
public class DOMValueException
    extends Exception
{

    private static final long serialVersionUID = 7812790392297375610L;

    public DOMValueException () {
    }

    public DOMValueException (String message) {
        super(message);
    }

    public DOMValueException (Throwable parentex) {
        super(parentex);
    }

} // DOMValueException
