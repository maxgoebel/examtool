package at.tuwien.prip.common.utils;

/**
 * Strong reference
 * 
 * @author ceresna
 */
public class Ref<T> 
    implements IRef<T> 
{

    private T t;
    
    public Ref() {}
    
    public Ref(T t) {
        this.t = t;
    }

    public void set(T t) {
        this.t = t;
    }
    
    public T get() {
        return t;
    }

}
