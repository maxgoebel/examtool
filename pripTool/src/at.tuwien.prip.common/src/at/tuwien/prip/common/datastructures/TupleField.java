package at.tuwien.prip.common.datastructures;

import java.util.ArrayList;

import at.tuwien.prip.common.utils.Counter;

/**
 * 
 * Data collection to hold two-dimensional
 * relational data sets. Base container is
 * a <a>Tuple</a>.
 * 
 * @author max
 *
 * @param <T>
 */
public class TupleField<T> {

	/* */
	private ArrayList<Tuple<T>> rows;
	
	/* */
	private final BidiMap<String,Integer> index;
	
	/* */
	private final static Counter colCounter = new Counter();
	
	/**
	 * 
	 * Constructor.
	 * 
	 * @param index
	 */
	public TupleField(String[] index) {
		this.index = new BidiMap<String, Integer>();
		for (String i : index) {
			this.index.put(i, (int) colCounter.get());
		}
		rows = new ArrayList<Tuple<T>>();
	}
	
	/**
	 * 
	 * Add a row.
	 * 
	 * @param index
	 * @param row
	 * @param defVal
	 */
	public void addRow (
			String[] index, 
			T[] row, 
			T defVal) 
	{
		assert index.length==row.length;
		
		Tuple<T> tuple = new Tuple<T>(index,row);
		
		/* find and add new column headers */
		if (index.length!=this.index.size()) {
			
			if (index.length>this.index.size()) {
				for (String s : index) {
					if (!this.index.keySetA().contains(s)) {
						addColumn(s, defVal);
					}
				}
			} else {
				for (String key : this.index.keySetA()) {
					if (!tuple.index.containsKey(key)) {
						tuple.addColumn(key, defVal);
					}
				}
			}
		}

		tuple.arrangeBy(this.index);
		rows.add(tuple);
	}
	
	
	public T queryData(String colName, T colVal, String resultCol)
	{
		for (Tuple<T> row : rows)
		{
			T tmp = row.get(colName);
			if (tmp instanceof String)
			{
				String sTmp = (String) tmp;
				String sCol = (String) colVal;
				if (sCol.endsWith(sTmp))
				{
					return row.get(resultCol);
				}
			}
			else if (tmp.equals(colVal))
			{
				return row.get(resultCol);
			}
		}
		return null;
	}
	/**
	 * 
	 *  Add a column and fill new column of existing rows 
	 *  with a default value.
	 *  
	 * @param colName
	 * @param defVal
	 */
	public void addColumn (String colName, T defVal) {
		if (index.containsKey(colName)) return; //already exists
		
		int colIndex; 	
		do {
			colIndex = (int) colCounter.get();
		} while (	index.values().contains(colIndex));
		
		index.put(colName, colIndex);
		
		for (Tuple<T> row : rows) {
			row.addColumn(colName, defVal);
		}
	}
	
	public Tuple<T> getRow (int i) {
		return rows.get(i);
	}
	
//	/**
//	 * 
//	 * Write this tuple field to a comma-separated file.
//	 * 
//	 * @param fileName
//	 * @throws IOException
//	 */
//	public void toCSV (String fileName, String encoding) 
//	throws IOException {
//
//		/* the writer */
//	     CSVWriter writer = 
//	    	 new CSVWriter(	
//	    		 new OutputStreamWriter(
//	    				 new FileOutputStream(fileName), 
//	    		 encoding), 
//	    	 '\t');
//	     
//	     /* write headers */
//	     String[] entries = new String[rows.get(0).index.keySetA().size()];
//	     for (int i=0; i<entries.length; i++) {
//    		 entries[i] = index.reverseGet(i);
//    	 }
//	     writer.writeNext(entries);
//		
//	     /* write data */
//	     for (Tuple<T> row : rows) {
//	    	 entries = new String[row.array.size()];
//	    	 for (int i=0; i<row.array.size(); i++) {
//	    		 entries[i] = row.get(i).toString();
//	    	 }
//	    	 writer.writeNext(entries);
//	     }
//		writer.close();	
//	}
	
	public int getRowCount () {
		return rows.size();
	}
	
	public int getColumnCount () {
		if (getRowCount()>0) {
			return rows.get(0).length();
		} else {
			return 0;
		}
	}
	
	public int getMaxIndex () {
		int result = 0;
		for (Integer i : index.m2.keySet()) {
			if (i>result) {
				result = i;
			}
		}
		return result;
	}
	
}//TupleField
