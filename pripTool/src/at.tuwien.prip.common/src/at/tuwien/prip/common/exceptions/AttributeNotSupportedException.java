package at.tuwien.prip.common.exceptions;


/**
 * 
 * AttributeNotSupportedException.java
 *
 *
 *
 * Created: Apr 27, 2009 2:20:00 AM
 *
 * @author mcg <goebel@gmail.com>
 * @version 1.0
 */
public class AttributeNotSupportedException extends Exception{

    /**
	 *
	 */
	private static final long serialVersionUID = -2981085776873037380L;

	public AttributeNotSupportedException() {
        super();
    }

    public AttributeNotSupportedException(String message, Throwable cause) {
        super(message, cause);
    }

    public AttributeNotSupportedException(String message) {
        super(message);
    }

    public AttributeNotSupportedException(Throwable cause) {
        super(cause);
    }

}
