package at.tuwien.prip.common.datastructures;

import java.util.Collection;
import java.util.List;


public interface Map2List<A, B, C> extends Map2<A, B, List<C>> {

    public List<C> putmore(A key1, B key2, C value);
    public List<C> putmore(A key1, B key2, Collection<C> values);
    public List<C> getsafe(A key1, B key2);

}
