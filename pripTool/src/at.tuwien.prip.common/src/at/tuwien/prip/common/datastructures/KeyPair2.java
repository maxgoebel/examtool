package at.tuwien.prip.common.datastructures;
interface KeyPair2<K1,K2> {
    K1 getKey1();
    K2 getKey2();

    boolean equals(Object o);
    int hashCode();
}