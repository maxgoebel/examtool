package views.projectNavigator;

import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.ui.navigator.CommonNavigator;

/**
 * ProjectNavigatorView.java
 *
 *
 *
 * @author max <mcgoebel@gmail.com>
 * @date Jan 25, 2011
 */
public class ProjectNavigatorView extends CommonNavigator {

	public ProjectNavigatorView() {
		super();
	}
	
	@Override
	protected Object getInitialInput() {
		return ResourcesPlugin.getWorkspace().getRoot();
	}
}