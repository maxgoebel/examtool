package views.projectNavigator;

import org.eclipse.core.internal.resources.File;
import org.eclipse.core.internal.resources.Folder;
import org.eclipse.core.internal.resources.Project;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.swt.graphics.Image;
import org.eclipse.ui.navigator.IDescriptionProvider;

import editor.BaseEmfEditor;

/**
 * Provides a label and icon for objects of type {@link PropertiesTreeData}.
 * 
 * @since 3.2
 *
 */
public class PropertiesLabelProvider extends AdapterFactoryLabelProvider
implements ILabelProvider, IDescriptionProvider 
{  
	
	/**
	 * Constructor.
	 */
	public PropertiesLabelProvider() {
		super(BaseEmfEditor.createAdapterFactory());
	}
	
	public Image getImage(Object element) {
//		if (element instanceof PropertiesTreeData)
//			return PlatformUI.getWorkbench().getSharedImages().getImage(
//					ISharedImages.IMG_OBJS_INFO_TSK); 
		return super.getImage(element);
	}

	public String getText(Object element) {
		if (element instanceof Project) {
			Project proj = (Project) element;
			return proj.getName();
		} else if (element instanceof Folder) {
			Folder folder = (Folder) element;
			return folder.getName();
		} else if (element instanceof File) {
			File file = (File) element;
			return file.getName();
		}
		return super.getText(element);
	}

	public String getDescription(Object anElement) {
//		if (anElement instanceof PropertiesTreeData) {
//			PropertiesTreeData data = (PropertiesTreeData) anElement;
//			return "Property: " + data.getName(); //$NON-NLS-1$
//		}
		return "empty description";
	}
  
}