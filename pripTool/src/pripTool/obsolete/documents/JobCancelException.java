package views.documents;


/**
 *
 * @author ceresna
 */
public class JobCancelException
    extends Exception
{

    private static final long serialVersionUID = -2909834858467659894L;

    public JobCancelException() {
    }

}
