package views.documents.provider;

import java.util.Collection;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.util.EList;
import org.weblearn.model.core.document.benchmark.ExtractionAnnotation;
import org.weblearn.model.core.document.benchmark.provider.ExtractionAnnotationItemProvider;

/**
 * ExtractionAnnotationItemProvider2.java
 * 
 *
 *
 * @author mcg <mcgoebel@gmail.com>
 * Nov 19, 2011
 */
public class ExtractionAnnotationItemProvider2 extends ExtractionAnnotationItemProvider 
{

	public ExtractionAnnotationItemProvider2(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}
	
	@Override
	public String getText(Object object) {
		// TODO Auto-generated method stub
		return super.getText(object);
	}
	
	@Override
	public Object getImage(Object object) {
		// TODO Auto-generated method stub
		return super.getImage(object);
	}

	@Override
	public Collection<?> getChildren(Object object) {
		EList<?> pages = ((ExtractionAnnotation)object).getPages();
		return pages;
	}
	
	@Override
	public boolean hasChildren(Object object) {
		EList<?> pages = ((ExtractionAnnotation)object).getPages();
		return pages==null || pages.size()==0 ? false : true;
	}
}
