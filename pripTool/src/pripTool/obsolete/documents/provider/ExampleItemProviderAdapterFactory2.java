package views.documents.provider;

import org.weblearn.model.core.learn.example.provider.ExampleItemProviderAdapterFactory;

/**
 * 
 * Delegates to custom item providers for emf folder EXAMPLE.
 * 
 * @author mcg <goebel@gmail.com>
 *
 */
public class ExampleItemProviderAdapterFactory2 
extends ExampleItemProviderAdapterFactory {

    public ExampleItemProviderAdapterFactory2() {
    }
 

//    @Override
//    public Adapter createFragmentExampleAdapter() {
//        if (fragmentExampleItemProvider == null) {
//        	fragmentExampleItemProvider = new FragmentExampleItemProvider2(this);
//        }
//
//        return fragmentExampleItemProvider;
//    }
//    
//    @Override
//    public Adapter createSelectionExampleAdapter() {
//    	if (selectionExampleItemProvider == null) {
//    		selectionExampleItemProvider = new SelectionExampleItemProvider2(this);
//        }
//
//        return selectionExampleItemProvider;
//    }
//    
//    @Override
//    public Adapter createTableExampleAdapter() {
//    	if (tableExampleItemProvider == null) {
//    		tableExampleItemProvider = new TableExampleItemProvider2(this);
//        }
//
//        return tableExampleItemProvider;
//    }

}
