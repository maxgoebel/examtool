package views.documents.provider;

import i18n.mt;

import java.util.Collection;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationWrapper;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemProviderAdapter;
import org.weblearn.model.core.document.DocumentClusterGroup;
import org.weblearn.model.core.document.DocumentCollection;
import org.weblearn.model.core.document.DocumentFactory;
import org.weblearn.model.core.document.DocumentPackage;

import annotationide.LearnUIImages;



public class UnclassifiedDocumentEntriesItemProvider
    extends ItemProviderAdapter
    implements
        IEditingDomainItemProvider,
        IStructuredItemContentProvider,
        ITreeItemContentProvider,
        IItemLabelProvider,
        IItemPropertySource
{

    private final DocumentClusterGroup docCluster;

    public UnclassifiedDocumentEntriesItemProvider(
    		AdapterFactory adapterFactory, DocumentClusterGroup docCol)
    {
        super(adapterFactory);
        this.docCluster = docCol;
        docCol.eAdapters().add(this);
    }

    @Override
    public Collection getChildren(Object object) {
        return docCluster.getUnclassified();
    }

    @Override
    public Object getParent(Object object) {
        return super.getParent(docCluster);
    }

    @Override
    protected Collection getChildrenFeatures(Object object) {
        if (childrenFeatures==null) {
            super.getChildrenFeatures(object);
            childrenFeatures.remove(
            		DocumentPackage.eINSTANCE.getDocumentClusterGroup_Unclassified());
        }
        return childrenFeatures;
    }

    @Override
    public String getText(Object object) {
        return mt.lb_Unclassified_Documents;
    }

    @Override
    public Object getImage(Object object) {
        return
            LearnUIImages.
            getImageDescriptor("icons/full/obj16/unclassifiedDocuments");
    }

    @Override
    public void notifyChanged(Notification notification) {
        switch (notification.getFeatureID(DocumentCollection.class)) {
            case DocumentPackage.DOCUMENT_CLUSTER_GROUP__UNCLASSIFIED:
            fireNotifyChanged(new NotificationWrapper(this, notification));
            return;
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    protected void collectNewChildDescriptors(Collection newChildDescriptors, Object object) {
        super.collectNewChildDescriptors(newChildDescriptors, object);
        newChildDescriptors.add(createChildParameter(
            DocumentPackage.eINSTANCE.getDocumentClusterGroup_Unclassified(),
            DocumentFactory.eINSTANCE.createDocumentEntry())); //BenchmarkDocument?
    }

}
