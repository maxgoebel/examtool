package views.documents.provider;

import java.util.Collection;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationWrapper;
import org.weblearn.model.core.document.Benchmark;
import org.weblearn.model.core.document.DocumentCollection;
import org.weblearn.model.core.document.DocumentFactory;
import org.weblearn.model.core.document.DocumentPackage;
import org.weblearn.model.core.document.provider.BenchmarkItemProvider;

import annotationide.LearnUIImages;

public class BenchmarkItemProvider2  extends BenchmarkItemProvider
{
//
//	private final Benchmark benchmark;

	/**
	 * Constructor 
	 * 
	 * @param adapterFactory
	 * @param benchmark
	 * @param docCluster
	 */
	public BenchmarkItemProvider2(AdapterFactory adapterFactory) {
		super(adapterFactory);
//		this.benchmark = benchmark;
//		docCluster.eAdapters().add(this); //necessary for notification
	}

	@Override
	public Collection getChildren(Object arg0) {
		return super.getChildren(arg0);
	}

	@Override
	public Object getImage(Object object) {
		return LearnUIImages.getImageDescriptor("icons/full/obj16/documentGroup");
	}

	@Override
	public String getText(Object obj) {
		if (obj instanceof Benchmark) {
			Benchmark benchmark = (Benchmark) obj;
			return "Benchmark: " + benchmark.getName();
		}
		return super.getText(obj);
	}
	
    @Override
    public void notifyChanged(Notification notification) {
        switch (notification.getFeatureID(DocumentCollection.class)) {
            case DocumentPackage.DOCUMENT_CLUSTER_GROUP__BENCHMARKS:
            fireNotifyChanged(new NotificationWrapper(this, notification));
            return;
        }
    }
    
    @SuppressWarnings("unchecked")
    @Override
    protected void collectNewChildDescriptors(Collection newChildDescriptors, Object object) {
        super.collectNewChildDescriptors(newChildDescriptors, object);
        newChildDescriptors.add(createChildParameter(
            DocumentPackage.eINSTANCE.getDocumentClusterGroup_Benchmarks(),
            DocumentFactory.eINSTANCE.createBenchmark())); //BenchmarkDocument?
    }

}
