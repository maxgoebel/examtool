package views.documents.provider;

import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.weblearn.model.core.document.selection.LabelSelection;
import org.weblearn.model.core.document.selection.Selection;
import org.weblearn.model.core.document.selection.provider.LabelSelectionItemProvider;

public class LabelSelectionItemProvider2 extends LabelSelectionItemProvider 
{

	public LabelSelectionItemProvider2(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}
	
	@Override
	public String getText(Object object) {
		return "Label Selection";
	}
	
	@Override
	public Object getImage(Object object) {
		// TODO Auto-generated method stub
		return super.getImage(object);
	}

	@Override
	public Collection<?> getChildren(Object object) {
		List children = new ArrayList();
		LabelSelection labSel = (LabelSelection) object;
		children.add(labSel.getBounds());
		children.add(labSel.getLabel());
		return children;
	}
	
	@Override
	public boolean hasChildren(Object object) {
		Rectangle selection = ((LabelSelection)object).getBounds();
		return selection!=null;
	}
}
