package views.documents.provider;

import java.util.Collection;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.util.EList;
import org.weblearn.model.core.document.benchmark.AnnotationPage;
import org.weblearn.model.core.document.benchmark.provider.AnnotationPageItemProvider;

/**
 * AnnotationPageItemProvider2.java
 * 
 *
 *
 * @author mcg <mcgoebel@gmail.com>
 * Nov 19, 2011
 */
public class AnnotationPageItemProvider2 extends AnnotationPageItemProvider
{
	public AnnotationPageItemProvider2(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}
	
	@Override
	public String getText(Object object) {
		// TODO Auto-generated method stub
		return super.getText(object);
	}
	
	@Override
	public Object getImage(Object object) {
		// TODO Auto-generated method stub
		return super.getImage(object);
	}
	
	@Override
	public Collection<?> getChildren(Object object) {
		return ((AnnotationPage)object).getItems(); 
	}
	
	@Override
	public boolean hasChildren(Object object) {
		EList<?> items = ((AnnotationPage)object).getItems();
		return items==null || items.size()==0 ? false : true;
	}
}
