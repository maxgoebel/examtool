package views.documents.provider;

import org.eclipse.emf.common.notify.Adapter;
import org.weblearn.model.core.document.provider.DocumentItemProviderAdapterFactory;

/**
 * 
 * DocumentItemProviderAdapterFactory2.java
 *
 *
 * Delegates view and content factories for the examples view.
 *
 *
 * Created: May 8, 2009 10:23:05 PM
 *
 * @author Michal Ceresna
 * @author mcg <goebel@gmail.com>
 * @version 1.0
 */
public class DocumentItemProviderAdapterFactory2
extends DocumentItemProviderAdapterFactory
{

    @Override
    public Adapter createWrapperDocumentAdapter() {
        if (wrapperDocumentItemProvider==null) {
        	wrapperDocumentItemProvider = new WrapperDocumentItemProvider2(this);
        }
        return wrapperDocumentItemProvider;
    }
	
	@Override
	public Adapter createDocumentEntryAdapter() {
		if (documentEntryItemProvider==null) {
			documentEntryItemProvider = new DocumentEntryItemProvider2(this);
		}
		return documentEntryItemProvider;
	}
	
	@Override
	public Adapter createBenchmarkDocumentAdapter() {
		if (benchmarkDocumentItemProvider==null) {
			benchmarkDocumentItemProvider = new BenchmarkDocumentItemProvider2(this);
		}
		return benchmarkDocumentItemProvider;
	}

    @Override
    public Adapter createDocumentCollectionAdapter() {
        if (documentCollectionItemProvider==null) {
        	documentCollectionItemProvider = new DocumentCollectionItemProvider2(this);
        }

        return documentCollectionItemProvider;
    }

    @Override
    public Adapter createDocumentClusterGroupAdapter() {
        if (documentClusterGroupItemProvider==null) {
        	documentClusterGroupItemProvider = new DocumentGroupItemProvider2(this);
        }

        return documentClusterGroupItemProvider;
    }

    @Override
    public Adapter createBenchmarkAdapter() {
    	if(benchmarkItemProvider==null) {
    		benchmarkItemProvider = new BenchmarkItemProvider2(this);
    	}
    	
    	return benchmarkItemProvider;
    }
    
    @Override
    public Object adapt(Object object, Object type) {
    	// TODO Auto-generated method stub
    	return super.adapt(object, type);
    }
    
}//DocumentItemProviderAdapterFactory2
