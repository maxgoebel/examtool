package views.documents.provider;

import org.eclipse.emf.common.notify.Adapter;
import org.weblearn.model.core.document.benchmark.provider.AnnotationsItemProviderAdapterFactory;

/**
 * AnnotationItemProviderAdapter2.java
 * 
 *
 *
 * @author mcg <mcgoebel@gmail.com>
 * Nov 19, 2011
 */
public class AnnotationsItemProviderAdapterFactory2 extends AnnotationsItemProviderAdapterFactory
{
	public AnnotationsItemProviderAdapterFactory2() {
	}
	
	@Override
	public Adapter createAnnotationPageAdapter() {
		if (annotationPageItemProvider == null) {
			annotationPageItemProvider = new AnnotationPageItemProvider2(this);
		}
		
		return annotationPageItemProvider;
	}
	
	@Override
	public Adapter createExtractionAnnotationAdapter() {
		if (extractionAnnotationItemProvider == null) {
			extractionAnnotationItemProvider = new ExtractionAnnotationItemProvider2(this);
		}

		return extractionAnnotationItemProvider;
	}

	@Override
	public Adapter createSelectionAnnotationAdapter() {
		if (selectionAnnotationItemProvider == null) {
			selectionAnnotationItemProvider = new SelectionAnnotationItemProvider2(this);
		}

		return selectionAnnotationItemProvider;
	}

	@Override
	public Adapter createLabelAnnotationAdapter() {
		if (labelAnnotationItemProvider == null) {
			labelAnnotationItemProvider = new LabelAnnotationItemProvider2(this);
		}

		return labelAnnotationItemProvider;
	}
	    
}
