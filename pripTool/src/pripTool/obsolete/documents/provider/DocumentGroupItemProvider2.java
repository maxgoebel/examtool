package views.documents.provider;

import i18n.mt;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.weblearn.model.core.document.Benchmark;
import org.weblearn.model.core.document.DocumentClusterGroup;
import org.weblearn.model.core.document.DocumentPackage;
import org.weblearn.model.core.document.provider.DocumentClusterGroupItemProvider;

import annotationide.LearnUIImages;

/**
 * DocumentGroupItemProvider2.java
 *
 *
 *
 * Created: May 8, 2009 10:23:51 PM
 *
 * @author Michal Ceresna
 * @author mcg <goebel@gmail.com>
 * @version 1.0
 */
public class DocumentGroupItemProvider2
extends DocumentClusterGroupItemProvider
{

	public DocumentGroupItemProvider2(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}


	@SuppressWarnings("unchecked")
	@Override
	public Collection getChildren(Object object) {
		Collection children = super.getChildren(object);
		DocumentClusterGroup docCluster = (DocumentClusterGroup) object;
		if (!docCluster.getUnclassified().isEmpty()) {
			children.add(new UnclassifiedDocumentEntriesItemProvider(adapterFactory, docCluster));
		}

		List<Benchmark> benchs = docCluster.getBenchmarks();
		for (Benchmark b : benchs) {
			if (!docCluster.getBenchmarks().isEmpty()) {
				children.add(new BenchmarksRootItemProvider(adapterFactory, docCluster));
			}
		}
		return children;
	}

	@Override
	public Collection getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.remove(DocumentPackage.eINSTANCE.getDocumentClusterGroup_Unclassified());
		}
		return childrenFeatures;
	}

	@Override
	public Object getImage(Object object) {
		return LearnUIImages.getImageDescriptor("icons/full/obj16/documentGroup");
	}

	@Override
	public String getText(Object obj) {
		return mt.lb_Document_Group;
	}

}
