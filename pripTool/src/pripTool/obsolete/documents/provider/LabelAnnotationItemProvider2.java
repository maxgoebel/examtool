package views.documents.provider;

import java.util.Collection;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.weblearn.model.core.document.benchmark.LabelAnnotation;
import org.weblearn.model.core.document.benchmark.provider.LabelAnnotationItemProvider;

/**
 * LabelAnnotationItemProvider2.java
 * 
 *
 *
 * @author mcg <mcgoebel@gmail.com>
 * Nov 19, 2011
 */
public class LabelAnnotationItemProvider2 extends LabelAnnotationItemProvider 
{

	public LabelAnnotationItemProvider2(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}
	
	@Override
	public String getText(Object object) {
		return super.getText(object);
	}
	
	@Override
	public Object getImage(Object object) {
		return super.getImage(object);
	}

	@Override
	public Collection<?> getChildren(Object object) {
		if (object instanceof LabelAnnotation) {
			return ((LabelAnnotation)object).getPages();
		}
		return super.getChildren(object);
	}
	
	@Override
	public boolean hasChildren(Object object) {
		if (object instanceof LabelAnnotation) {
			return ((LabelAnnotation)object).getPages().size()>0;
		}
		return super.hasChildren(object);
	}
}
