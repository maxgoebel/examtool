package views.documents.provider;

import org.eclipse.emf.common.notify.Adapter;
import org.weblearn.model.core.document.selection.provider.SelectionItemProviderAdapterFactory;

public class SelectionItemProviderAdapterFactory2 extends SelectionItemProviderAdapterFactory {

	
	@Override
	public Adapter createLabelSelectionAdapter() {
		if(labelSelectionItemProvider==null) {
			labelSelectionItemProvider = new LabelSelectionItemProvider2(this);
    	}
    	return labelSelectionItemProvider;
	}
	
}
