package views.documents.provider;

import i18n.mt;

import java.util.Collection;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.weblearn.model.core.document.provider.DocumentCollectionItemProvider;

import annotationide.LearnUIImages;


/**
 * 
 * DocumentCollectionItemProvider2.java
 *
 *
 *
 * Created: May 8, 2009 10:24:56 PM
 *
 * @author Michal Ceresna
 * @author mcg <goebel@gmail.com>
 * @version 1.0
 */
public class DocumentCollectionItemProvider2 extends DocumentCollectionItemProvider 
{

	public DocumentCollectionItemProvider2(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	@Override
	public Collection getChildren(Object arg0) {
		return super.getChildren(arg0);
	}
	
	@Override
	public Object getImage(Object object) {
	return LearnUIImages.getImageDescriptor("icons/full/obj16/documentGroup");
	}

	@Override
	public String getText(Object obj) {
	return mt.lb_Document_Group;
	}

}//DocumentCollectionItemProvider2
