package views.documents;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.edit.provider.ComposedAdapterFactory;
import org.eclipse.emf.edit.provider.ReflectiveItemProviderAdapterFactory;
import org.eclipse.emf.edit.provider.resource.ResourceItemProviderAdapterFactory;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryContentProvider;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IMenuListener;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.ui.IActionBars;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.part.Page;
import org.weblearn.model.core.document.Benchmark;
import org.weblearn.model.core.document.DocumentCollectionList;
import org.weblearn.model.core.document.DocumentEntry;

import views.documents.actions.AddFromFileAction;
import views.documents.actions.LoadAction;
import views.documents.actions.RemoveAction;
import views.documents.actions.RemoveDocumentAction;
import views.documents.actions.SaveAction;
import views.documents.provider.AnnotationsItemProviderAdapterFactory2;
import views.documents.provider.DocumentItemProviderAdapterFactory2;
import views.documents.provider.ExampleItemProviderAdapterFactory2;
import views.documents.provider.SelectionItemProviderAdapterFactory2;
import annotationide.LearnUIImages;
import container.IDocumentCollectionContainer;

/**
 * 
 * DocumentsPage.java
 *
 * 
 * The page responsible for displaying the document view
 *
 * Created: May 8, 2009 10:56:52 PM
 *
 * @author Michal Ceresna
 * @author mcg <goebel@gmail.com>
 * @version 1.0
 */
public class DocumentsPage extends Page 
{

    public final IWorkbenchPart boundPart;
    public IDocumentCollectionContainer docCol = null;

    public TreeViewer viewer = null;

    private ISelection lastViewerSel;

    protected Action loadAction;
    protected Action saveAction;
    protected Action removeAction;
    protected Action addURLAction;
    protected Action addFromFileAction;
    protected Action addFromBrowserAction;
    protected Action addFromNavigationAction;
    protected Action removeDocumentAction;
    protected Action loadBenchmarkAction;
    protected Action runExperimentAction;
    
    public DocumentsPage(IWorkbenchPart boundPart) {
        this.boundPart = boundPart;
    }

    /**
     *
     * Create the adapter factory.
     *
     * @return the adapterFactory
     */
    private ComposedAdapterFactory createAdapterFactory() 
    {
        List<AdapterFactory> factories = new ArrayList<AdapterFactory>();
        factories.add(new DocumentItemProviderAdapterFactory2());
        factories.add(new ResourceItemProviderAdapterFactory());
        factories.add(new ReflectiveItemProviderAdapterFactory());
        factories.add(new ExampleItemProviderAdapterFactory2());
        factories.add(new AnnotationsItemProviderAdapterFactory2());
        factories.add(new SelectionItemProviderAdapterFactory2());
        return new ComposedAdapterFactory(factories);
    }

    /**
     *
     * Create the controls of this page.
     *
     * @param parent
     */
    public void createControl(Composite parent) 
    {
        viewer = new TreeViewer(parent, SWT.SINGLE | SWT.H_SCROLL | SWT.V_SCROLL);
        ComposedAdapterFactory adapterFactory = createAdapterFactory();
        viewer.setContentProvider(new AdapterFactoryContentProvider(adapterFactory));
        viewer.setLabelProvider(new AdapterFactoryLabelProvider(adapterFactory));
        viewer.setAutoExpandLevel(2);

        //need to set the input here if the documentContainer is not null
        if (docCol!=null) 
        {
        	DocumentCollectionList dCols = docCol.getDocumentCollectionList();
        	if (dCols!=null) {
        		viewer.setInput(dCols);
        	}
        }

        lastViewerSel = viewer.getSelection();

        //right mouse-click handler
        setRightMouseClickListener();

        //selection handler
        viewer.addSelectionChangedListener(new ISelectionChangedListener() 
        {
            public void selectionChanged(SelectionChangedEvent e) 
            {
                IStructuredSelection selection = (IStructuredSelection) e.getSelection();
                if (lastViewerSel==null && selection==null) return;
                if (lastViewerSel!=null && selection!=null && lastViewerSel.equals(selection)) return;

                lastViewerSel = e.getSelection();
                
                Object selectedObj = selection.getFirstElement(); //only one selection permitted
                if (selectedObj instanceof DocumentEntry) 
                {
                    final DocumentEntry de = (DocumentEntry) selectedObj;
                    docCol.setDocumentSelection(de);
                }
            }
        });

        //required to connect with the Property View (could probably delete this)
        getSite().setSelectionProvider(viewer);

        //setup toolbar menu
        makeActions();
        contributeActions();
        hookContextMenu();
        addRightClickMenu();
    }

    public ISelection getLastViewerSel() {
		return lastViewerSel;
	}

	public Control getControl() {
        return
            viewer==null?
            null:
            viewer.getControl();
    }

    public void setFocus() {
        viewer.getControl().setFocus();
    }

    protected void styleTree(Tree tree) {
        //tree.setLinesVisible(true);
        tree.setFont(tree.getParent().getFont());
    }

    protected void makeActions() 
    {
//        addAction = new AddAction(this);
//      activateAction = new ActivateAction(this);

        removeAction = new RemoveAction(this);
        loadAction = new LoadAction(this);
        saveAction = new SaveAction(this);
//        addURLAction = new AddURLAction(this);
        addFromFileAction = new AddFromFileAction(this);
//        addFromBrowserAction = new AddFromBrowserAction(this);
//        addFromNavigationAction = new ImportNavigationDocsAction(this);
        removeDocumentAction = new RemoveDocumentAction(this);
//        loadBenchmarkAction = new LoadBenchmarkAction(this);
//        runExperimentAction = new RunExperimentAction(this);//for now...
        runExperimentAction.setEnabled(false);
    }


    private void contributeActions()
    {
        IActionBars bars = getSite().getActionBars();
        fillLocalPullDown(bars.getMenuManager());
        fillLocalToolBar(bars.getToolBarManager());
    }

    protected void fillLocalPullDown(IMenuManager manager) 
    {
//        manager.add(loadAction);
//        manager.add(saveAction);
//    	manager.add(removeAction);
////        manager.add(new Separator("#add"));
//        manager.add(addURLAction);
//        manager.add(addFromFileAction);
//        manager.add(addFromBrowserAction);
//        manager.add(addFromNavigationAction);
    }

    protected void fillContextMenu(IMenuManager manager) {
//        manager.add(activateAction);
//        manager.add(removeAction);
//        manager.add(new Separator(IWorkbenchActionConstants.MB_ADDITIONS));
    }

    protected void fillLocalToolBar(IToolBarManager manager) {
//        manager.add(addURLAction);
//        manager.add(addFromBrowserAction);
//        manager.add(addFromNavigationAction);
//        manager.add(loadAction);
//        manager.add(addAction);
//        manager.add(finishedAction);
//        manager.add(new Separator("#incluster"));
//        manager.add(clusterWrapAction);
//        manager.add(clusterFinishedDocAction);
    }

    private void hookContextMenu() {
//        MenuManager menuMgr = new MenuManager("#PopupMenu");
//        menuMgr.setRemoveAllWhenShown(true);
//        menuMgr.addMenuListener(new IMenuListener() {
//            public void menuAboutToShow(IMenuManager manager) {
//                DocumentsPage.this.fillContextMenu(manager);
//            }
//        });
//        Menu menu = menuMgr.createContextMenu(viewer.getControl());
//        viewer.getControl().setMenu(menu);
//        getSite().registerContextMenu(menuMgr, viewer);

//    	MenuManager menuMgr = new MenuManager("#PopupMenu");
//    	menuMgr.setRemoveAllWhenShown(true);
//    	menuMgr.addMenuListener(new IMenuListener() {
//    		public void menuAboutToShow(IMenuManager manager) {
//    			DocumentsPage.this.fillContextMenu(manager);
//    		}
//    	});
//    	Menu menu = menuMgr.createContextMenu(viewer.getControl());
//    	viewer.getControl().setMenu(menu);
//    	getSite().registerContextMenu("docs", menuMgr, viewer);
//        WrapperEditor we = LearnUIUtils.getWrapperEditor();
//        if (we!=null) {
//            Menu menu = we.createAndRegisterContextMenuFor(viewer);
//            System.err.println("menu"+menu);
//        }
    }

    /*
     * Do some cleanup by removing listeners hooked to the nsIDOMDocument
     */
    public void dispose() {
//        if (docCol!=null) {
//        	DocumentClusterGroup dcluster = docCol.getDocumentClusterGroup();
//            if (dcluster!=null) unregisterUnclassifiedListener(dcluster);
//        }

        super.dispose();
    }

    /**
     * 
     * IDOMInspector interface
     *
     * This method MUST only be called once at init time. It is part of the setup
     * needed by the DOMInspector.
     */
    public void setDocumentCollectionContainer(IDocumentCollectionContainer dcolc) 
    {

        //prevent this method to be called more than once
        if (this.docCol!=null)
            throw new RuntimeException("DocumentsPage already initialized, cannot call setDocumentCollectionContainer() more than once.");

        //save a reference
        this.docCol = dcolc;

        //the documentContainer is set as the input of the treeViewer in the createControl method
    }

    /**
//     * 
//     * This is an implementation of the ISelectionListner interface but it is invoked by
//     * the host PageBookView. The PageBookView is the one that registers as the
//     * ISelectionListener and it routes selectionChanged calls to the page that is
//     * visible.
//     *
//     * This way, we limit the number of events going arounds that will potentially
//     * get ignored.
//     *
//     * @see org.eclipse.ui.ISelectionListener#selectionChanged(org.eclipse.ui.IWorkbenchPart, org.eclipse.jface.viewers.ISelection)
//     */
    public void selectionChanged(IWorkbenchPart part, ISelection selection) 
    {
        if(viewer==null)
            return; //do nothing since the viewer is where the selection will be revealed

        if( selection.isEmpty() )
        {
            viewer.setSelection(null);
        }
        
        if (selection instanceof Benchmark)
        {
        	updateContextMenu();
        }
    }


    /**********************************************************************
     *
     *
     * Add a right-click menu with associated actions:
     *
     *  - remove selected document entry
     *  - remove all document entries
     *  - refresh view
     *
	 * 
	 * Add a right-click menu to this viewer.
	 *
	 */
	private void addRightClickMenu() {
		final MenuManager mgr = new MenuManager();

		final Action removeSelectedDocumentEntryAction = new Action("Remove") {
			public void run() {
				removeSelectedEntriesFromTreeViewer();
			}
		};
		removeSelectedDocumentEntryAction.setImageDescriptor(
				LearnUIImages.
				getImageDescriptor("icons/full/obj16/remove_example")); //$NON-NLS-1$);

		final Action removeAllDocumentEntriesAction = new Action("Remove All") {
			public void run() {
				removeAllEntriesFromTreeViewer();
			}
		};
		removeAllDocumentEntriesAction.setImageDescriptor(
				LearnUIImages.
				getImageDescriptor("icons/full/obj16/remove_example")); //$NON-NLS-1$);

		final Action refreshAction = new Action("Reload") {
			public void run() {
				viewer.refresh();
				viewer.expandToLevel(viewer.getAutoExpandLevel());
			}
		};
		refreshAction.setImageDescriptor(
				LearnUIImages.
				getImageDescriptor("icons/full/obj16/restart2")); //$NON-NLS-1$);

		mgr.setRemoveAllWhenShown(true);
		mgr.addMenuListener(new IMenuListener() {

			public void menuAboutToShow(IMenuManager manager) {
					manager.add(removeSelectedDocumentEntryAction);
					manager.add(removeAllDocumentEntriesAction);
					manager.add(refreshAction);
					manager.add(runExperimentAction);
			}

		});

		viewer.getControl().setMenu(mgr.createContextMenu(viewer.getControl()));
	}

	/**
	 * 
	 * Remove all selected entries
	 * 
	 * @param v
	 * @param index
	 */
	private void removeSelectedEntriesFromTreeViewer()
	{
		removeDocumentAction.run();
		viewer.refresh();
		viewer.expandToLevel(viewer.getAutoExpandLevel());
	}

	/**
	 * 
	 * Remove all entries
	 * 
	 * @param v
	 * @param index
	 */
	private void removeAllEntriesFromTreeViewer() {
		docCol.getDocumentClusterGroup().getUnclassified().clear();
		viewer.refresh();
		viewer.expandToLevel(viewer.getAutoExpandLevel());
	}
    
    //overridden in subclasses
    protected void onUnclassifiedChanged() {}

    private boolean isRightMouseClicked = false;

    /**
     * 
 	 * A right-click has been detected...
 	 * 
 	 */
 	private void setRightMouseClickListener () {
 		viewer.getTree().addMouseListener(new MouseAdapter() 
 		{
 			public void mouseDown(MouseEvent e) 
 			{	
				if (e.button==3) {
					isRightMouseClicked = true;
					updateContextMenu();
				}
				else
					isRightMouseClicked = false;
 			}
 		});
 	}

	/**
	 * 
	 * @return the isRightMouseClicked
	 */
	public boolean isRightMouseClicked() {
		return isRightMouseClicked;
	}

	private void updateContextMenu ()
	{
		ISelection sel = viewer.getSelection();
		if (sel instanceof IStructuredSelection) 
		{
			IStructuredSelection sselection = (IStructuredSelection) sel;
			if (sselection.getFirstElement() instanceof Benchmark) {
				runExperimentAction.setEnabled(true);
			} else {
				runExperimentAction.setEnabled(false);
			}
		}
	}
	
}//DocumentsPage
