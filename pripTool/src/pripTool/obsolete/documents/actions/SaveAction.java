package views.documents.actions;

import i18n.mt;

import java.io.File;
import java.util.Collections;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.dialogs.ProgressMonitorDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.ui.actions.WorkspaceModifyOperation;
import org.weblearn.model.core.document.DocumentCollection;

import utils.ErrorDump;
import views.documents.DocumentsPage;
import annotationide.LearnUIImages;
import container.IDocumentCollectionContainer;

/**
 *
 * @author ceresna
 */
public class SaveAction extends Action
{

    private final DocumentsPage page;

    public SaveAction(DocumentsPage page) {
        super(mt.ac_Documents_SaveAs);
        setToolTipText(mt.tt_Save_Document_Collection);
        setImageDescriptor(
            LearnUIImages.
            getImageDescriptor("icons/full/obj16/saveas")); //$NON-NLS-1$
        setId("org.weblearn.core.learn.learnSaveAsDocuments");
        this.page = page;
    }

    protected void doSave(final IDocumentCollectionContainer dcc, final File f) {
//        IStatusLineManager slm = view.getViewSite().getActionBars().getStatusLineManager();
//        IProgressMonitor progressMonitor =
//            slm!=null ?
//            slm.getProgressMonitor() :
//            new NullProgressMonitor();

        //Do the work within an operation because this is
        //a long running activity that modifies the workbench.
        WorkspaceModifyOperation operation =
            new WorkspaceModifyOperation() {
                public void execute(IProgressMonitor monitor) {
                    try {
                        ResourceSet rs = new ResourceSetImpl();
                        URI fileURI = URI.createFileURI(f.getAbsolutePath());
                        Resource r = rs.createResource(fileURI);

                        DocumentCollection dc = dcc.getDocumentCollection();
//                        for (DocumentCollection col : dc) {
                        	r.getContents().add(dc);
                        	r.save(Collections.EMPTY_MAP);
//                        }
                        
                    }
                    catch (Exception exception) {
                        ErrorDump.error(this, exception);
                    }
                }
            };

        try {
            // This runs the options, and shows progress.
            new ProgressMonitorDialog(page.getSite().getShell()).
            run(true, false, operation);
        }
        catch (Exception exception) {
            // Something went wrong that shouldn't.
            ErrorDump.error(this, exception);
        }
    }

    public void run() {
        FileDialog d = new FileDialog(page.getSite().getShell(), SWT.SAVE);
        d.open();
        String fname = d.getFileName();
        String path = d.getFilterPath();
        if (fname.length()>0) {
            File f = new File(path, fname);
            doSave(page.docCol, f);
        }
    }

}
