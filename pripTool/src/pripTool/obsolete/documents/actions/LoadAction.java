package views.documents.actions;

import i18n.mt;

import java.io.File;
import java.util.Collections;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IStatusLineManager;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.dialogs.ProgressMonitorDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.ui.actions.WorkspaceModifyOperation;
import org.weblearn.model.core.document.DocumentClusterGroup;

import utils.ErrorDump;
import views.documents.DocumentsPage;
import annotationide.LearnUIImages;
import at.tuwien.prip.common.utils.IRef;
import at.tuwien.prip.common.utils.Ref;
import container.IDocumentCollectionContainer;

/**
 * 
 * LoadAction.java
 *
 *
 * Load documents action.
 *
 * Created: May 8, 2009 11:01:42 PM
 *
 * @author Michal Ceresna
 * @version 1.0
 */
public class LoadAction extends Action
{

	/**
	 *
	 */
	private final DocumentsPage page;

	/**
	 *
	 * @param page
	 */
	public LoadAction (DocumentsPage page) {
		super(mt.ac_Documents_Open);

		setToolTipText(mt.tt_Open_Document_Collection);
		setImageDescriptor(LearnUIImages.	getImageDescriptor("icons/full/obj16/open")); //$NON-NLS-1$
		setId("org.weblearn.core.learn.learnLoadDocuments");
		this.page = page;
	}

	@SuppressWarnings("unchecked")
	protected void doLoad(final IDocumentCollectionContainer dcc, final File f) {
//		IStatusLineManager slm = view.getViewSite().getActionBars().getStatusLineManager();

		IStatusLineManager slm = this.page.getSite().getActionBars().getStatusLineManager();
		IProgressMonitor progressMonitor = slm!=null ? slm.getProgressMonitor() : new NullProgressMonitor();
		assert progressMonitor!=null;

		//Do the work within an operation because this is
		//a long running activity that modifies the workbench.
		final IRef<Exception> failed = new Ref<Exception>(null);
		WorkspaceModifyOperation operation =
			new WorkspaceModifyOperation() {
			public void execute(IProgressMonitor monitor) {

				try {
					ResourceSet rs = new ResourceSetImpl();
					URI fileURI = URI.createFileURI(f.getAbsolutePath());
					Resource r = rs.getResource(fileURI, true);
					r.load(Collections.EMPTY_MAP);

					DocumentClusterGroup docCol =
						(DocumentClusterGroup) r.getContents().get(0);

					DocumentClusterGroup dc = dcc.getDocumentClusterGroup();
					dc.getClusters().clear();
					dc.getClusters().addAll(docCol.getClusters());
					dc.getUnclassified().clear();
					dc.getUnclassified().addAll(docCol.getUnclassified());

				}
				catch (Exception exception) {
					ErrorDump.error(this, exception);
					failed.set(exception);
				}
			}
		};

		try {
			// This runs the options, and shows progress.
			new ProgressMonitorDialog(page.getSite().getShell()).
			run(true, false, operation);

			if (failed.get()!=null) {
				MessageDialog.
				openError(page.getSite().getShell(),
						mt.dlg_Error,
						mt.msg_documents_loading_failed);
			}
		}
		catch (Exception exception) {
			// Something went wrong that shouldn't.
			ErrorDump.error(this, exception);
		}
	}

	/**
	 *
	 */
	public void run() {
		FileDialog d = new FileDialog(page.getSite().getShell(), SWT.OPEN);
		d.open();
		String fname = d.getFileName();
		String path = d.getFilterPath();
		if (fname.length()>0) {
			File f = new File(path, fname);
			doLoad(page.docCol, f);
		}
	}

}
