package views.documents.actions;

import static at.tuwien.prip.mozcore.utils.ProxyUtils.qi;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IWorkbenchPart;
import org.mozilla.interfaces.nsIDOM3Document;

import at.tuwien.prip.docwrap.ide.DocWrapUIUtils;
import at.tuwien.prip.docwrap.ide.LearnUIImages;
import at.tuwien.prip.docwrap.ide.i18n.mt;
import at.tuwien.prip.docwrap.ide.shared.container.IDOMDocumentContainer;
import at.tuwien.prip.docwrap.ide.views.documents.AddURLDialog;
import at.tuwien.prip.docwrap.ide.views.documents.DocumentsPage;

/**
 * Adds to the collection new unclassified
 * documents from disc file(s).
 *
 * @author ceresna
 */
public class AddURLAction
extends BaseAddURLsAction
{

    private final IDOMDocumentContainer docc;

    public AddURLAction(DocumentsPage page) {
        super(mt.ac_Documents_Add_URL, page);
        setToolTipText(mt.tt_Add_Document_URL);
        setImageDescriptor(
                LearnUIImages.
                getImageDescriptor("icons/full/obj16/document_add")); //$NON-NLS-1$
        setId("org.weblearn.core.learn.learnAddDocumentFromURL"); //$NON-NLS-1$

        IWorkbenchPart p = page.boundPart;
        this.docc = (IDOMDocumentContainer) p.getAdapter(IDOMDocumentContainer.class);
    }

    public void run() {
        String uri = null;
        if (docc!=null) {
            nsIDOM3Document doc = qi(docc.getDocument(), nsIDOM3Document.class);
            if (doc!=null) {
                uri = doc.getDocumentURI();
            }
        }

        Shell s = DocWrapUIUtils.getActiveWorkbenchWindowShell();
        AddURLDialog d = new AddURLDialog(s, uri);
        int ret = d.open();
        if (ret!=Dialog.OK) return;

        String url = d.getURL();
        if (url.length()==0) return;

        super.run(url);
    }

}
