package views.documents.actions;

import java.util.ListIterator;

import org.eclipse.emf.edit.domain.IEditingDomainProvider;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.ui.IWorkbenchPart;
import org.weblearn.model.core.document.DocumentClusterGroup;
import org.weblearn.model.core.document.DocumentCollection;
import org.weblearn.model.core.document.DocumentEntry;

import utils.DocWrapUIUtils;
import utils.ErrorDump;
import views.documents.DocumentsPage;
import annotationide.LearnUIImages;
import editor.WrapperEditor;


/**
 * 
 * RemoveAction.java
 *
 *
 *
 * Created: May 8, 2009 11:02:54 PM
 *
 * @author Michal Ceresna
 * @version 1.0
 */
public class RemoveAction extends Action
{

    private final DocumentsPage page;

    protected final IEditingDomainProvider edp;

    public RemoveAction(DocumentsPage page) {
        super("Remove Document");
        setToolTipText("Remove Document");
        setImageDescriptor(
            LearnUIImages.
            getImageDescriptor("icons/full/obj16/removedoc")); //$NON-NLS-1$
        setId("org.weblearn.core.learn.learnRemoveDocument");
        this.page = page;

        IWorkbenchPart p = page.boundPart;
        this.edp = (IEditingDomainProvider) p.getAdapter(IEditingDomainProvider.class);

        setEnabled(edp!=null);
    }

    @SuppressWarnings("unchecked")
    public void run() {
        try {
        	WrapperEditor we = DocWrapUIUtils.getWrapperEditor();
            if (we==null) return;

            ISelection s = page.viewer.getSelection();
            if (!(s instanceof StructuredSelection)) return;

            StructuredSelection sel = (StructuredSelection) s;
            Object selobj = sel.getFirstElement();
            if (!(selobj instanceof DocumentEntry)) return;
            DocumentEntry delde = (DocumentEntry) sel.getFirstElement();

            DocumentClusterGroup dcol = we.getDocumentClusterGroup();
            ListIterator<DocumentCollection> dcit = dcol.getClusters().listIterator();
            while (dcit.hasNext())
            {
            	DocumentCollection docGroup = dcit.next();
                if (docGroup.getDocuments().contains(delde)) {
                    //remove from list
                    docGroup.getDocuments().remove(delde);
                    break;
                }
            }
        }
        catch (Exception e) {
            ErrorDump.error(this, e);
        }
    }
    
}
