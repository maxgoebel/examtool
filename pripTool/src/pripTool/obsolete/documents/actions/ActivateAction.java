package views.documents.actions;


import org.eclipse.jface.action.Action;

import views.documents.DocumentsPage;
import annotationide.LearnUIImages;

/**
 *
 * @author ceresna
 */
public class ActivateAction
    extends Action
{

//    private final DocumentsPage page;

    public ActivateAction(DocumentsPage page) {
        super("Activate Document");
        setToolTipText("Activate Document");
        setImageDescriptor(
            LearnUIImages.
            getImageDescriptor("icons/full/obj16/activatedoc")); //$NON-NLS-1$
        setId("org.weblearn.core.learn.learnActivateDocument");
//        this.page = page;
    }

    public void run() {
//        try {
//            WrapperEditor we = LearnUIUtils.getWrapperEditor();
//            if (we==null) return;
//
//            ISelection s = view.getViewer().getSelection();
//            if (!(s instanceof StructuredSelection)) return;
//
//            StructuredSelection sel = (StructuredSelection) s;
//            DocumentEntry de = (DocumentEntry) sel.getFirstElement();
//            if (de!=null) {
//                we.getChrome().go(de.getUri());
//            }
//        }
//        catch (Exception e) {
//            ErrorDump.error(this, e);
//        }
    }

}
