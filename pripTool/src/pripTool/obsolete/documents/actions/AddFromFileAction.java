package views.documents.actions;

import i18n.mt;

import java.io.File;
import java.util.LinkedList;
import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.FileDialog;

import views.documents.DocumentsPage;

/**
 * Adds to the collection new unclassified
 * documents from disc file(s).
 *
 * @author ceresna
 */
public class AddFromFileAction
extends BaseAddURLsAction
{

    private String path = null; //last directory in FileOpen dialog

    public AddFromFileAction(DocumentsPage page) {
        super(mt.ac_Documents_Add_from_File, page);
        setToolTipText(mt.tt_Add_Unclassfied_Documents_from_File_to_Collection);
//        setImageDescriptor(
//            LearnUIImages.
//            getImageDescriptor("icons/full/obj16/adddoc")); //$NON-NLS-1$
        setId("org.weblearn.core.learn.learnAddDocumentFromFile"); //$NON-NLS-1$
    }

    public void run() {

        FileDialog d = new FileDialog(page.getSite().getShell(), SWT.OPEN | SWT.MULTI);
        d.setFilterPath(path);
        d.setFilterExtensions(new String[]{"*.html;*.pdf;*.tiff"});
        d.open();
        String[] fnames = d.getFileNames();
        if (fnames.length==0) return;
        path = d.getFilterPath();

        List<String> uris = new LinkedList<String>();
        for (String fname : fnames) {
            File f = new File(path, fname);
            uris.add(f.toURI().toString());
        }

        super.run(uris);
    	page.viewer.refresh();
		page.viewer.expandToLevel(page.viewer.getAutoExpandLevel());
    }

}
