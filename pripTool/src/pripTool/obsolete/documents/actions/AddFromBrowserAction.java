package views.documents.actions;

import static at.tuwien.prip.mozcore.utils.ProxyUtils.qi;

import org.eclipse.ui.IWorkbenchPart;
import org.mozilla.interfaces.nsIDOM3Document;

import at.tuwien.prip.docwrap.ide.LearnUIImages;
import at.tuwien.prip.docwrap.ide.i18n.mt;
import at.tuwien.prip.docwrap.ide.shared.container.IDOMDocumentContainer;
import at.tuwien.prip.docwrap.ide.views.documents.DocumentsPage;

/**
 *
 * @author ceresna
 */
public class AddFromBrowserAction
extends BaseAddURLsAction
{

	private final IDOMDocumentContainer docc;

    public AddFromBrowserAction(DocumentsPage page) {
        super(mt.ac_Documents_Add_from_Browser, page);
        setToolTipText(mt.tt_Add_Document_from_Browser);
        setImageDescriptor(
            LearnUIImages.
            getImageDescriptor("icons/full/obj16/browser_add")); //$NON-NLS-1$
        setId("org.weblearn.core.learn.learnAddDocumentFromBrowser");

        IWorkbenchPart p = page.boundPart;
        this.docc = (IDOMDocumentContainer) p.getAdapter(IDOMDocumentContainer.class);

        setEnabled(docc!=null && edp!=null);
    }

    public void run() {
        nsIDOM3Document doc = qi(docc.getDocument(), nsIDOM3Document.class);
        String uri = doc.getDocumentURI();
        super.run(uri);
    }

}
