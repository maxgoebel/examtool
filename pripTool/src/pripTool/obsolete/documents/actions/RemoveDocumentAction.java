package views.documents.actions;

import i18n.mt;

import java.util.Iterator;

import org.eclipse.emf.common.command.CompoundCommand;
import org.eclipse.emf.edit.command.RemoveCommand;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.emf.edit.domain.IEditingDomainProvider;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TreePath;
import org.eclipse.jface.viewers.TreeSelection;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.part.IPageSite;
import org.weblearn.model.core.document.Benchmark;
import org.weblearn.model.core.document.DocumentClusterGroup;
import org.weblearn.model.core.document.DocumentCollection;
import org.weblearn.model.core.document.DocumentCollectionList;
import org.weblearn.model.core.document.DocumentEntry;

import views.documents.DocumentsPage;

/**
 * 
 * Action to remove selected document in document view from 
 * editing domain.
 * 
 * @author max
 *
 */
public class RemoveDocumentAction extends Action 
{

	protected final DocumentsPage page;
	protected final IEditingDomainProvider edp;

	public RemoveDocumentAction(DocumentsPage page) 
	{
		this.page = page;

		IWorkbenchPart p = page.boundPart;
		this.edp = (IEditingDomainProvider) p.getAdapter(IEditingDomainProvider.class);

		setEnabled(edp!=null);
	}

	@Override
	public void run() 
	{
		CompoundCommand ccmd = new CompoundCommand(mt.cmd_Add_Document);

		EditingDomain ed = edp.getEditingDomain();
		if (ed==null) return;

		Object parent = null;
		
		IStructuredSelection selection = (IStructuredSelection)page.viewer.getSelection();
		if (selection instanceof TreeSelection) {
			TreeSelection tsel = (TreeSelection) selection;
			TreePath path = tsel.getPathsFor(tsel.getFirstElement())[0];
			parent = path.getFirstSegment();
		}
		else if (selection instanceof DocumentEntry) {

		}

		DocumentClusterGroup docCluster = page.docCol.getDocumentClusterGroup();
		DocumentCollection collection = (DocumentCollection) page.docCol.getDocumentCollectionList().getCollections().get(0);
		DocumentCollectionList colList = page.docCol.getDocumentCollectionList();

		if (docCluster==null) return;

		for (Iterator<?> iterator = selection.iterator(); iterator.hasNext();)
		{	
			Object o = iterator.next();
			if (o instanceof DocumentCollection) {
				RemoveCommand cmd = 
					new RemoveCommand (	ed, colList.getCollections(), o);
				ccmd.append(cmd);
			}
			else if (o instanceof DocumentEntry) {
				RemoveCommand cmd = 
					new RemoveCommand (	ed, collection.getDocuments(), o);
				ccmd.append(cmd);
			} 
			else if (o instanceof Benchmark) 
			{
				if (parent!=null && parent instanceof Benchmark) {
					Benchmark benchmark = (Benchmark) parent;
					RemoveCommand cmd = 
						new RemoveCommand (	ed, benchmark.getDocuments(), o);
					ccmd.append(cmd);
				}
			}
		}

		ed.getCommandStack().execute(ccmd);

		page.viewer.refresh();
		page.viewer.expandToLevel(page.viewer.getAutoExpandLevel());

		//close associated editors if necessary
		closeAssociatedEditorsViews(page.getSite());

	}

	private void closeAssociatedEditorsViews (IPageSite site) 
	{
//		IEditorReference[] eRefs = site.getPage().getEditorReferences();
//		IEditorReference editor = null;
//		for (IEditorReference ref : eRefs) {
//			if (ref.getId().equals(WrapperEditor.ID)) {
//				editor = ref;
//				break;
//			}
//		}
//		if (editor!=null)
//		{
//			//				site.getPage().closeAllEditors(true);
//			//				WrapperEditor dwEditor = (WrapperEditor) editor.getEditor(false);
//			//				dwEditor.clearInput();
//			//				site.getPage().closeEditor(dwEditor, true);
//
//			IViewReference[] vRefs = site.getPage().getViewReferences();
//			IViewReference view = null;
//			for (IViewReference ref : vRefs) {
//				if (ref.getId().equals(PatternViewClassic.ID)) {
//					view = ref;
//					break;
//				}
//			}
//			if (view!=null) {
//				PatternViewClassic pvc = (PatternViewClassic) view.getView(false);
//				pvc.setGraph(null);
//			}
//		}
	}

}
