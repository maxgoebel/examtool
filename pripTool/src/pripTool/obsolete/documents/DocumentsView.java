package views.documents;

import i18n.mt;

import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.ISelectionProvider;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.ISelectionListener;
import org.eclipse.ui.IViewSite;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.part.IPage;
import org.eclipse.ui.part.MessagePage;
import org.eclipse.ui.part.PageBook;
import org.eclipse.ui.part.PageBookView;


/**
 * DocumentsView.java
 *
 *
 * @author mcg <mcgoebel@gmail.com>
 * @date May 31, 2011
 */
public class DocumentsView extends PageBookView
implements ISelectionProvider, ISelectionListener
{

    public static final String ID = "at.tuwien.prip.docwrap.ide.docView";

    private DocumentsPage page;

    /*
     *
     */
    protected IPage createDefaultPage(PageBook book) {
        MessagePage page = new MessagePage();
        initPage(page);
        page.createControl(book);
        page.setMessage(mt.lb_No_Document_Collection_in_Editor);
        return page;
    }

    /*
     *
     */
    protected PageRec doCreatePage(IWorkbenchPart part)
    {
        Object adapterObj = part.getAdapter(IDocumentsViewAdapter.class);
        if (adapterObj!=null && adapterObj instanceof IDocumentsViewAdapter) 
        {
            IDocumentsViewAdapter adapter = (IDocumentsViewAdapter) adapterObj;
            page = adapter.getDocumentsPage();
            initPage(page);
            page.createControl(getPageBook());
            return new PageRec(part, page);
        }

        return null; //nothing to create
    }

    /*
     *
     */
    protected void doDestroyPage(IWorkbenchPart part, PageRec pageRecord) {
        DocumentsPage page = (DocumentsPage) pageRecord.page;
        page.dispose();
        pageRecord.dispose();
    }

    /*
     * Try to return the active editor
     */
    protected IWorkbenchPart getBootstrapPart() {
        IWorkbenchPage page = getSite().getPage();
        if (page!=null)
            return page.getActiveEditor();

        return null;
    }

    /*
     * (non-Javadoc)
     * @see org.eclipse.ui.part.PageBookView#init(org.eclipse.ui.IViewSite)
     */
    public void init(IViewSite site) throws PartInitException {
    	site.getPage().addSelectionListener(this); //setting as selection listener
    	super.init(site);
    }

    /*
     * Only IEditorPart instances are of interest
     */
    protected boolean isImportant(IWorkbenchPart part) {
        return part instanceof IEditorPart;
    }

    public void dispose() {
        super.dispose();
        getSite().getPage().removeSelectionListener(this);
    }

    /*
     * ISelectionProvider interface
     */
    public void addSelectionChangedListener(ISelectionChangedListener listener) {
        getSelectionProvider().addSelectionChangedListener(listener);
    }

    public ISelection getSelection() {
        return getSelectionProvider().getSelection();
    }

    public void removeSelectionChangedListener(ISelectionChangedListener listener) {
        getSelectionProvider().removeSelectionChangedListener(listener);
    }

    public void setSelection(ISelection selection) {
        getSelectionProvider().setSelection(selection);
    }

    /*
     * ISelectionListener interface
     *
     * Notifying only the DOM Inspector page that is in view. It should match
     * the Browser editor that is providing the selection.
     */
     public void selectionChanged(IWorkbenchPart part, ISelection selection) {
        // we ignore our own selection or null selection
        if (part==this || selection==null)
            return;

        if (this.page==null)
        	return;
        
        if (page.isRightMouseClicked())
        	return;

        if( !(getCurrentPage() instanceof DocumentsPage))
            return;

        // pass the selection to the page
        DocumentsPage page = (DocumentsPage) getCurrentPage();
        if (page!=null) {
            page.selectionChanged(part, selection);
        }
    }

     public IWorkbenchPart getBoundPart(IPage page) {
         return super.getPageRec(page).part;
     }

}//DocumentsView
