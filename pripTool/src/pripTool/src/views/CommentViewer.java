package views;

import org.eclipse.jface.text.Document;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.ITextInputListener;
import org.eclipse.jface.text.ITextListener;
import org.eclipse.jface.text.TextEvent;
import org.eclipse.jface.text.TextViewer;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.part.ViewPart;

import utils.document.DocumentUpdateEvent;
import utils.document.IDocumentUpdateListener;
import utils.model.ExamSheet;
import utils.model.IModelChangedListener;
import utils.model.ModelChangedEvent;
import annotationide.Activator;
import editor.ExamEditor;

public class CommentViewer extends ViewPart
implements ISelectionChangedListener, IModelChangedListener, IDocumentUpdateListener
{

	private TextViewer viewer;
	
//	private Action saveComment;
	
	public static final String ID = "pripTool.commentEditor";


	@Override
	public void createPartControl(Composite parent) 
	{
		Activator.modelControl.addModelChangedListener(this);
		ExamEditor.documentControl.addDocumentUpdateListener(this);
		
		viewer =  new TextViewer(parent, SWT.NONE);
		viewer.setInput(new Document());
		
		viewer.addTextListener(new ITextListener() {
			
			@Override
			public void textChanged(TextEvent event) 
			{
				ExamSheet active = Activator.modelControl.getExam().getActive();
				if (active!=null) {
					String text = ((Document)viewer.getInput()).get();
					active.setComment(text);
				}
			}
		});
	}

	@Override
	public void setFocus() {
		// TODO Auto-generated method stub
		
	}

	public String getComment() {
		return ((Document) viewer.getInput()).get();
	}
	
	public void resetInput(ExamSheet sheet) {
		Document doc = new Document();
		doc.set(sheet.getComment());
		viewer.setInput(doc);
	}
	@Override
	public void modelChanged(ModelChangedEvent event) 
	{	
		//save to model...
//		System.out.println("hello world part 1");
		
	}

	@Override
	public void selectionChanged(SelectionChangedEvent event) {
//		System.out.println("hello world part 2");
		
	}

	@Override
	public void documentUpdated(DocumentUpdateEvent ev) {
//		System.out.println("hello world part 3");
		
	}

}
