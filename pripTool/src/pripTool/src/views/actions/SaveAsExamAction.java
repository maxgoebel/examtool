package views.actions;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import utils.SafeSaveDialog;
import utils.model.ExamModel;
import utils.model.ExamSheet;
import annotationide.Activator;
import annotationide.ICommandIds;
import at.tuwien.prip.common.datastructures.MapList;
import at.tuwien.prip.core.model.document.segments.RectSegment;

/**
 * 
 * @author max
 *
 */
public class SaveAsExamAction extends Action
{

	private final IWorkbenchWindow window;

	/**
	 * 
	 * @param text
	 * @param window
	 */
	public SaveAsExamAction(String text, IWorkbenchWindow window) 
	{
		super(text);
		this.window = window;

		setId(ICommandIds.CMD_LOAD_EXAM);
		setActionDefinitionId(ICommandIds.CMD_LOAD_EXAM);
		Image saveImage = PlatformUI.getWorkbench().getSharedImages().getImage(ISharedImages.IMG_ETOOL_SAVE_EDIT);
		setImageDescriptor(ImageDescriptor.createFromImage(saveImage) );
	}

	public void run() 
	{
		SafeSaveDialog dialog = new SafeSaveDialog(window.getShell());
		dialog.setFileName("exam.xml");

		String result = dialog.open();
		if (result==null || result.length()<1) {return;}

		ExamModel model = Activator.modelControl.getExam();
		serializeModel(model, result);
	}

	/**
	 * 
	 * @param model
	 */
	public void serializeModel(ExamModel model, String fileName)
	{
		DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder;

		try 
		{
			docBuilder = docFactory.newDocumentBuilder();
			Document doc = docBuilder.newDocument();
			Element rootElement = doc.createElement("document");
			doc.appendChild(rootElement);

			for (ExamSheet sheet : model.getPages()) 
			{
				Element page = doc.createElement("page");
				page.setAttribute("fileName", sheet.getFileName());
				page.setAttribute("pageNumber", ""+sheet.getPageNum());
				page.setAttribute("status", ""+sheet.getStatus());
				
				rootElement.appendChild(page);
				
				for (Rectangle in : sheet.getSelectionBoxes()) 
				{
					Element box = doc.createElement("highlight");
					box.setAttribute("x", ""+in.x);
					box.setAttribute("y", ""+in.y);
					box.setAttribute("w", ""+in.width);
					box.setAttribute("h", ""+in.height);
					page.appendChild(box);
				}
				for (Rectangle in : sheet.getGreenMarkers()) 
				{
					Element box = doc.createElement("greenMarker");
					box.setAttribute("x", ""+in.x);
					box.setAttribute("y", ""+in.y);
					box.setAttribute("w", ""+in.width);
					box.setAttribute("h", ""+in.height);
					page.appendChild(box);
				}
				for (Rectangle in : sheet.getRedMarkers()) 
				{
					Element box = doc.createElement("redMarker");
					box.setAttribute("x", ""+in.x);
					box.setAttribute("y", ""+in.y);
					box.setAttribute("w", ""+in.width);
					box.setAttribute("h", ""+in.height);
					page.appendChild(box);
				}
				for (Rectangle in : sheet.getYellowMarkers()) 
				{
					Element box = doc.createElement("yellowMarker");
					box.setAttribute("x", ""+in.x);
					box.setAttribute("y", ""+in.y);
					box.setAttribute("w", ""+in.width);
					box.setAttribute("h", ""+in.height);
					page.appendChild(box);
				}
				
			}
			// write the content to XML file
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");

			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(new File(fileName));

			// Output to console for testing
			// StreamResult result = new StreamResult(System.out);

			transformer.transform(source, result);
			System.out.println("File saved!");

		} 
		catch (ParserConfigurationException e) 
		{
			e.printStackTrace();
		} 
		catch (TransformerConfigurationException e) 
		{
			e.printStackTrace();
		} 
		catch (TransformerException e) 
		{
			e.printStackTrace();
		}	
	}

	/**
	 * 
	 * @param input
	 * @param eval
	 * @param fileName
	 */
	public static void createXMLfromRectangles(
			MapList<Integer,RectSegment> input, 
			MapList<Integer,RectSegment> eval, 
			String fileName) 
	{
		DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder;

		int numPages = 0;
		if (input==null) {
			List<Integer> tmp = new ArrayList<Integer>(eval.keySet());
			Collections.sort(tmp);
			if (tmp.size()>0) {
				numPages = tmp.get(tmp.size()-1);
			}
		} else 
		{
			int tmp1Size = 0;
			List<Integer> tmp1 = new ArrayList<Integer>(eval.keySet());
			if (tmp1.size()>0) {
				Collections.sort(tmp1);
				tmp1Size = tmp1.get(tmp1.size()-1);
			}
			int tmp2Size = 0;
			List<Integer> tmp2 = new ArrayList<Integer>(input.keySet());
			if (tmp2.size()>0) {
				Collections.sort(tmp2);
				tmp2Size = tmp2.get(tmp2.size()-1);
			}
			numPages = Math.max(tmp1Size,tmp2Size);
		}

		try 
		{
			docBuilder = docFactory.newDocumentBuilder();
			Document doc = docBuilder.newDocument();
			Element rootElement = doc.createElement("document");
			doc.appendChild(rootElement);

			for (int i=0; i<numPages; i++) 
			{
				Element page = doc.createElement("page");
				page.setAttribute("number", ""+i+1);

				List<RectSegment> inputBoxes = input.get(i+1);
				if (inputBoxes!=null) 
				{
					for (RectSegment in : inputBoxes) {
						Element box = doc.createElement("input");
						box.setAttribute("x", ""+in.getX1());
						box.setAttribute("y", ""+in.getY1());
						box.setAttribute("w", ""+in.getWidth());
						box.setAttribute("h", ""+in.getHeight());
						page.appendChild(box);
					}
				}

				List<RectSegment> evalBoxes = eval.get(i+1);
				if (evalBoxes!=null) 
				{
					for (RectSegment ev : evalBoxes) {
						Element box = doc.createElement("eval");
						box.setAttribute("x", ""+ev.getX1());
						box.setAttribute("y", ""+ev.getY1());
						box.setAttribute("w", ""+ev.getWidth());
						box.setAttribute("h", ""+ev.getHeight());
						page.appendChild(box);
					}
				}
				rootElement.appendChild(page);
			}

			// write the content to XML file
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");

			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(new File(fileName));

			// Output to console for testing
			// StreamResult result = new StreamResult(System.out);

			transformer.transform(source, result);

			System.out.println("File saved!");

		} 
		catch (ParserConfigurationException e) 
		{
			e.printStackTrace();
		} 
		catch (TransformerConfigurationException e) 
		{
			e.printStackTrace();
		} 
		catch (TransformerException e) 
		{
			e.printStackTrace();
		}	
	}
}
