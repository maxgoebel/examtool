package views.actions;

import java.awt.Color;
import java.awt.Rectangle;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.eclipse.jface.action.Action;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.ui.IWorkbenchWindow;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import utils.model.ExamModel;
import utils.model.ExamSheet;

import annotationide.Activator;
import at.ac.tuwien.dbai.pdfwrap.analysis.PageProcessor;
import at.ac.tuwien.dbai.pdfwrap.exceptions.DocumentProcessingException;
import at.ac.tuwien.dbai.pdfwrap.utils.ProcessFile;
import at.tuwien.prip.common.datastructures.HashMapList;
import at.tuwien.prip.common.datastructures.MapList;
import at.tuwien.prip.core.model.graph.AdjacencyGraph;
import at.tuwien.prip.core.model.segments.GenericSegment;
import at.tuwien.prip.core.model.segments.Page;
import at.tuwien.prip.core.model.segments.RectSegment;

/**
 * ExtractFromPDFAction.java
 *
 * 
 *
 * @author max
 * @date Jun 30, 2012
 */
public class ExtractFromPDFAction extends Action 
{

	private final IWorkbenchWindow window;

	/**
	 * 
	 * @param title
	 * @param window
	 */
	public ExtractFromPDFAction(String title, IWorkbenchWindow window) 
	{
		this.window = window;
		setText(title);
	}

	@Override
	public void run() 
	{
		FileDialog dialog = new FileDialog(window.getShell());
		dialog.setFileName(System.getenv("HOME"));
		dialog.setFilterExtensions(new String[]{"*.csv","*.zip"});
		String result = dialog.open();
		if (result==null || result.length()<1) {return;}

		int pageNum = 0;

		try 
		{
			File file = new File(result);
			byte[] inputDoc = ProcessFile.getBytesFromFile(file);

			List<AdjacencyGraph<GenericSegment>> adjGraphList = new ArrayList<AdjacencyGraph<GenericSegment>>();

			//load the pages
			List<Page> pages = ProcessFile.processPDF(
					inputDoc, 
					null,
					PageProcessor.PP_INSTRUCTION,//processType, 
					false,false,//rulingLines,ignoreSpaces, 
					pageNum,pageNum, //startPage,endPage, 
					"","", //encoding,password, 
					0, //maxIterations, 
					adjGraphList, 
					false);

			/*
			 * Load in all red, green, and yellow boxes
			 */
			MapList<Integer,RectSegment> evalBoxes = new HashMapList<Integer,RectSegment>();
			MapList<Integer,RectSegment> inputBoxes = new HashMapList<Integer,RectSegment>();
			MapList<Integer,RectSegment> gradeBoxes = new HashMapList<Integer,RectSegment>();
			Rectangle bb = null;

			for (int i=0; i<pages.size(); i++) 
			{
				Page page = pages.get(i);
				bb = page.getBoundingRectangle();
				List<GenericSegment> segments = page.getItems();
				for (GenericSegment gs : segments) 
				{
					if (gs instanceof RectSegment) 
					{
						RectSegment rs = (RectSegment) gs;
						if (rs.isFilled() && rs.getFillColor().equals(Color.green)) {
							inputBoxes.putmore(i+1,(RectSegment) gs);
						}
						else if (rs.isFilled() && rs.getFillColor().equals(Color.red)) {
							evalBoxes.putmore(i+1,(RectSegment) gs);
						}
						else if (rs.isFilled() && rs.getFillColor().equals(Color.black)) {
							gradeBoxes.putmore(i+1,(RectSegment) gs);
						}
					}
				}
			}

			String outFile = "geo.xml";
			createXMLfromRectangles(bb, inputBoxes, evalBoxes, gradeBoxes, outFile);

			//load in model
			ExamModel model = Activator.modelControl.getExam();
			if (new File(result).exists())
			{
				LoadExamAction.processXML(result, model);
			}

			//update all sheets
			for (ExamSheet sheet : model.getPages()) 
			{
				sheet.setSelectionBoxes(model.getSelectionBoxes().get(sheet.getPageNum()));
				sheet.clearPoints();
			}
		}
		catch (IOException e) 
		{

		}
		catch (DocumentProcessingException e)
		{

		}
	}

	/**
	 * Create an XML file from all detected boxes.
	 * 
	 * @param bounds
	 * @param input
	 * @param eval
	 * 
	 * @param fileName
	 */
	public static void createXMLfromRectangles(
			Rectangle bounds,
			MapList<Integer,RectSegment> input, 
			MapList<Integer,RectSegment> eval, 
			MapList<Integer,RectSegment> grades, 
			String fileName) 
	{
		DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder;

		int numPages = 0;
		if (input==null) {
			List<Integer> tmp = new ArrayList<Integer>(eval.keySet());
			Collections.sort(tmp);
			if (tmp.size()>0) {
				numPages = tmp.get(tmp.size()-1);
			}
		} else 
		{
			int tmp1Size = 0;
			List<Integer> tmp1 = new ArrayList<Integer>(eval.keySet());
			if (tmp1.size()>0) {
				Collections.sort(tmp1);
				tmp1Size = tmp1.get(tmp1.size()-1);
			}
			int tmp2Size = 0;
			List<Integer> tmp2 = new ArrayList<Integer>(input.keySet());
			if (tmp2.size()>0) {
				Collections.sort(tmp2);
				tmp2Size = tmp2.get(tmp2.size()-1);
			}
			int tmp3Size = 0;
			List<Integer> tmp3 = new ArrayList<Integer>(grades.keySet());
			if (tmp3.size()>0) {
				Collections.sort(tmp3);
				tmp3Size = tmp3.get(tmp3.size()-1);
			}
			numPages = Math.max(Math.max(tmp1Size,tmp2Size),tmp3Size);
		}

		try 
		{
			docBuilder = docFactory.newDocumentBuilder();
			Document doc = docBuilder.newDocument();
			Element rootElement = doc.createElement("document");
			rootElement.setAttribute("x", bounds.x+"");
			rootElement.setAttribute("y", bounds.y+"");
			rootElement.setAttribute("w", bounds.width+"");
			rootElement.setAttribute("h", bounds.height+"");
			doc.appendChild(rootElement);

			for (int i=0; i<numPages; i++) 
			{
				Element page = doc.createElement("page");
				page.setAttribute("number", ""+new Integer(i+1));

				//write input boxes
				List<RectSegment> inputBoxes = input.get(i+1);
				if (inputBoxes!=null) 
				{
					for (RectSegment in : inputBoxes) {
						Element box = doc.createElement("input");
						box.setAttribute("x", ""+(int)in.getX1());
						box.setAttribute("y", ""+(int)in.getY1());
						box.setAttribute("w", ""+(int)in.getWidth());
						box.setAttribute("h", ""+(int)in.getHeight());
						page.appendChild(box);
					}
				}

				//write selection boxes
				List<RectSegment> evalBoxes = eval.get(i+1);
				if (evalBoxes!=null) 
				{
					for (RectSegment ev : evalBoxes) {
						Element box = doc.createElement("eval");
						box.setAttribute("x", ""+(int)ev.getX1());
						box.setAttribute("y", ""+(int)ev.getY1());
						box.setAttribute("w", ""+(int)ev.getWidth());
						box.setAttribute("h", ""+(int)ev.getHeight());
						page.appendChild(box);
					}
				}

				//write grades
				List<RectSegment> gradeBoxes = grades.get(i+1);
				if (gradeBoxes!=null) 
				{
					for (RectSegment in : gradeBoxes) {
						Element box = doc.createElement("grade");
						box.setAttribute("x", ""+(int)in.getX1());
						box.setAttribute("y", ""+(int)in.getY1());
						box.setAttribute("w", ""+(int)in.getWidth());
						box.setAttribute("h", ""+(int)in.getHeight());
						page.appendChild(box);
					}
				}

				rootElement.appendChild(page);
			}

			// write the content to XML file
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");

			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(new File(fileName));

			// Output to console for testing
			// StreamResult result = new StreamResult(System.out);

			transformer.transform(source, result);

			System.out.println("Transformation finished, file saved!");

		} 
		catch (ParserConfigurationException e) 
		{
			e.printStackTrace();
		} 
		catch (TransformerConfigurationException e) 
		{
			e.printStackTrace();
		} 
		catch (TransformerException e) 
		{
			e.printStackTrace();
		}	
	}
}
