package views.actions;

import java.io.File;

import org.eclipse.jface.action.Action;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.ui.IWorkbenchWindow;

import utils.LoadExamJob;
import utils.model.ExamModel;
import utils.model.ExamSheet;
import annotationide.Activator;

/**
 * 
 * LoadNewXmlModelAction.java
 *
 * 
 *
 * @author max
 * @date Jun 19, 2012
 */
public class LoadNewXmlModelAction extends Action
{

	private final IWorkbenchWindow window;
	
	public LoadNewXmlModelAction(String text, IWorkbenchWindow window) 
	{
		super(text);
		this.window = window;
	}
	
	@Override
	public void run()
	{
		ExamModel model = Activator.modelControl.getExam();
		FileDialog dialog = new FileDialog(window.getShell());
		dialog.setFileName(System.getenv("HOME"));
		dialog.setFilterExtensions(new String[]{"*.xml"});
		String result = dialog.open();
		if (result==null || result.length()<1) {return;}
		
		if (new File(result).exists())
		{
			LoadExamJob.processXML(result, model);
		}
		
		//update all sheets
		for (ExamSheet sheet : model.getPages()) 
		{
			sheet.setSelectionBoxes(model.getSelectionBoxes().get(sheet.getPageNum()));
			sheet.clearPoints();
		}
	}
	
}
