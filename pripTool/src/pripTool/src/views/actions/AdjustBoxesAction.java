package views.actions;

import org.eclipse.jface.action.Action;
import org.eclipse.ui.IWorkbenchWindow;

/**
 * 
 * AdjustBoxesAction.java
 *
 * 
 *
 * @author max
 * @date Jun 27, 2012
 */
public class AdjustBoxesAction extends Action
{
	private final IWorkbenchWindow window;

	/**
	 * 
	 * @param window
	 */
	public AdjustBoxesAction(String title, IWorkbenchWindow window) 
	{
		super(title);
		this.window = window;
	}

	@Override
	public void run() 
	{
		
	}
}
