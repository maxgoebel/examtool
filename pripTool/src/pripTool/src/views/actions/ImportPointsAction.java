package views.actions;

import org.eclipse.jface.action.Action;
import org.eclipse.ui.IWorkbenchWindow;

import utils.ImportPointsDialog;
import utils.model.ExamModel;
import utils.model.ExamSheet;
import annotationide.Activator;
import at.tuwien.prip.common.datastructures.TupleField;
import at.tuwien.prip.common.utils.CSVUtils;

/**
 * ImportPointsAction.java
 *
 * 
 *
 * @author max
 * @date Jun 30, 2012
 */
public class ImportPointsAction extends Action 
{

	private final IWorkbenchWindow window;
	
	/**
	 * 
	 * @param window
	 */
	public ImportPointsAction(String title, IWorkbenchWindow window) 
	{
		super(title);
		this.window = window;
	}
	
	@Override
	public void run()
	{
		ImportPointsDialog dialog = new ImportPointsDialog(window.getShell());
		int result = dialog.open();
		if (result==0)
		{
			ExamModel model = Activator.modelControl.getExam();
			String csvFile = dialog.getFirstName();
			if (model!=null && csvFile!=null && csvFile.length()>0)
			{
				//read in CSV file
				TupleField<String> csvField = CSVUtils.loadCSV(csvFile, "");
				
				//set points
				for (ExamSheet sheet : model.getPages()) 
				{
					String pt1 = csvField.queryData("Matr.Nr.",sheet.getStudentID(), "Pt1");
					String pt2 = csvField.queryData("Matr.Nr.",sheet.getStudentID(), "Pt2");
					String pt3 = csvField.queryData("Matr.Nr.",sheet.getStudentID(), "Pt3");
					
					sheet.setGrades(new String[]{pt1,pt2,pt3});
				}
			}
		}
	}
}
