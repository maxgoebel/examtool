package views.actions;

import org.eclipse.jface.action.Action;
import org.eclipse.ui.IWorkbenchWindow;

import annotationide.Activator;

/**
 * AdjustModelAction.java
 *
 * 
 *
 * @author max
 * @date Jun 30, 2012
 */
public class AdjustModelAction extends Action 
{

	private final IWorkbenchWindow window;
	
	/**
	 * Constructor.
	 * 
	 * @param title
	 * @param window
	 */
	public AdjustModelAction(String title, IWorkbenchWindow window) 
	{
		super (title, Action.AS_CHECK_BOX);
		this.window = window;
		setText(title);
	}
	
	@Override
	public void run() {
		Activator.setChangeModelState(!Activator.isChangeModelState());
	}
}

