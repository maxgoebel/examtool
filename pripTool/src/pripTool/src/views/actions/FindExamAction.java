package views.actions;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.dialogs.ErrorDialog;
import org.eclipse.jface.dialogs.IInputValidator;
import org.eclipse.jface.dialogs.InputDialog;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;

import utils.model.ExamModel;
import utils.model.ExamSheet;
import views.exam.ExamEditorInput;
import annotationide.Activator;
import editor.ExamEditor;

/**
 * FindExamAction.java
 *
 * 
 *
 * @author max
 * @date May 15, 2012
 */
public class FindExamAction extends Action
{

	private final IWorkbenchWindow window;
	
	/**
	 * 
	 * @param window
	 */
	public FindExamAction(String title, IWorkbenchWindow window) 
	{
		super(title);
		this.window = window;
	}
	
	@Override
	public void run() {
		InputDialog dlg = new InputDialog(Display.getCurrent().getActiveShell(),
				"Suche", "Suche Matrikelnummer:", "", new LengthValidator());
		if (dlg.open() == Window.OK)
		{
			ExamModel model = Activator.modelControl.getExam();
			ExamSheet found = null;
			for (ExamSheet sheet : model.getPages()) 
			{
				if (sheet.getStudentID().equals(dlg.getValue())) 
				{
					found = sheet;
					break;
				}
			}
			if (found==null) 
			{
				IStatus status=new Status(IStatus.ERROR,"Plugin ID","Matrikelnummer "+dlg.getValue()+" konnte nicht gefunden werden.",null);
				ErrorDialog.openError(Display.getCurrent().getActiveShell(), "Not Found", "", status);
			} 
			else 
			{
				//send selection to pdf editor
				IWorkbenchPage page = window.getActivePage();
				page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();

				if(page!=null && found!=null) 
				{
					try
					{
						page.closeAllEditors(true);
						ExamEditorInput input = new ExamEditorInput(found);
						IEditorPart editor = page.openEditor(input, ExamEditor.ID,true);
						ExamEditor we = (ExamEditor) editor;
						we.setInput2(input);
					}
					catch (PartInitException e) {
						e.printStackTrace();
					}
				}
				
			}
		}
	}
	
	/**
	 * This class validates a String. It makes sure that the String is between 5 and 8
	 * characters
	 */
	class LengthValidator implements IInputValidator {
	  /**
	   * Validates the String. Returns null for no error, or an error message
	   * 
	   * @param newText the String to validate
	   * @return String
	   */
	  public String isValid(String newText) {
	    int len = newText.length();

	    // Determine if input is too short or too long
	    if (len != 7) return "Bitte genau 7 Stellen eingeben";

	    // Input must be OK
	    return null;
	  }
	}
	
}
