package views.actions;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.jobs.IJobManager;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.core.runtime.jobs.ProgressProvider;
import org.eclipse.jface.action.Action;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.ui.IWorkbenchWindow;

import utils.LoadExamJob;
import utils.LoadExamProgressMonitor;
import annotationide.ICommandIds;
import at.tuwien.prip.common.log.ErrorDump;

/**
 * @author max
 *
 */
public class LoadExamAction extends Action
{

	public static String SEPARATOR = ",";

	private final IWorkbenchWindow window;

	/**
	 * 
	 * @param text
	 * @param window
	 */
	public LoadExamAction(String text, IWorkbenchWindow window) 
	{
		super(text);
		this.window = window;

		setId(ICommandIds.CMD_LOAD_EXAM);
		setActionDefinitionId(ICommandIds.CMD_LOAD_EXAM);
		setImageDescriptor(annotationide.Activator.getImageDescriptor("/icons/sample2.gif"));
	}

	public void run() 
	{	
		FileDialog dialog = new FileDialog(window.getShell());
		dialog.setFileName(System.getenv("HOME"));
		dialog.setFilterExtensions(new String[]{"*.zip","*.csv"});
		String result = dialog.open();
		if (result==null || result.length()<1) {return;}

		ErrorDump.debug(this, result + " opened");

		Job job = new LoadExamJob(window, "Load Exam Job", result);
		
		// Setting the progress monitor
		IJobManager manager = Job.getJobManager();


		final IProgressMonitor p = new LoadExamProgressMonitor(window.getShell());
		ProgressProvider provider = new ProgressProvider() {
			@Override
			public IProgressMonitor createMonitor(Job job) {
				return p;
			}
		};

		manager.setProgressProvider(provider);
		
		job.setUser(true);
		job.schedule();
	}

}
