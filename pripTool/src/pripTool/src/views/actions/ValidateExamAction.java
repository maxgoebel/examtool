package views.actions;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.ui.IWorkbenchWindow;

import utils.model.ExamModel;
import annotationide.Activator;

public class ValidateExamAction extends Action
{

	private final IWorkbenchWindow window;

	/**
	 * 
	 * @param title
	 * @param window
	 */
	public ValidateExamAction(String title, IWorkbenchWindow window) 
	{
		this.window = window;
		setText(title);
	}

	@Override
	public void run() 
	{
		//load in model
		ExamModel model = Activator.modelControl.getExam();

		if (model==null || model.getPages().length==0) 
		{
			MessageDialog.openInformation(window.getShell(), "Error", "No exam file could be found!");
		}
		model.getGradeFields();
		model.getSelectionBoxes();
		model.getInputFields();

		model.getNumUniqueIds();

		MessageDialog.openInformation(window.getShell(), "Information", "The loaded exam validated successfully!");
	}
}
