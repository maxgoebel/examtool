package views.actions;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import utils.model.ExamModel;
import utils.model.ExamSheet;
import annotationide.Activator;
import annotationide.ICommandIds;
import annotationide.LearnUIPlugin;
import at.tuwien.prip.common.datastructures.MapList;
import at.tuwien.prip.common.utils.ListUtils;
import at.tuwien.prip.core.model.document.segments.RectSegment;

/**
 * SaveExamAction.java
 *
 * 
 *
 * @author max
 * @date May 17, 2012
 */
public class SaveExamAction extends Action 
{

	protected final IWorkbenchWindow window;

	/**
	 * 
	 * @param text
	 * @param window
	 */
	public SaveExamAction(String text, IWorkbenchWindow window) 
	{
		super(text);
		this.window = window;

		setId(ICommandIds.CMD_LOAD_EXAM);
		setActionDefinitionId(ICommandIds.CMD_LOAD_EXAM);
		Image saveImage = PlatformUI.getWorkbench().getSharedImages().getImage(ISharedImages.IMG_ETOOL_SAVE_EDIT);
		setImageDescriptor(ImageDescriptor.createFromImage(saveImage) );
	}

	public void run() 
	{
		ExamModel model = Activator.modelControl.getExam();
		
		String result = model.getPath() + File.separator + ".exam-"+LearnUIPlugin.getTimestamp()+".mdl";//dialog.open();
		if (result==null || result.length()<1) {return;}

		serializeModel(model, result);
	}

	/**
	 * 
	 * @param fileName
	 * @return
	 */
	public static ExamModel unserializeModel(String fileName) 
	{
		ExamModel result = new ExamModel();
		List<ExamSheet> sheets = new ArrayList<ExamSheet>();
		
		try
		{
			File file = new File(fileName);
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = dbf.newDocumentBuilder();
			Document doc = db.parse(file);
			doc.getDocumentElement().normalize();
		
			NodeList nodeLst = doc.getElementsByTagName("document");
			Node docNode = nodeLst.item(0);
			if (docNode==null) {
				return result;
			}
			
			
			NodeList sheetNodes = ((Element)docNode).getElementsByTagName("sheet");
			for (int s = 0; s < sheetNodes.getLength(); s++) 
			{
				Node fstNode = sheetNodes.item(s);
				if (fstNode.getNodeType() == Node.ELEMENT_NODE) {
					Element sheetRoot = (Element) fstNode;
					ExamSheet sheet = loadExamSheet(sheetRoot);
					sheets.add(sheet);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		result.setPages(ListUtils.toArray(sheets, ExamSheet.class));
		return result;
	}
	
	/**
	 * 
	 * @param root
	 * @return
	 */
	private static ExamSheet loadExamSheet(Element root) 
	{
		ExamSheet result = null;
		if (root.getNodeName().equals("sheet")) 
		{
							
			Node comment = root.getElementsByTagName("comment").item(0);
			String fileName = root.getAttribute("fileName");
			int pageNumber = Integer.parseInt(root.getAttribute("pageNumber"));
			int status = Integer.parseInt(root.getAttribute("status"));
			String id = root.getAttribute("matNum");
			
			result = new ExamSheet(id, fileName, pageNumber);
			result.setPageNum(pageNumber);
			result.setStatus(status);
			result.setFileName(fileName);
			result.setComment(comment.getTextContent());
			
//			List<Rectangle> highlightBoxes = new ArrayList<Rectangle>();
//			NodeList highlights = root.getElementsByTagName("highlight");
//			for (int i=0; i<highlights.getLength(); i++) {
//				Element hl = (Element) highlights.item(i);
//				int x = Integer.parseInt(hl.getAttribute("x"));
//				int y = Integer.parseInt(hl.getAttribute("y"));
//				int w = Integer.parseInt(hl.getAttribute("w"));
//				int h = Integer.parseInt(hl.getAttribute("h"));
//				highlightBoxes.add(new Rectangle(x, y, w, h));
//			}
//			result.setHighlightBoxes(highlightBoxes);
			
			// green markers
			NodeList greens = root.getElementsByTagName("greenMarker");
			for (int i=0; i<greens.getLength(); i++) {
				Element hl = (Element) greens.item(i);
				int x = Integer.parseInt(hl.getAttribute("x"));
				int y = Integer.parseInt(hl.getAttribute("y"));
				int w = Integer.parseInt(hl.getAttribute("w"));
				int h = Integer.parseInt(hl.getAttribute("h"));
				result.getGreenMarkers().add(new Rectangle(x, y, w, h));
			}

			// green markers
			NodeList reds = root.getElementsByTagName("greenMarker");
			for (int i=0; i<reds.getLength(); i++) {
				Element hl = (Element) reds.item(i);
				int x = Integer.parseInt(hl.getAttribute("x"));
				int y = Integer.parseInt(hl.getAttribute("y"));
				int w = Integer.parseInt(hl.getAttribute("w"));
				int h = Integer.parseInt(hl.getAttribute("h"));
				result.getRedMarkers().add(new Rectangle(x, y, w, h));
			}

			// green markers
			NodeList yellows = root.getElementsByTagName("greenMarker");
			for (int i=0; i<yellows.getLength(); i++) {
				Element hl = (Element) yellows.item(i);
				int x = Integer.parseInt(hl.getAttribute("x"));
				int y = Integer.parseInt(hl.getAttribute("y"));
				int w = Integer.parseInt(hl.getAttribute("w"));
				int h = Integer.parseInt(hl.getAttribute("h"));
				result.getYellowMarkers().add(new Rectangle(x, y, w, h));
			}

		}
		return result;
	}

	/**
	 * 
	 * @param model
	 */
	public void serializeModel(ExamModel model, String fileName)
	{
		DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder;

		try 
		{
			docBuilder = docFactory.newDocumentBuilder();
			Document doc = docBuilder.newDocument();
			Element rootElement = doc.createElement("document");
			doc.appendChild(rootElement);

			for (ExamSheet sheet : model.getPages()) 
			{
				Element page = doc.createElement("sheet");
				page.setAttribute("fileName", sheet.getFileName());
				page.setAttribute("pageNumber", ""+sheet.getPageNum());
				page.setAttribute("status", ""+sheet.getStatus());
				page.setAttribute("matNum", sheet.getStudentID());
				rootElement.appendChild(page);

				Element comment = doc.createElement("comment");
				comment.setTextContent(sheet.getComment());
				page.appendChild(comment);

//				for (Rectangle in : sheet.getHighlightBoxes()) 
//				{
//					Element box = doc.createElement("highlight");
//					box.setAttribute("x", ""+in.x);
//					box.setAttribute("y", ""+in.y);
//					box.setAttribute("w", ""+in.width);
//					box.setAttribute("h", ""+in.height);
//					page.appendChild(box);
//				}
				for (Rectangle in : sheet.getGreenMarkers()) 
				{
					Element box = doc.createElement("greenMarker");
					box.setAttribute("x", ""+in.x);
					box.setAttribute("y", ""+in.y);
					box.setAttribute("w", ""+in.width);
					box.setAttribute("h", ""+in.height);
					page.appendChild(box);
				}
				for (Rectangle in : sheet.getRedMarkers()) 
				{
					Element box = doc.createElement("redMarker");
					box.setAttribute("x", ""+in.x);
					box.setAttribute("y", ""+in.y);
					box.setAttribute("w", ""+in.width);
					box.setAttribute("h", ""+in.height);
					page.appendChild(box);
				}
				for (Rectangle in : sheet.getYellowMarkers()) 
				{
					Element box = doc.createElement("yellowMarker");
					box.setAttribute("x", ""+in.x);
					box.setAttribute("y", ""+in.y);
					box.setAttribute("w", ""+in.width);
					box.setAttribute("h", ""+in.height);
					page.appendChild(box);
				}
			}
			
			// write the content to XML file
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");

			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(new File(fileName));
			
			// Output to console for testing
			// StreamResult result = new StreamResult(System.out);

			transformer.transform(source, result);
			System.out.println("File saved!");
		} 
		catch (ParserConfigurationException e) 
		{
			e.printStackTrace();
		} 
		catch (TransformerConfigurationException e) 
		{
			e.printStackTrace();
		} 
		catch (TransformerException e) 
		{
			e.printStackTrace();
		}	
	}

	/**
	 * 
	 * @param input
	 * @param eval
	 * @param fileName
	 */
	public static void createXMLfromRectangles(
			MapList<Integer,RectSegment> input, 
			MapList<Integer,RectSegment> eval, 
			String fileName) 
	{
		DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder;

		int numPages = 0;
		if (input==null) 
		{
			List<Integer> tmp = new ArrayList<Integer>(eval.keySet());
			Collections.sort(tmp);
			if (tmp.size()>0) {
				numPages = tmp.get(tmp.size()-1);
			}
		} 
		else 
		{
			int tmp1Size = 0;
			List<Integer> tmp1 = new ArrayList<Integer>(eval.keySet());
			if (tmp1.size()>0) {
				Collections.sort(tmp1);
				tmp1Size = tmp1.get(tmp1.size()-1);
			}
			int tmp2Size = 0;
			List<Integer> tmp2 = new ArrayList<Integer>(input.keySet());
			if (tmp2.size()>0) {
				Collections.sort(tmp2);
				tmp2Size = tmp2.get(tmp2.size()-1);
			}
			numPages = Math.max(tmp1Size,tmp2Size);
		}

		try 
		{
			docBuilder = docFactory.newDocumentBuilder();
			Document doc = docBuilder.newDocument();
			Element rootElement = doc.createElement("document");
			doc.appendChild(rootElement);

			for (int i=0; i<numPages; i++) 
			{
				Element page = doc.createElement("page");
				page.setAttribute("number", ""+i+1);

				List<RectSegment> inputBoxes = input.get(i+1);
				if (inputBoxes!=null) 
				{
					for (RectSegment in : inputBoxes) {
						Element box = doc.createElement("input");
						box.setAttribute("x", ""+in.getX1());
						box.setAttribute("y", ""+in.getY1());
						box.setAttribute("w", ""+in.getWidth());
						box.setAttribute("h", ""+in.getHeight());
						page.appendChild(box);
					}
				}

				List<RectSegment> evalBoxes = eval.get(i+1);
				if (evalBoxes!=null) 
				{
					for (RectSegment ev : evalBoxes) {
						Element box = doc.createElement("eval");
						box.setAttribute("x", ""+ev.getX1());
						box.setAttribute("y", ""+ev.getY1());
						box.setAttribute("w", ""+ev.getWidth());
						box.setAttribute("h", ""+ev.getHeight());
						page.appendChild(box);
					}
				}
				rootElement.appendChild(page);
			}

			// write the content to XML file
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");

			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(new File(fileName));

			// Output to console for testing
			// StreamResult result = new StreamResult(System.out);

			transformer.transform(source, result);

			System.out.println("File saved!");

		} 
		catch (ParserConfigurationException e) 
		{
			e.printStackTrace();
		} 
		catch (TransformerConfigurationException e) 
		{
			e.printStackTrace();
		} 
		catch (TransformerException e) 
		{
			e.printStackTrace();
		}	
	}
}
