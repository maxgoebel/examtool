package views.actions;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.dialogs.InputDialog;
import org.eclipse.jface.window.Window;
import org.eclipse.ui.IWorkbenchWindow;

import utils.model.ExamModel;

/**
 * LoadHeaderAction.java
 *
 * 
 *
 * @author max
 * @date Jun 27, 2012
 */
public class LoadHeaderAction extends Action 
{

	private final IWorkbenchWindow window;

	/**
	 * 
	 * @param window
	 */
	public LoadHeaderAction(String title, IWorkbenchWindow window) 
	{
		super(title);
		this.window = window;
	}

	@Override
	public void run() 
	{
		InputDialog dlg = new InputDialog(window.getShell(),
				"Info", "Bitte CSV header eingeben: ", "", null);
		if (dlg.open() == Window.OK) {
			String header = dlg.getValue();
			ExamModel.CSV_Header = header; //remember...
		}
	}
}
