package views.actions;

import java.io.BufferedWriter;
import java.io.FileWriter;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.dialogs.InputDialog;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.window.Window;
import org.eclipse.ui.IWorkbenchWindow;

import utils.SafeSaveDialog;
import utils.model.ExamModel;
import utils.model.ExamSheet;
import annotationide.Activator;

/**
 * ExportCSVAction.java
 *
 * 
 *
 * @author max
 * @date Jun 26, 2012
 */
public class ExportCSVAction extends Action 
{
	public static String SEPARATOR = ";";
	
	private final IWorkbenchWindow window;
	
	/**
	 * 
	 * @param window
	 */
	public ExportCSVAction(String title, IWorkbenchWindow window) 
	{
		super(title);
		this.window = window;
	}
	
	@Override
	public void run()
	{
		//check model is complete:
		ExamModel model = Activator.modelControl.getExam();
		for (ExamSheet sheet : model.getPages())
		{
			if (sheet.getStatus()!=3) 
			{
				if (MessageDialog.openConfirm(window.getShell(), "Info", "Es existieren unbearbeitete Prüfungen im Datenset. Wirklich exportieren?"))
				{
					break;
				} else {
					return;
				}
			}
		}
		
		SafeSaveDialog dialog = new SafeSaveDialog(window.getShell());
		dialog.setFileName("exam.csv");

		String result = dialog.open();
		if (result==null || result.length()<1) {return;}

		exportToCSV(model, result);
	}
	
	private String getHeader()
	{
		return "Matr.Nr."+SEPARATOR +
				"Matr.Check1"+SEPARATOR+
				"qrCode"+SEPARATOR+
				"0"+SEPARATOR+
				"1"+SEPARATOR+
				"2"+SEPARATOR+
				"3"+SEPARATOR+
				"4"+SEPARATOR+
				"5"+SEPARATOR+
				"6"+SEPARATOR+
				"7"+SEPARATOR+
				"8"+SEPARATOR+
				"9"+SEPARATOR+
				"Matr.Check2"+SEPARATOR+
				"Dilate1"+SEPARATOR+
				"Dilate2-4"+SEPARATOR+
				"Ei"+SEPARATOR+
				"Erode"+SEPARATOR+
				"Sj"+SEPARATOR+
				"a"+SEPARATOR+
				"b"+SEPARATOR+
				"Geraden"+SEPARATOR+
				"Length"+SEPARATOR+
				"FCC0"+SEPARATOR+
				"FCC01"+SEPARATOR+
				"FCC23"+SEPARATOR+
				"FCC-Bin"+SEPARATOR+
				"Bin-0*"+SEPARATOR+
				"Bin-01*"+SEPARATOR+
				"Bin-2*"+SEPARATOR+
				"Bin-23*"+SEPARATOR+
				"Kommentar(Seite1)"+SEPARATOR+
				"Kommentar(Seite2)"+SEPARATOR+"\n";
	}
	
	/**
	 * 
	 * @param model
	 * @param fileName
	 */
	private void exportToCSV (ExamModel model, String fileName) 
	{
		try
		{	
			String header = ExamModel.CSV_Header.trim();
			header = getHeader();
			if (header.split(SEPARATOR).length<2)
			{
				InputDialog dlg = new InputDialog(window.getShell(),
						"Info", "Es fehlt ein gültiger CSV Header. ", "", null);
				if (dlg.open() == Window.OK) {
					header = dlg.getValue();
					ExamModel.CSV_Header = header; //remember...
				}
			}
			if (header.contains(";")) {
				SEPARATOR = ";";
			} else if (header.contains(",")) {
				SEPARATOR = ",";
			} else {
				SEPARATOR = "\t"; //tab
			}
			//Create file 
			FileWriter fstream = new FileWriter(fileName);
			BufferedWriter out = new BufferedWriter(fstream);
			
			//Write header
			out.write(header+"\n");
			
			String[] matNums = model.getMatNums();
			for (String mat : matNums) 
			{
				//get first page
				ExamSheet s1 = model.findSheet(mat, 0);
				
				//get second page
				ExamSheet s2 = model.findSheet(mat, 1);
				
				if (s1!=null && s2!=null) 
				{
					out.write(toCsvString(s1, s2));
				}
			}
			
			//Close the output stream
			out.close();
			System.out.println("Export finished successfully...\n");
		}
		catch (Exception e){//Catch exception if any
			System.err.println("Error: " + e.getMessage());
			e.printStackTrace();
		}
	}
	
	/**
	 * 
	 * @param sheet
	 * @return
	 */
	private String toCsvString(ExamSheet s1, ExamSheet s2) 
	{
		assert s1.getStudentID()==s2.getStudentID();
		assert s1.getPageNum()==0;
		assert s2.getPageNum()==1;
		
		StringBuffer sb = new StringBuffer();
		sb.append(s1.getStudentID());
		String[] bs1 = s1.getPoints();

		sb.append(SEPARATOR+bs1[0]); //the matrikel number
		
		String qrCode = "";
		if (s1.getQrCode()==null || s1.getQrCode().trim().length()==0) 
		{
			qrCode = s1.getComment().split("\\n")[0].trim();
		}
		else 
		{
			qrCode = s1.getQrCode().trim();
		}

		sb.append(SEPARATOR+qrCode.trim());
		String[] questions = qrCode.split("\\s");
		String[] bilder = new String[]{"e","e","e","e","e","e","e","e","e","e"};
		if (questions.length==5)
		{
			for (int i=1; i<=questions.length; i++)
			{
				int id = Integer.parseInt(questions[i-1]);
				bilder[id] = bs1[i];
			}
			for (String bild : bilder) {
				sb.append(SEPARATOR+bild);
			}
		}

		String[] bs2 = s2.getPoints();
		for (int i=0; i<bs2.length; i++) {
			sb.append(SEPARATOR+(bs2[i]));
		}
		
		sb.append(SEPARATOR+s1.getComment()+SEPARATOR+s2.getComment());

		sb.append("\n");
		
		String result = sb.toString();
		return result;
	}
	
}
