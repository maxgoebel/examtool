package views;

import org.eclipse.jface.action.ToolBarManager;
import org.eclipse.jface.text.Document;
import org.eclipse.jface.text.ITextListener;
import org.eclipse.jface.text.TextEvent;
import org.eclipse.jface.text.TextViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Table;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ScrolledForm;
import org.eclipse.ui.forms.widgets.Section;
import org.eclipse.ui.menus.IMenuService;
import org.eclipse.ui.part.ViewPart;

import utils.document.DocumentUpdateEvent;
import utils.document.IDocumentUpdateListener;
import utils.model.ExamSheet;
import utils.model.IModelChangedListener;
import utils.model.ModelChangedEvent;
import annotationide.Activator;
import editor.ExamEditor;

public class SolutionView extends ViewPart
implements IModelChangedListener, IDocumentUpdateListener
{

	private TextViewer viewer;
	
	public static final String ID = "pripTool.solutionView";
	
	@Override
	public void createPartControl(Composite parent) 
	{
		Activator.modelControl.addModelChangedListener(this);
		ExamEditor.documentControl.addDocumentUpdateListener(this);
		
		FormToolkit toolkit = new FormToolkit(parent.getDisplay());
	    ScrolledForm form = toolkit.createScrolledForm(parent);
	    ToolBarManager manager = (ToolBarManager) form.getToolBarManager();
	    toolkit.decorateFormHeading(form.getForm());
	    IMenuService menuService = (IMenuService) getSite().getService(IMenuService.class);
	    menuService.populateContributionManager(manager, "popup:formsToolBar");
	    manager.update(true);

	    form.setText("Solution");

	    // Lets make a layout for the first section of the screen
//	    GridLayout layout = new GridLayout();
//	    layout.numColumns = 1;
//	    layout.marginWidth = 2;
//	    layout.marginHeight = 2;
	    // Creating the Screen
//	    Section section = toolkit.createSection(parent, Section.DESCRIPTION
//	        | Section.TITLE_BAR);
//	    section.setText("Section 1"); //$NON-NLS-1$
//	    section.setDescription("This demonstrates the usage of section");
	    // Composite for storing the data
//	    Composite client = toolkit.createComposite(section, SWT.WRAP);
//	    layout = new GridLayout();
//	    layout.numColumns = 2;
//	    layout.marginWidth = 2;
//	    layout.marginHeight = 2;
//	    client.setLayout(layout);
//	    Table t = toolkit.createTable(client, SWT.NULL);
//	    GridData gd = new GridData(GridData.FILL_BOTH);
//	    gd.heightHint = 20;
//	    gd.widthHint = 100;
//	    t.setLayoutData(gd);
//	    toolkit.paintBordersFor(client);
//	    Button b = toolkit.createButton(client, "Do something", SWT.PUSH); //$NON-NLS-1$
//	    gd = new GridData(GridData.VERTICAL_ALIGN_BEGINNING);
//	    b.setLayoutData(gd);
//	    section.setClient(client);
	    
//		viewer =  new TextViewer(parent, SWT.NONE);
//		viewer.setInput(new Document());
//		
//		viewer.addTextListener(new ITextListener() {
//			
//			@Override
//			public void textChanged(TextEvent event) 
//			{
//				ExamSheet active = Activator.modelControl.getExam().getActive();
//				if (active!=null) {
//					String text = ((Document)viewer.getInput()).get();
//					active.setComment(text);
//				}
//			}
//		});
	}

	@Override
	public void setFocus() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void documentUpdated(DocumentUpdateEvent ev) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void modelChanged(ModelChangedEvent event) {
		// TODO Auto-generated method stub
		
	}

}
