package views.exam;

import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.ListViewer;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.IMemento;
import org.eclipse.ui.IViewSite;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.ViewPart;

import annotationide.Activator;

import utils.model.IModelChangedListener;
import utils.model.ModelChangedEvent;
import views.CustomEditorInput;

import editor.ExamEditor;

/**
 * 
 *
 */
public class DirListView extends ViewPart 
implements ISelectionChangedListener, IModelChangedListener 
{
	private String iDirname = null;
	private IMemento iMemento = null;
	private ListViewer iViewer;

	public static final String ID = "annotationIDE.views.dirView";
    
	/**
	 * 
	 */
	public DirListView() 
	{
		super();
		
		//register with the model
		Activator.modelControl.addModelChangedListener(this);
	}

	public void init(IViewSite site, IMemento memento) throws PartInitException 
	{
		iMemento = memento;
		super.init(site, memento);
		
		Activator.modelControl.addModelChangedListener(this);
	}

	public void createPartControl(Composite parent) 
	{
		iViewer = new ListViewer(parent);
		iViewer.setContentProvider(new DirListProvider());
		iViewer.setLabelProvider(new LabelProvider());
		restoreState();
		iViewer.setInput(iDirname);
		getSite().setSelectionProvider(iViewer);
//		makeActions();
//		hookContextMenu();
//		contributeToActionBars();
		
		iViewer.addSelectionChangedListener(this);
	}

	private void restoreState() {
		if (iMemento == null) {
			if (iDirname == null) {
				iDirname = System.getProperty("user.home");
			}
			return;
		}
		IMemento dirname = iMemento.getChild("directory");
		if (dirname != null) {
			iDirname = dirname.getID();
		}
	}

	public void saveState(IMemento memento) {
		memento.createChild("directory",iDirname);
		super.saveState(memento);
	}

	@Override
	public void setFocus() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void selectionChanged(SelectionChangedEvent event) 
	{
		StructuredSelection ss = (StructuredSelection) event.getSelection();
		String file = (String) ss.getFirstElement();
		//send selection to pdf editor
		IWorkbenchPage page = getSite().getPage();
		page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();

		try
		{
			page.openEditor(new CustomEditorInput(file), ExamEditor.ID);
		}
		catch (PartInitException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void modelChanged(ModelChangedEvent event) {
		//simply refresh
		iViewer.setInput(Activator.modelControl.getExam());
		
	}

}
