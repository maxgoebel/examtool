package views.exam;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IMenuListener;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.dialogs.IInputValidator;
import org.eclipse.jface.dialogs.InputDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IMemento;
import org.eclipse.ui.IViewSite;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.ViewPart;

import utils.model.ExamModel;
import utils.model.ExamSheet;
import utils.model.IModelChangedListener;
import utils.model.ModelChangedEvent;
import views.actions.FindExamAction;
import annotationide.Activator;
import editor.ExamEditor;

/**
 * 
 * @author max
 *
 */
public class ExamNavigatorView extends ViewPart 
implements ISelectionChangedListener, IModelChangedListener 
{
	private ExamSheet activeSheet;
	private ExamSheet prevSheet;
	
	private String iDirname = null;
	private IMemento iMemento = null;
	private TableViewer iViewer;

	private Action renameItemAction;
	
	public static final String ID = "annotationIDE.examView";
    
	/**
	 * Constructor.
	 */
	public ExamNavigatorView() 
	{
		super();
	}

	/**
	 * 
	 */
	public void init(IViewSite site, IMemento memento) throws PartInitException 
	{
		iMemento = memento;
		super.init(site, memento);
		getSite().getShell().getDisplay().addFilter(SWT.KeyDown, new Listener() {

            public void handleEvent(Event e) {
                if(((e.stateMask & SWT.CTRL) == SWT.CTRL) && (e.keyCode == 'f'))
                {
                	if (iViewer.getInput()!=null) {
                		FindExamAction fea = new FindExamAction("Find Exam", getSite().getWorkbenchWindow());
                		fea.run();
                	}
                }
                else if((e.keyCode == 16777217)) //up arrow
                {
                	if (iViewer.getInput()!=null) 
                	{
                		ExamModel model = (ExamModel) iViewer.getInput();
                		
                		StructuredSelection ssel = (StructuredSelection) iViewer.getSelection();
                		ExamSheet sheet = (ExamSheet) ssel.getFirstElement();
                		ExamSheet prev = null;
                		for (int i=0; i<model.getPages().length; i++)
                		{
                			if (sheet.equals(model.getPages()[i]))
                			{
                				if (i>0)
                				{
                					prev = model.getPages()[i-1];
                				}
                				break;
                			}
                		}
                		if(prev!=null)
                		{
                			ISelection selection = new StructuredSelection(prev);
                			iViewer.setSelection(selection, true);
                		}
                	}
                }
                else if((e.keyCode == 16777218)) //down arrow
                {
                	if (iViewer.getInput()!=null) 
                	{
                		ExamModel model = (ExamModel) iViewer.getInput();
                		
                		StructuredSelection ssel = (StructuredSelection) iViewer.getSelection();
                		ExamSheet sheet = (ExamSheet) ssel.getFirstElement();
                		ExamSheet next = null;
                		for (int i=0; i<model.getPages().length; i++)
                		{
                			if (sheet.equals(model.getPages()[i]))
                			{
                				if (i<model.getPages().length-1)
                				{
                					next = model.getPages()[i+1];
                				}
                				break;
                			}
                		}
                		if(next!=null)
                		{
                			ISelection selection = new StructuredSelection(next);
                			iViewer.setSelection(selection, true);
                		}
                	}
                }
            }
        });
	}

	public void createPartControl(Composite parent) 
	{
		Activator.modelControl.addModelChangedListener(this);
		
		iViewer = new TableViewer(parent);
		iViewer.setContentProvider(new ExamContentProvider());
		iViewer.setLabelProvider(new ExamSheetLabelProvider());
		restoreState();
		
		getSite().setSelectionProvider(iViewer);
		makeActions();
//		hookContextMenu();
//		contributeToActionBars();
		
		iViewer.addSelectionChangedListener(this);
		createContextMenu();
	}

	private void makeActions()
	{
		renameItemAction = new Action("Matrikelnummer ändern") 
		{
			@Override
			public void run() 
			{
				InputDialog dlg = new InputDialog(Display.getCurrent().getActiveShell(),
						"", "Korrekte Matrikelnummer:", "", new LengthValidator());
				if (dlg.open() == Window.OK)
				{
					StructuredSelection ss = (StructuredSelection) iViewer.getSelection();
					ExamSheet sheet =  (ExamSheet) ss.getFirstElement();
					sheet.setStudentID(dlg.getValue()+"");
					iViewer.refresh();
				}
			}
		};
		updateActionEnablement();
	}
	
	private void restoreState() {
		if (iMemento == null) {
			if (iDirname == null) {
				iDirname = System.getProperty("user.home");
			}
			return;
		}
		IMemento dirname = iMemento.getChild("directory");
		if (dirname != null) {
			iDirname = dirname.getID();
		}
	}

	public void saveState(IMemento memento) {
		memento.createChild("directory",iDirname);
		super.saveState(memento);
	}

	@Override
	public void setFocus() {		
	}

	@Override
	public void selectionChanged(SelectionChangedEvent event) 
	{
		StructuredSelection ss = (StructuredSelection) event.getSelection();
		ExamSheet sheet =  (ExamSheet) ss.getFirstElement();
		prevSheet = activeSheet;
		activeSheet = sheet;
		
		Activator.modelControl.getExam().setActive(activeSheet);
		
		//send selection to pdf editor
		IWorkbenchPage page = getSite().getPage();
		page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();

		if(page!=null && sheet!=null) 
		{
			try
			{
				page.closeAllEditors(true);
				ExamEditorInput input = new ExamEditorInput(sheet);
				IEditorPart editor = page.openEditor(input, ExamEditor.ID,true);
				ExamEditor we = (ExamEditor) editor;
				we.setInput2(input);
			}
			catch (PartInitException e) {
				e.printStackTrace();
			}
		}
//		iViewer.setSelection(ss, true);
		updateActionEnablement();
	}
	
	/**
	 * 
	 */
	private void createContextMenu() 
	{
		// Create menu manager.
		MenuManager menuMgr = new MenuManager();
		menuMgr.setRemoveAllWhenShown(true);
		menuMgr.addMenuListener(new IMenuListener() {
			public void menuAboutToShow(IMenuManager mgr) {
				fillContextMenu(mgr);
			}
		});

		// Create menu.
		iViewer.getControl().setMenu(
				menuMgr.createContextMenu(iViewer.getControl()));

		// Register menu for extension.
		getSite().registerContextMenu(menuMgr, iViewer);
	}
	
	/**
	 * 
	 * @param mgr
	 */
	private void fillContextMenu(IMenuManager mgr) 
	{
		mgr.add(renameItemAction);
	}
	
	private void updateActionEnablement() {
		IStructuredSelection sel = 
				(IStructuredSelection)iViewer.getSelection();
		renameItemAction.setEnabled(sel!=null && sel.size() > 0);
	}
	
	public void refresh() {
		iViewer.setInput(Activator.modelControl.getExam());
	}
	
	@Override
	public void modelChanged(ModelChangedEvent event)
	{
		IWorkbenchPage page = getSite().getPage();
		IWorkbenchWindow wwin = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
		if (wwin!=null)
		{
			page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
			if (page!=null) {
				page.closeAllEditors(true);
			}
		}
				
		ExamModel exam = event.getModel();

		//simply refresh
		iViewer.setInput(exam);
		iViewer.refresh();
	}
	
	public ExamSheet getActiveSheet() {
		return activeSheet;
	}
	
	public ExamSheet getPrevSheet() {
		return prevSheet;
	}
	
	/**
	 * This class validates a String. It makes sure that the String is between 5 and 8
	 * characters
	 */
	class LengthValidator implements IInputValidator {
	  /**
	   * Validates the String. Returns null for no error, or an error message
	   * 
	   * @param newText the String to validate
	   * @return String
	   */
	  public String isValid(String newText) {
	    int len = newText.length();

	    // Determine if input is too short or too long
	    if (len != 7) return "Bitte genau 7 Stellen eingeben";

	    // Input must be OK
	    return null;
	  }
	}

}
