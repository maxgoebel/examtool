package views.exam;

import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.Viewer;

import utils.model.ExamModel;


/**
 * 
 * @author max
 *
 */
public class ExamContentProvider implements IStructuredContentProvider
{
//	private String iDirname = null;

	ExamModel model;
	
	public ExamContentProvider() {
		super();
	}

	public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
		model = (ExamModel) newInput;
	}

	public Object[] getElements(Object inputElement) 
	{
		if (model == null)
			return null;
		
		return model.getPages();
//		File dir = new File(iDirname);
//		FilenameFilter filter = new FilenameFilter() {
//			public boolean accept(File directory, String filename) {
//				if (filename.endsWith("pdf"))
//					return true;
//				else
//					return false;
//			}
//		};
//		String[] dirList = null;
//		if (dir.isDirectory()) {
//			dirList = dir.list(filter);
//			for (int i=0; i<dirList.length;++i) {
//				dirList[i] = iDirname + File.separatorChar + dirList[i];
//			}
//
//		}
//		return dirList;
	}
	
	@Override
	public void dispose() {

	}
}