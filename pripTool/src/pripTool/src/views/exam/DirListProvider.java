package views.exam;

import java.io.File;
import java.io.FilenameFilter;

import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.Viewer;

/**
 * 
 * @author max
 *
 */
public class DirListProvider implements IStructuredContentProvider
{
	private String iDirname = null;

	public DirListProvider() {
		super();
	}

	public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
		iDirname = (String) newInput;
	}

	public Object[] getElements(Object inputElement) 
	{
		if (iDirname == null)
			return null;
		
		File dir = new File(iDirname);
		FilenameFilter filter = new FilenameFilter() {
			public boolean accept(File directory, String filename) {
				if (filename.endsWith("pdf"))
					return true;
				else
					return false;
			}
		};
		String[] dirList = null;
		if (dir.isDirectory()) {
			dirList = dir.list(filter);
			for (int i=0; i<dirList.length;++i) {
				dirList[i] = iDirname + File.separatorChar + dirList[i];
			}

		}
		return dirList;
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		
	}
}