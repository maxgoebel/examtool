package views.exam;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IPersistableElement;

import utils.model.ExamSheet;

import annotationide.Activator;

public class ExamEditorInput implements IEditorInput {

	private ExamSheet sheet;

	public ExamEditorInput(ExamSheet sheet) {
		this.sheet = sheet;
	}
	
	public ExamSheet getSheet() {
		return sheet;
	}
	
	@Override
	public Object getAdapter(Class adapter) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean exists() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public ImageDescriptor getImageDescriptor() {
		 return Activator.getImageDescriptor("/icons/someicon.png");
	}

	@Override
	public String getName() {
		return sheet.toString();
	}

	@Override
	public IPersistableElement getPersistable() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getToolTipText() {
		return "PDF Editor";
	}

}

