package views.exam;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.swt.graphics.Image;

import utils.model.ExamSheet;

/**
 * 
 * @author max
 *
 */
public class ExamSheetLabelProvider extends LabelProvider {

	public ExamSheetLabelProvider() {
	}
	
	@Override
	public Image getImage(Object element) 
	{
		if (element instanceof ExamSheet) 
		{
			ExamSheet sheet = (ExamSheet) element;
			if (sheet.getStatus()==3) 
			{
				 ImageDescriptor id = annotationide.Activator.getImageDescriptor("/icons/green.png");
				 return id.createImage();
			}
			else if (sheet.getPageNum()==0 && sheet.getQrCode()==null) 
			{
				 ImageDescriptor id = annotationide.Activator.getImageDescriptor("/icons/red.png");
				 return id.createImage();
			}
			else if (sheet.getStatus()==0) 
			{
				 ImageDescriptor id = annotationide.Activator.getImageDescriptor("/icons/inProgress.png");
				 return id.createImage();
			}
			else if (sheet.getStatus()==2) 
			{
				 ImageDescriptor id = annotationide.Activator.getImageDescriptor("/icons/yellow.png");
				 return id.createImage();
			}
			
		}
		return super.getImage(element);
	}
	
	@Override
	public String getText(Object element) {
		return super.getText(element);
	}
	
}
