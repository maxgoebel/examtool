package editor;


import java.util.BitSet;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.jface.action.IMenuListener;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.dialogs.ErrorDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Cursor;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.ui.IActionBars;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.IViewPart;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.EditorPart;

import utils.PDFUtils;
import utils.document.DocumentController;
import utils.document.DocumentUpdate;
import utils.document.IDocumentUpdateListener;
import utils.model.ExamSheet;
import views.CommentViewer;
import views.exam.ExamEditorInput;
import views.exam.ExamNavigatorView;
import annotationide.PDFViewerSWT;

/**
 * ExamEditor.java
 *
 *
 * @author mcg <mcgoebel@gmail.com>
 * @date May 25, 2011
 */
public class ExamEditor extends BaseEditor
implements IMenuListener
{ 
	public static String ID = "at.tuwien.prip.docwrap.ide.wrapperEditor";

	//	private DocumentCollectionContainer dcc;

	private Map<EditorPart, Integer> editor2PageIndex;
	private BitSet activeEditors = new BitSet();


	/* A PDF viewer */
	protected PDFViewerSWT pdfSWTViewer;

	//	private final WrapperBuilder wrapperBuilder;

	/* SINGLETON selection and document controllers */
	public static final DocumentController documentControl = new DocumentController();

	/**
	 * Constructor.
	 * 
	 * @param site
	 * @param input
	 */
	public ExamEditor(IEditorSite site, IEditorInput input) 
	{
		this();

		try 
		{
			init(site, input);
		} 
		catch (PartInitException e) 
		{
			e.printStackTrace();
		}
	}

	/**
	 * 
	 */
	public void setInput2(IEditorInput input)
	{
		if (input instanceof ExamEditorInput)
		{
			ExamEditorInput eei = (ExamEditorInput) input;
			ExamSheet insheet = eei.getSheet();
			ImageData data = insheet.getImgData();
			if (data==null) 
			{
				data = PDFUtils.getImageFromPDFPage(insheet.getPage(),1.175, true); //1.175
//				insheet.setImgData(data);
			}

			//search for Comment View
			//send selection to PDF editor
			IWorkbenchPage page = getSite().getPage();
			page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
			if(page!=null) 
			{
				ExamSheet prevSheet = null;
				IViewPart navView = page.findView(ExamNavigatorView.ID);
				if (navView!=null) {
					prevSheet = ((ExamNavigatorView) navView).getPrevSheet();
				}

				IViewPart commView = page.findView(CommentViewer.ID);
				if (commView!=null) 
				{
					String comment = ((CommentViewer) commView).getComment();

					if (prevSheet!=null) {
						prevSheet.setComment(comment);
					}
					((CommentViewer) commView).resetInput(insheet);
				}
			}

			pdfSWTViewer.setExamSheet(insheet);
			pdfSWTViewer.getCanvas().setImageData(data, true);
		}
	}

	/**
	 * Constructor.
	 */
	public ExamEditor() 
	{
		super();

		editor2PageIndex = new HashMap<EditorPart, Integer>();

		setTitleToolTip("Exam Editor");
	}

	/**
	 * 
	 * @param message
	 */
	public void setStatusLine(String message) 
	{
		// Get the status line and set the text
		IActionBars bars = getEditorSite().getActionBars();
		bars.getStatusLineManager().setMessage(message);
	}

	@Override
	public void showBusy(boolean busy) 
	{
		super.showBusy(busy);
		/* change cursor to/from busy */
		Cursor busyCursor;
		if (busy) {
			busyCursor = new Cursor(getSite().getShell().getDisplay(), SWT.CURSOR_WAIT);
		} else {
			busyCursor = new Cursor(getSite().getShell().getDisplay(), SWT.CURSOR_ARROW);
		}
		getSite().getShell().setCursor(busyCursor);
	}

	@Override
	public void dispose() 
	{
		super.dispose();

		deregisterFromDocumentUpdate(pdfSWTViewer);
		documentControl.removeDocumentUpdateListener(pdfSWTViewer);
	}

	@Override
	protected void createPages() 
	{
		createPage1();
	}

	/**
	 * Setup PDF editor.
	 */
	private void createPage1() 
	{
		try 
		{
			pdfSWTViewer = new PDFViewerSWT(getEditorSite(),getEditorInput());
			pdfSWTViewer.setWrapperEditor((ExamEditor) this);
			documentControl.addDocumentUpdateListener(pdfSWTViewer);

			int index = addPage(pdfSWTViewer, getEditorInput());
			setPageText(index, "PDF Viewer");

			editor2PageIndex.put(pdfSWTViewer, index);
			activeEditors.set(index);

//			createAndRegisterContextMenuFor();
			
		} catch (PartInitException e) {
			ErrorDialog.openError(
					getSite().getShell(),
					"Error creating nested text editor",
					null,
					e.getStatus());
		}
	}

	/**
	 * 
	 * @param update
	 */
	public void setDocumentUpdated (DocumentUpdate update) 
	{
		documentControl.setDocumentUpdate(update);
	}


	/**
	 * This creates a context menu for the viewer and adds a
	 * listener as well registering the menu for extension.
	 */
	//	@SuppressWarnings("restriction")
	public Menu createAndRegisterContextMenuFor(Control control)
	{
		//EMF drag&drop support
		//		super.addEMFDragDropSupportFor(viewer);
		//drag&drop support
		//		int dndOperations = DND.DROP_COPY | DND.DROP_MOVE | DND.DROP_LINK;
		//		Transfer[] transfers = new Transfer[] { LocalTransfer.getInstance() };
		//		viewer.addDragSupport(dndOperations, transfers, new ViewerDragAdapter(viewer));

		MenuManager contextMenu = new MenuManager("#PopUp");
		//contextMenu.add(new Separator("additions"));
		contextMenu.setRemoveAllWhenShown(true);
		contextMenu.addMenuListener(this);
		Menu menu= contextMenu.createContextMenu(control);
		control.setMenu(menu);
//		getSite().registerContextMenu(contextMenu, control);

		MenuItem fileSaveItem = new MenuItem(menu, SWT.PUSH);
		fileSaveItem.setText("&MAX");
		//		IWorkbenchPartSite site = getSite();
		//		PartSite.registerContextMenu(site.getId(),
		//				contextMenu,
		//				viewer,
		//				false,
		//				site.getPart(),
		//				new ArrayList<PopupMenuExtender>(1));

		return menu;
	}

//	private long lastStart;
	//	public void delayedSelect(final Command cmd, final boolean alsoInOutline)
	//	{
	//		final long start = System.currentTimeMillis();
	//		lastStart = start;
	//
	//		final Display d = Display.getCurrent();
	//		Thread t = new Thread() {
	//			@Override
	//			public void run() {
	//				try { sleep(400); } catch (InterruptedException e) { ErrorDump.error(this, e); }
	//				//check if not canceled
	//				if (lastStart<0) return;
	//				//check if a new selection was started
	//				if (lastStart>start) return;
	//
	//				d.syncExec(new Runnable() { public void run() {
	//					immediateSelect(cmd, alsoInOutline);
	//				}});
	//			}};
	//			t.start();
	//	}
	//
	//	public void immediateSelect(Command cmd, boolean alsoInOutline)
	//	{
	//		ISelection sel = new StructuredSelection(cmd);
	//
	//		//change properties view
	//		ISelection esel = getSelection();
	//		if (esel==null || !sel.equals(esel)) {
	//			setSelection(sel);
	//		}
	//	}



	public void registerForDocumentUpdate (IDocumentUpdateListener listener) {
		documentControl.addDocumentUpdateListener(listener);
	}

	public void deregisterFromDocumentUpdate (IDocumentUpdateListener listener) {
		documentControl.removeDocumentUpdateListener(listener);
	}


	/**
	 * This implements {@link org.eclipse.jface.action.IMenuListener}
	 * to help fill the context menus with contributions from the Edit menu.
	 */
	//OK
	public void menuAboutToShow(IMenuManager menuManager) {
		((IMenuListener) getEditorSite().getActionBarContributor()).
		menuAboutToShow(menuManager);
	}

	//	public DocumentEntry getCurrentDocumentEntry () {
	//		return documentControl.getCurrentDocumentEntry();
	//	}

	public int getCurrentDocumentPage () {
		return documentControl.getCurrentDocumentPage();
	}

	//	public WrapperBuilder getWrapperBuilder() {
	//		return wrapperBuilder;
	//	}

	//	@Override
	//	public void resourceChanged(IResourceChangeEvent event) {
	//		// Auto-generated method stub
	//
	//	}

	public PDFViewerSWT getPdfViewer() {
		return pdfSWTViewer;
	}


}//WrapperEditor
