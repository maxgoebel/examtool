package editor;



import org.eclipse.core.resources.IMarker;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.IFileEditorInput;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.ide.IDE;
import org.eclipse.ui.part.MultiPageEditorPart;


/**
 * MpMozBrowserEditor.java
 *
 *
 * @author mcg <mcgoebel@gmail.com>
 * @date May 27, 2011
 */
public abstract class MpBaseEditor extends MultiPageEditorPart 
{

	public static String ID = "at.tuwien.prip.docwrap.ide.mpWebEditor";

	
	/**
	 * Constructor.
	 */
	public MpBaseEditor() 
	{
		super();
		
//		//override default XMIimplementation to add DOM persistance
//		Resource.Factory.Registry.INSTANCE.getExtensionToFactoryMap().
//		put(mt._UI_WrapperEditor_FilenameExtension,	new XMIResourceWithDOMFactoryImpl());
	}

	/**
	 * Constructor.
	 */
	public MpBaseEditor(IEditorSite site, IEditorInput input) {
		this();
	}	

	@Override
	protected void pageChange(int newPageIndex) {

		IEditorPart activeEditor = getEditor(newPageIndex);
		if (activeEditor != null) {

			IToolBarManager mgr = getEditorSite().getActionBars().getToolBarManager();
			mgr.removeAll();
			mgr.update(true);
		}

		super.pageChange(newPageIndex);
	}

	/**
	 * The <code>MultiPageEditorPart</code> implementation of this 
	 * <code>IWorkbenchPart</code> method disposes all nested editors.
	 * Subclasses may extend.
	 */
	public void dispose() {
		super.dispose();
	}
	/**
	 * Saves the multi-page editor's document.
	 */
	public void doSave(IProgressMonitor monitor) {
		getEditor(0).doSave(monitor);
	}
	/**
	 * Saves the multi-page editor's document as another file.
	 * Also updates the text for page 0's tab, and updates this multi-page editor's input
	 * to correspond to the nested editor's.
	 */
	public void doSaveAs() {
		IEditorPart editor = getEditor(0);
		editor.doSaveAs();
		setPageText(0, editor.getTitle());
		setInput(editor.getEditorInput());
	}
	/* (non-Javadoc)
	 * Method declared on IEditorPart
	 */
	public void gotoMarker(IMarker marker) {
		setActivePage(0);
		IDE.gotoMarker(getEditor(0), marker);
	}
	/**
	 * The <code>MultiPageEditorExample</code> implementation of this method
	 * checks that the input is an instance of <code>IFileEditorInput</code>.
	 */
	public void init(IEditorSite site, IEditorInput editorInput)
	throws PartInitException {

		setSite(site);
		setInput(editorInput);

		if (!(editorInput instanceof IFileEditorInput))
		{
			throw new PartInitException("Invalid Input: Must be IFileEditorInput");
		}

	}

	/* (non-Javadoc)
	 * Method declared on IEditorPart.
	 */
	public boolean isSaveAsAllowed() {
		return true;
	}

}
