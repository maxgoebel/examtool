package annotationide;

import java.awt.geom.AffineTransform;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.Label;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.ISelectionProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.events.MouseMoveListener;
import org.eclipse.swt.events.MouseWheelListener;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.IViewReference;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.part.EditorPart;

import utils.IPlotMouseListener;
import utils.PDFUtils;
import utils.SWT2Dutil;
import utils.document.DocumentController;
import utils.document.DocumentModel;
import utils.document.DocumentUpdate;
import utils.document.DocumentUpdate.UpdateType;
import utils.document.DocumentUpdateEvent;
import utils.document.IDocumentUpdateListener;
import utils.model.ExamModel;
import utils.model.ExamSheet;
import utils.model.IModelChangedListener;
import utils.model.ModelChangedEvent;
import views.CustomEditorInput;
import views.exam.ExamEditorInput;
import views.exam.ExamNavigatorView;

import com.sun.pdfview.PDFFile;
import com.sun.pdfview.PDFPage;

import editor.ExamEditor;

/**
 * 
 * PDFViewerSWT.java
 *
 *
 * @author mcg <mcgoebel@gmail.com>
 * @date Oct 1, 2011
 */
public class PDFViewerSWT extends EditorPart 
implements ISelectionProvider, IDocumentUpdateListener, IModelChangedListener
{	
	public ExamEditor we;

	private int currentPage, maxPage;

	public static PDFPage page = null;

	private SWTImageCanvas canvas = null;

	private IStructuredSelection activeSelection;

	private List<ISelectionChangedListener> listeners;

	private double zoomLevel = 1d;

	private ExamModel model = null;

	private ExamSheet examSheet;

	private PDFFile pdfFile;

	private Composite parent;

	private static boolean contrast = true;

	private Rectangle selectedRectangle = null;
	
	/* buttons */
	private Button prevPageButton = null;
	private Button nextPageButton = null;


	/* actions */
	Action showDocumentAction;
	Action toggleEdgesAction;
	Action toggleNodesAction;
	Action selectAllAction;
	Action nextPageAction;
	Action prevPageAction;
	Action resizeAction;
	org.eclipse.swt.widgets.Label showPageNums;

	private Composite group1;

	boolean isAnno = false; boolean isWrapper = false;

	public static final String ID = "annotationIDE.pdfEditor";
	/**
	 * Constructor.
	 * 
	 * @param parent
	 * @param site
	 * @param input
	 */
	public PDFViewerSWT(IEditorSite site, IEditorInput input) 
	{
		super.setSite(site);
		setPartName("Document View");
		setTitleToolTip("test");

		if (input instanceof CustomEditorInput) 
		{
			CustomEditorInput in = (CustomEditorInput) input;
			String fileName = in.getUri();

			try 
			{
				RandomAccessFile raf = new RandomAccessFile (fileName, "r");
				FileChannel fc = raf.getChannel ();
				ByteBuffer buf = fc.map (FileChannel.MapMode.READ_ONLY, 0, fc.size ());
				PDFFile pdfFile = new PDFFile (buf);

				page = pdfFile.getPage(0);
				zoomLevel = 1.1;
				
				raf.close();
			}
			catch (FileNotFoundException e) {
				e.printStackTrace();
			} 
			catch (IOException e) {
				e.printStackTrace();
			}
		}
		else if (input instanceof ExamEditorInput) {
			//			ExamEditorInput eei = (ExamEditorInput) input;
			this.examSheet = ((ExamEditorInput) input).getSheet();
		}
		listeners = new ArrayList<ISelectionChangedListener>();
	}

	@Override
	public void dispose() 
	{
		Activator.modelControl.removeModelChangedListener(this);
		this.canvas.dispose();
//		this.we.dispose();
		super.dispose();
	}

	@Override
	public void createPartControl(Composite parent) 
	{
		this.parent = parent;

		setPartName("Document View");
		Activator.modelControl.addModelChangedListener(this);

		/* BEGIN CONTENT */
		GridLayout layout = new GridLayout();
		GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, false);
		gridData.heightHint = 40;

		parent.setLayout(layout);

		//add a control panel
		//		Composite controlPanel = createControlPanel(parent);	
		//		controlPanel.setLayoutData(gridData);
		//		controlPanel.pack();

		//add the pdf canvas
		gridData = new GridData(SWT.FILL, SWT.FILL, true, true);
		gridData.widthHint = 900;
		gridData.heightHint = 680;

		canvas = new SWTImageCanvas(
				this,
				parent, 
				SWT.NO_BACKGROUND | SWT.NO_REDRAW_RESIZE | SWT.V_SCROLL | SWT.H_SCROLL);

		canvas.setLayoutData(gridData);
		canvas.pack();
		canvas.addPlotMouseListener(new PlotMouseListener(canvas));
		canvas.addKeyListener(new PlotKeyListener());

		if (page!=null) 
		{
			getSite().getShell().getDisplay().asyncExec(
					new Runnable() 
					{
						public void run() 
						{
							ImageData data = PDFUtils.getImageFromPDFPage(page,zoomLevel, contrast);
							canvas.setImageData(data, true);
						}
					}
					);
		}

		if (model==null) 
		{
			model = Activator.modelControl.getExam();
			if (model.getSelectionBoxes()!=null && model.getSelectionBoxes().size()==1)
			{
				List<Rectangle> tmpBoxes = model.getSelectionBoxes().get(1);

				List<Rectangle> highlightBoxes = new ArrayList<Rectangle>();
				for (Rectangle r : tmpBoxes)
				{
					ExamSheet sheet = model.getPages()[0];

					java.awt.Rectangle b = sheet.getPage().getBBox().getBounds();
					int yNew = (b.y+b.height) - r.y - 20;
					Rectangle s = new Rectangle(r.x - 10, yNew, r.width + 15, r.height);
					//				(bounds.y + bounds.height) - 
					//					node.setSegY1( Math.abs(y2 - node.getSegY1()) );
					//					node.setSegY2( Math.abs(y2 - node.getSegY2()) );

					highlightBoxes.add(new Rectangle(s.x, s.y, s.width, s.height));
				}
				examSheet.setSelectionBoxes(highlightBoxes);
				examSheet.setHighlightIds(model.getHighlightIds());
				canvas.redraw();
			}
		}
	}

	public void setExamSheet(ExamSheet sheet) {
		this.examSheet = sheet;
	}

	public ExamSheet getExamSheet() {
		return examSheet;
	}

	/**
	 * 
	 * @param uri
	 */
	public void setDocument(String uri) 
	{
		if (page!=null) 
		{
			zoomLevel = 0.6;
			canvas.setImageData(
					PDFUtils.getImageFromPDFPage(page,zoomLevel,contrast),
					false);
		}

		canvas.update();
	}

	/**
	 * 
	 * @param image
	 */
	public void setDocument(Image image) 
	{
		//adjust contrast if necessary
		//		float brightenFactor = 1.2f;
		//		BufferedImage img = image.getAsBufferedImage();
		//
		//		RescaleOp op = new RescaleOp(brightenFactor, 0, null);
		//		img = op.filter(img, img);

		canvas.setImage(image);
		canvas.update();
	}

	@Override
	public void addSelectionChangedListener(ISelectionChangedListener listener) 
	{
		if (!listeners.contains(listener)) {
			listeners.add(listener);
		}
	}

	@Override
	public ISelection getSelection() {
		return activeSelection;
	}

	@Override
	public void removeSelectionChangedListener(
			ISelectionChangedListener listener) 
	{
		if (listeners.contains(listener)) {
			listeners.remove(listener);
		}
	}

	@Override
	public void setSelection(ISelection selection) 
	{
		//inverse transform to rectangle
		Rectangle selRect = (Rectangle) ((IStructuredSelection)selection).getFirstElement();
		AffineTransform transform = canvas.getTransform();
		Rectangle imageRect = SWT2Dutil.inverseTransformRect(transform, selRect);

		selection = new StructuredSelection(imageRect);
		SelectionChangedEvent sce = new SelectionChangedEvent(this, selection);
		if (selection!=activeSelection) {
			for (ISelectionChangedListener listener : listeners) {
				listener.selectionChanged(sce);
			}
			activeSelection = (IStructuredSelection) selection;
		}
	}

	/**
	 * 
	 * @param parent
	 */
	protected Composite createControlPanel (Composite parent) 
	{
		Composite panel = new Composite(parent, SWT.NONE);
		GridLayout layout = new GridLayout();
		GridData rdata =  new GridData(SWT.CENTER,SWT.TOP,true,true);
		panel.setLayoutData(rdata);
		panel.setLayout(layout);

		////////////////////////////////////////////////////////////////
		group1 = new Composite(panel, SWT.NONE);
		GridLayout glayout = new GridLayout(4,true);
		group1.setLayout(glayout);
		group1.setEnabled(false);

		GridData gridData = new GridData();
		gridData.grabExcessHorizontalSpace = true;
		gridData.heightHint = 40;
		gridData.widthHint = 160;
		group1.setLayoutData(gridData);

		gridData = new GridData();
		gridData.heightHint = 30;
		gridData.widthHint = 30;
		gridData.horizontalAlignment = SWT.CENTER;

		prevPageButton = new Button(group1, SWT.ARROW | SWT.LEFT);
		prevPageButton.setText("Previous");
		prevPageButton.setVisible(maxPage>currentPage);
		prevPageButton.setEnabled(currentPage>1);
		prevPageButton.setLayoutData(gridData);
		prevPageButton.addListener(SWT.Selection, new Listener() 
		{
			@Override
			public void handleEvent(Event event) {
				ExamEditor.documentControl.prevPage();
			}
		});

		showPageNums = new org.eclipse.swt.widgets.Label(group1,SWT.NONE);
		showPageNums.setEnabled(false);
		showPageNums.setVisible(false);
		showPageNums.setText((currentPage)+"/"+(maxPage));

		GridData data2 = new GridData(SWT.CENTER,SWT.CENTER,true,true);
		data2.horizontalSpan = 2;
		data2.widthHint = 30;
		data2.horizontalAlignment = SWT.CENTER;
		showPageNums.setLayoutData(data2);

		nextPageButton = new Button(group1, SWT.ARROW | SWT.RIGHT);
		nextPageButton.setText("Next");
		nextPageButton.setVisible(maxPage>currentPage);
		nextPageButton.setEnabled(currentPage<maxPage);
		nextPageButton.setLayoutData(gridData);
		nextPageButton.addListener(SWT.Selection, new Listener() 
		{
			@Override
			public void handleEvent(Event event) {
				ExamEditor.documentControl.nextPage();
			}
		});
		group1.pack();

		panel.pack();
		return panel;
	}

	/**
	 * 
	 */
	private void updateActionEnablement() 
	{
		//		DocumentEntry de = we.getCurrentDocumentEntry();
		//		if (de!=null) 
		//		{
		//			if (group1!=null) {
		//				group1.setEnabled(true);
		//			}
		//
		//
		//			if (showPageNums!=null) {
		//				showPageNums.setVisible(true);
		//				showPageNums.setEnabled(true);
		//				parent.getDisplay().syncExec( new Runnable() {
		//					public void run() {
		//						showPageNums.setText( (currentPage)+"/"+(maxPage) );
		//						showPageNums.redraw();
		//					}
		//				} );
		//
		//				prevPageButton.setVisible(maxPage>1);
		//				prevPageButton.setEnabled(currentPage>1);
		//				nextPageButton.setVisible(maxPage>1);
		//				nextPageButton.setEnabled(currentPage<maxPage);
		//			} 
		//
		//			if (nextPageAction!=null) {
		//				nextPageAction.setEnabled(currentPage!=maxPage);
		//			}
		//			if (prevPageAction!=null) {
		//				prevPageAction.setEnabled(currentPage!=1);
		//			}
		//		} 
		parent.redraw();
		parent.update();
	}

	/****************************************************************
	 * 
	 * Delegate editor functionality to parent...
	 */
	@Override
	public void doSave(IProgressMonitor monitor) {
		//		parent.doSave(monitor);
	}

	@Override
	public void doSaveAs() {
	}

	@Override
	public void init(IEditorSite site, IEditorInput input)
			throws PartInitException {
		setPartName("Document View");
	}

	@Override
	public boolean isDirty() {
		//		return parent.isDirty();
		return false;
	}

	@Override
	public boolean isSaveAsAllowed() {
		//		return parent.isSaveAsAllowed();
		return false;
	}

	@Override
	public void setFocus() {
		canvas.setFocus();	
	}

	public double getZoomLevel() {
		return zoomLevel;
	}

	/**
	 * Deal with document update events.
	 */
	@Override
	public void documentUpdated(DocumentUpdateEvent ev) 
	{	
		//do whatever needs doing...
		DocumentUpdate update = ev.getDocumentUpdate();
		DocumentModel model = update.getUpdate();

		//finally, notify canvas
		if (update.getProvider()!=null && !update.getProvider().equals(canvas))
		{
			canvas.documentUpdated(ev);
		}

		if (ev.getDocumentUpdate().getType()==UpdateType.DOCUMENT_CHANGE) 
		{
			if (model.getPdfFile()!=null)
			{
				PDFPage page = model.getPdfFile().getPage(model.getPageNum());
				ImageData imgData = PDFUtils.getImageFromPDFPage(page, model.getScale(),contrast);
				canvas.setImageData(imgData, false);
			}
			if (model.getPageNum()!=0)
			{
				maxPage = model.getNumPages();
				currentPage = model.getPageNum();
			}
			if (model.getPdfFile()!=null) 
			{
				pdfFile = model.getPdfFile();
				page = pdfFile.getPage(model.getPageNum());
			}
		}
		else if (ev.getDocumentUpdate().getType()==UpdateType.PAGE_CHANGE) 
		{
			currentPage = model.getPageNum();
			maxPage = model.getNumPages();

			zoomLevel = DocumentController.docModel.getScale();

			page = model.getPdfFile().getPage(currentPage, true);
			if (page!=null) 
			{
				getSite().getShell().getDisplay().asyncExec(
						new Runnable() 
						{
							public void run() 
							{
								ImageData data = PDFUtils.getImageFromPDFPage(page,zoomLevel,contrast);
								canvas.setImageData(data, false);
							}
						}
						);
			}
		}
		updateActionEnablement();
	}

	public SWTImageCanvas getCanvas() {
		return canvas;
	}

	public class PlotKeyListener 
	implements KeyListener
	{

		@Override
		public void keyPressed(KeyEvent e) {
			//if (!canvas.hideHighlights) {
			if (e.keyCode==SWT.F2) {
				canvas.hideHighlights(!canvas.hideHighlights);
			}
			if (e.keyCode==SWT.F3) {
				canvas.increaseContrast();
			}
			if (e.keyCode==SWT.F4) {
				canvas.increaseContrast();
				canvas.growColor(12312);
				canvas.contrastChange();
//				canvas.binarize();
			}
			if (e.keyCode==SWT.F5) {
				canvas.binarize();
			}
			if (e.keyCode==SWT.F6) {
				canvas.removeBackground();
			}
			if (e.keyCode==SWT.F7) {
				canvas.growColor(12312);
			}
			if (e.keyCode==SWT.F8) {
				canvas.enhance();
			}
			//}
		}

		@Override
		public void keyReleased(KeyEvent e) {
			//canvas.hideHighlights(false);
		}

	}

	/**
	 * 
	 * A custom mouse and paint Listener for the plotter.
	 */
	public class PlotMouseListener 
	implements MouseListener, MouseMoveListener, MouseWheelListener,
	PaintListener, IPlotMouseListener
	{
		IFigure lastFig = null;
		Color lastFgColor;
		Color lastBgColor;
		Label lastLabel;
		SWTImageCanvas canvas;

		private Rectangle focusRectangle = null;


		private boolean dragOn = false;
		int mouseX = 0;
		int mouseY = 0;

		/**
		 * Constructor.
		 */
		public PlotMouseListener(SWTImageCanvas canvas) 
		{ 
			this.canvas = canvas;
		}

		@Override
		public void mouseMove(MouseEvent e) 
		{
			if (dragOn) 
			{
				focusRectangle.width = (-1) * (focusRectangle.x - e.x);
				focusRectangle.height = (-1) * (focusRectangle.y - e.y);
				mouseX = e.x;
				mouseY = e.y;
				canvas.redraw();
			} 
		}

		@Override
		public void mouseScrolled(MouseEvent e) 
		{		
			canvas.setBackground(ColorConstants.darkGray);
			
			if ((e.stateMask & SWT.CTRL) != 0) 
			{
				if (e.count >=0) 
				{ 
					if (zoomLevel>2) return;
					canvas.zoomIn();
					
					
					zoomLevel = canvas.getZoomLevel();
				}
				else
				{
					if (zoomLevel<0.6) return;
					
					canvas.zoomOut();
					zoomLevel = canvas.getZoomLevel();
					//				zoomLevel = DocumentController.docModel.getScale() - DocumentController.docModel.getScale()/9;
				}
			}
		}

		@Override
		public void mouseDown(MouseEvent e) 
		{
			mouseX = (int) (e.x - canvas.getTransform().getTranslateX());;
			mouseY = (int) (e.y - canvas.getTransform().getTranslateY());;

			canvas.selectionRectangle = null;
			if (e.button==1) {
				focusRectangle = new Rectangle(mouseX, mouseY, 0, 0);
				dragOn = true;
			}

			ExamModel exam = Activator.modelControl.getExam();
			List<Rectangle> rects = exam.getSelectionBoxes().get(examSheet.getPageNum());
			int i=0;
			for (Rectangle r : rects) 
			{
				if (r.contains(mouseX, mouseY)) 
				{
					if (e.button==1) 
					{
						if (!Activator.isChangeModelState())
						{
							if (examSheet.getGreenMarkers().contains(r)) {
								examSheet.getGreenMarkers().remove(r);
								examSheet.getRedMarkers().add(r); //becomes red
								examSheet.setPoint(i, "r");
							}
							else if (examSheet.getRedMarkers().contains(r)) {
								examSheet.getRedMarkers().remove(r);
								examSheet.getYellowMarkers().add(r); //becomes yellow
								examSheet.setPoint(i, "y");
							}
							else if (examSheet.getYellowMarkers().contains(r)) {
								examSheet.getYellowMarkers().remove(r);
								examSheet.getGreenMarkers().add(r); //becomes green
								examSheet.setPoint(i, "g");
							}
							else 
							{
								examSheet.getGreenMarkers().add(r); //becomes green
								examSheet.setPoint(i, "g");
							}
						}
						else
						{
							//modify the selected box
							selectedRectangle = r;
							canvas.redraw();
						}
					} 

					examSheet.setStatus(computeStatus(examSheet));

					//register annotation view as selection listener...
					IWorkbenchPage page = getSite().getPage();
					if (page!=null) 
					{
						IViewReference ref = page.findViewReference(ExamNavigatorView.ID);
						if (ref!=null) 
						{
							ExamNavigatorView env = (ExamNavigatorView) ref.getPart(true);
							env.refresh();
						}
					}
					canvas.redraw();

					break;
				}
				i++;
			}

			//the trick is to remember the original 
			//document transform here as calibration...
			canvas.dtx =  canvas.transform.getTranslateX();
			canvas.dty = canvas.transform.getTranslateY();

			canvas.selTransform  = new AffineTransform();
			canvas.redraw();
		}

		public int computeStatus (ExamSheet sheet) 
		{
			if (sheet.getGreenMarkers().contains(sheet.getSelectionBoxes().get(0)))
			{
				return 3;
			}
//			int allBoxes = sheet.getHighlightBoxes().size();
//			int allMarks = sheet.getGreenMarkers().size() + sheet.getRedMarkers().size() + sheet.getYellowMarkers().size();
//			if(allBoxes == allMarks) {
//				if (sheet.getRedMarkers().size()==allBoxes) {
//					return 1; //all red
//				}
//				if (sheet.getYellowMarkers().size()==allBoxes) {
//					return 2; //all yellow
//				}
//				if (sheet.getGreenMarkers().size()==allBoxes) {
//					return 3; //all green
//				}
//				else return 2;//mixed
//			}
			return 0; //in progress
		}


		@Override
		public void mouseUp(MouseEvent e) 
		{

		}

		@Override
		public void mouseDoubleClick(MouseEvent e) { /* not implemented */ }


		public void paintControl(PaintEvent e)
		{
			if (focusRectangle!=null && focusRectangle.width>0 && focusRectangle.height>0)  
			{			
				//draw focus
				//				e.gc.setForeground(ColorConstants.black);
				//				e.gc.drawFocus(
				//						focusRectangle.x,
				//						focusRectangle.y,
				//						focusRectangle.width,
				//						focusRectangle.height);				
			}
			else 
			{
				if (canvas.selectionRectangle!=null && 
						canvas.selectionRectangle.width>0 && 
						canvas.selectionRectangle.height>0) 
				{
					focusRectangle = null;
				}
			}
		}

		public Rectangle getFocusRectangle()
		{
			return focusRectangle;
		}

	}

	public void setWrapperEditor(ExamEditor wrapperEditor) {
		this.we = wrapperEditor;	
	}

	@Override
	public void modelChanged(ModelChangedEvent event) 
	{
		this.model = event.getModel();

		List<Rectangle> tmpBoxes = model.getInputFields().get(1);
		List<Rectangle> highlightBoxes = new ArrayList<Rectangle>();
		for (Rectangle t : tmpBoxes) {
			highlightBoxes.add(new Rectangle(t.x, t.y, t.width, t.height));
		}
		examSheet.setSelectionBoxes(highlightBoxes);
		canvas.redraw();
	}
	
	public Rectangle getSelectedRectangle() {
		return selectedRectangle;
	}
	
	public void setSelectedRectangle(Rectangle selectedRectangle) {
		this.selectedRectangle = selectedRectangle;
	}

}//PDFViewerSWT
