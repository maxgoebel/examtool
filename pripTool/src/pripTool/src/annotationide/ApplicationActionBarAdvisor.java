package annotationide;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.ICoolBarManager;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.jface.action.ToolBarContributionItem;
import org.eclipse.jface.action.ToolBarManager;
import org.eclipse.swt.SWT;
import org.eclipse.ui.IWorkbenchActionConstants;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.actions.ActionFactory;
import org.eclipse.ui.actions.ActionFactory.IWorkbenchAction;
import org.eclipse.ui.application.ActionBarAdvisor;
import org.eclipse.ui.application.IActionBarConfigurer;

import views.actions.AdjustModelAction;
import views.actions.ExportCSVAction;
import views.actions.FindExamAction;
import views.actions.ImportPointsAction;
import views.actions.LoadExamAction;
import views.actions.LoadHeaderAction;
import views.actions.LoadNewXmlModelAction;
import views.actions.ValidateExamAction;

/**
 * An action bar advisor is responsible for creating, adding, and disposing of the
 * actions added to a workbench window. Each window will be populated with
 * new actions.
 */
public class ApplicationActionBarAdvisor extends ActionBarAdvisor 
{
	// Actions - important to allocate these only in makeActions, and then use them
	// in the fill methods.  This ensures that the actions aren't recreated
	// when fillActionBars is called with FILL_PROXY.
	private IWorkbenchAction exitAction;
	private IWorkbenchAction aboutAction;
	private IWorkbenchAction newWindowAction;
	//    private OpenViewAction openViewAction;
	//    private Action messagePopupAction;
	private Action loadExamAction;
	private Action loadXmlAction;
	private Action validateExamAction;
	//    private Action saveAsExamAction;
	//    private Action saveExamAction;
	private Action importPointsAction;
	private Action exportToCSVAction;
	private Action findExamAction;
	private Action loadHeaderAction;
	private Action adjustModelAction;
//	private Action extractFromPDFAction;


	public ApplicationActionBarAdvisor(IActionBarConfigurer configurer) 
	{
		super(configurer);
	}

	protected void makeActions(final IWorkbenchWindow window)
	{
		// Creates the actions and registers them.
		// Registering is needed to ensure that key bindings work.
		// The corresponding commands keybindings are defined in the plugin.xml file.
		// Registering also provides automatic disposal of the actions when
		// the window is closed.

		exitAction = ActionFactory.QUIT.create(window);
		register(exitAction);

		aboutAction = ActionFactory.ABOUT.create(window);
		register(aboutAction);

		newWindowAction = ActionFactory.OPEN_NEW_WINDOW.create(window);
		register(newWindowAction);

		//        openViewAction = new OpenViewAction(window, "Open Another Message View", View.ID);
		//        register(openViewAction);
		//        
		//        messagePopupAction = new MessagePopupAction("Open Message", window);
		//        register(messagePopupAction);

		loadExamAction = new LoadExamAction("Load Exam", window);
		loadXmlAction = new LoadNewXmlModelAction("Load XML Model", window);
		exportToCSVAction = new ExportCSVAction("Export to CSV", window);
		importPointsAction = new ImportPointsAction("Import Points", window);
		//        saveAsExamAction = new SaveAsExamAction("Save As", window);
		//        saveExamAction = new SaveExamAction("Save Exam", window);
		findExamAction = new FindExamAction("Find Exam", window);
		loadHeaderAction = new LoadHeaderAction("Load CSV Header", window);
		validateExamAction = new ValidateExamAction("Validate Exam", window);
		adjustModelAction = new AdjustModelAction("Adjust Model", window);
		
//		adjustModelAction.setEnabled(false);
//		extractFromPDFAction = new ExtractFromPDFAction("Extract Model from PDF", window);
//		extractFromPDFAction.setEnabled(false);
	}

	@Override
	protected void fillMenuBar(IMenuManager menuBar)
	{
		MenuManager fileMenu = new MenuManager("&File", IWorkbenchActionConstants.M_FILE);
		MenuManager toolsMenu = new MenuManager("&Tools", "Tools");

		menuBar.add(fileMenu);
		menuBar.add(toolsMenu);

		// File
		fileMenu.add(loadExamAction);
		fileMenu.add(new Separator());
		fileMenu.add(importPointsAction);
		fileMenu.add(exportToCSVAction);
		fileMenu.add(new Separator());
		fileMenu.add(findExamAction);
		fileMenu.add(new Separator());
		fileMenu.add(exitAction);
		
		// Tools
		toolsMenu.add(validateExamAction);
		toolsMenu.add(loadHeaderAction);
		toolsMenu.add(loadXmlAction);
		toolsMenu.add(adjustModelAction);
	}

	protected void fillCoolBar(ICoolBarManager coolBar) 
	{
		IToolBarManager toolbar = new ToolBarManager(SWT.FLAT | SWT.RIGHT);
		coolBar.add(new ToolBarContributionItem(toolbar, "main"));   
		toolbar.add(loadExamAction);
	}
}
