package annotationide;

import org.eclipse.ui.IPageLayout;
import org.eclipse.ui.IPerspectiveFactory;


import views.CommentViewer;
//import views.NavigationView;
import views.exam.ExamNavigatorView;

public class Perspective implements IPerspectiveFactory {

	/**
	 * The ID of the perspective as specified in the extension.
	 */
	public static final String ID = "annotationIDE.perspective";

	public void createInitialLayout(IPageLayout layout)
	{
		String editorArea = layout.getEditorArea();
		layout.setEditorAreaVisible(true);
		
		layout.addStandaloneView(ExamNavigatorView.ID,  false, IPageLayout.LEFT, 0.23f, editorArea);
//		IFolderLayout folder = layout.createFolder("messages", IPageLayout.TOP, 0.5f, editorArea);
//		folder.addPlaceholder(Doc.ID + ":*");
//		folder.addView(View.ID);
		layout.addStandaloneView(CommentViewer.ID,  false, IPageLayout.RIGHT, 0.75f, editorArea);
//		layout.getViewLayout(NavigationView.ID).setCloseable(false);
	}
}
