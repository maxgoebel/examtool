package annotationide;

import org.eclipse.jface.action.IContributionItem;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.application.ActionBarAdvisor;
import org.eclipse.ui.application.IActionBarConfigurer;
import org.eclipse.ui.application.IWorkbenchWindowConfigurer;
import org.eclipse.ui.application.WorkbenchWindowAdvisor;

public class ApplicationWorkbenchWindowAdvisor extends WorkbenchWindowAdvisor {

    public ApplicationWorkbenchWindowAdvisor(IWorkbenchWindowConfigurer configurer) {
        super(configurer);
    }

    @Override
    public void createWindowContents(Shell shell) {
       super.createWindowContents(shell);
       shell.setMaximized(true);
    }
    
    public ActionBarAdvisor createActionBarAdvisor(IActionBarConfigurer configurer) {
        return new ApplicationActionBarAdvisor(configurer);
    }
    
    public void preWindowOpen() {
        IWorkbenchWindowConfigurer configurer = getWindowConfigurer();
        //configurer.setInitialSize(new Point(600, 400));
        configurer.setShowCoolBar(false);
        configurer.setShowStatusLine(false);
        configurer.setShowProgressIndicator(true);
    }
    
    @Override
    public void postWindowOpen() 
    {
    	super.postWindowClose();
    	
    	// remove unwanted UI contributions that eclipse makes by default
		IWorkbenchWindow[] windows = PlatformUI.getWorkbench().getWorkbenchWindows();

		for (int i = 0; i < windows.length; ++i) {
			IWorkbenchPage page = windows[i].getActivePage();
			if (page != null) {
				
				
				// hide generic 'File' commands
//				page.hideActionSet("org.eclipse.ui.actionSet.openFiles");
				page.hideActionSet("org.eclipse.atf.mozilla.ide.debug.ui.JSDebugActionSet");
				page.hideActionSet("org.eclipse.ui.externaltools.ExternalToolsSet");
				
				// hide 'Convert Line Delimiters To...'
				page.hideActionSet("org.eclipse.ui.edit.text.actionSet.convertLineDelimitersTo");

				// hide 'Search' commands
				page.hideActionSet("org.eclipse.search.searchActionSet");

				// hide 'Annotation' commands
				page.hideActionSet("org.eclipse.ui.edit.text.actionSet.annotationNavigation");

				// hide 'Forward/Back' type navigation commands
				page.hideActionSet("org.eclipse.ui.edit.text.actionSet.navigation");
				
				page.hideActionSet( "org.eclipse.debug.ui.launchActionSet");
				page.hideActionSet( "org.eclipse.debug.ui.debugActionSet");
			}
		}
		
		IContributionItem[] mItems, mSubItems;
		IMenuManager mm = getWindowConfigurer ().getActionBarConfigurer ().getMenuManager ();
		mItems = mm.getItems ();
		for (int i = 0; i < mItems.length; i++)
		{
			if (mItems[i] instanceof MenuManager)
			{
				mSubItems = ((MenuManager) mItems[i]).getItems ();
				for (int j = 0; j < mSubItems.length; j++)
				{
					if (mItems[i].getId ().equals ("file"))
						((MenuManager) mItems[i]).remove ("org.eclipse.ui.openLocalFile");
					else if (mItems[i].getId ().equals ("help"))
					{
						((MenuManager) mItems[i]).remove ("group.updates");
						((MenuManager) mItems[i]).remove ("org.eclipse.update.ui.updateMenu");
						((MenuManager) mItems[i]).remove ("org.eclipse.ui.actions.showKeyAssistHandler");
					}
				}

				if (mItems[i].getId().equals("org.eclipse.ui.run")) {
					mm.remove(mItems[i]);
				} else 	if (mItems[i].getId().startsWith("org.eclipse.gmf.examples.")) {
					mm.remove(mItems[i]);
				} else if (mItems[i].getId().equals("org.eclipse.ui.externaltools.ExternalToolsSet")) {
					mm.remove(mItems[i]);
				} else if (mItems[i].getId().startsWith("org.eclipse.search.")) {
					mm.remove(mItems[i]);
				}
			}
		}
		mm.update(true);
    }
}
