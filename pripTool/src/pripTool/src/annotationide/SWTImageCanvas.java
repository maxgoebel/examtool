package annotationide;

import ij.IJ;
import ij.ImagePlus;
import ij.process.ImageConverter;
import ij.process.ImageProcessor;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Toolkit;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.FilteredImageSource;
import java.awt.image.ImageFilter;
import java.awt.image.ImageProducer;
import java.awt.image.LookupOp;
import java.awt.image.RGBImageFilter;
import java.awt.image.RescaleOp;
import java.awt.image.ShortLookupTable;
import java.awt.image.WritableRaster;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.eclipse.draw2d.ColorConstants;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ControlAdapter;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.events.MouseMoveListener;
import org.eclipse.swt.events.MouseWheelListener;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.graphics.ImageLoader;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.ScrollBar;

import utils.ErrorDump;
import utils.IPlotMouseListener;
import utils.PDFUtils;
import utils.SWT2Dutil;
import utils.document.DocumentModel;
import utils.document.DocumentUpdate;
import utils.document.DocumentUpdateEvent;
import utils.model.ExamModel;
import utils.model.ExamSheet;
import annotationide.PDFViewerSWT.PlotKeyListener;
import at.tuwien.prip.core.model.graph.comparators.YRectangleComparator;

/**
 * A scrollable image canvas that extends org.eclipse.swt.graphics.Canvas.
 * <p/>
 * It requires Eclipse (version >= 2.1) on Win32/win32; Linux/gtk; MacOSX/carbon.
 * <p/>
 * This implementation using the pure SWT, no UI AWT package is used. For 
 * convenience, I put everything into one class. However, the best way to
 * implement this is to use inheritance to create multiple hierarchies.
 * 
 * @author Chengdong Li: cli4@uky.edu
 */
public class SWTImageCanvas extends Canvas 
{
	/* zooming rates in x and y direction are equal.*/
	final float ZOOMIN_RATE = 1.1f; /* zoomin rate */
	final float ZOOMOUT_RATE = 0.9f; /* zoomout rate */

	private Image sourceImage; /* original image */
	private Image screenImage; /* screen image */

	Rectangle selectionRectangle = null;

	AffineTransform transform = new AffineTransform();
	AffineTransform selTransform = new AffineTransform();

	private String currentDir=""; /* remembering file open directory */

	PDFViewerSWT viewer = null;

	private float zoomLevel = 1f;
	double dtx = 0, dty = 0;


	/**
	 * Constructor.
	 * 
	 * @param viewer
	 * @param parent
	 */
	public SWTImageCanvas(final PDFViewerSWT viewer, final Composite parent) 
	{
		this(viewer, parent, SWT.NULL);
	}


	/**
	 * Constructor for ScrollableCanvas.
	 * 
	 * @param viewer
	 * @param parent the parent of this control.
	 * @param style the style of this control.
	 */
	public SWTImageCanvas(final PDFViewerSWT viewer, final Composite parent, int style) 
	{
		super(parent, style|SWT.BORDER|SWT.V_SCROLL|SWT.H_SCROLL
				| SWT.NO_BACKGROUND);

		this.viewer = viewer;

		addControlListener(new ControlAdapter() { /* resize listener. */
			public void controlResized(ControlEvent event) {
				syncScrollBars();
			}
		});
		addPaintListener(new PaintListener() { /* paint listener. */
			public void paintControl(final PaintEvent event) {
				paint(event.gc);
			}
		});

		setBackground(ColorConstants.darkGray);
		initScrollBars();
	}

	/**
	 * 
	 * @param ev
	 */
	public void documentUpdated(DocumentUpdateEvent ev) 
	{	
		DocumentUpdate update = ev.getDocumentUpdate();
		DocumentModel model = update.getUpdate();

		switch (update.getType())
		{
		case RESIZE: /* scaling */

			double scale = model.getScale();
			zoomLevel = (float) scale;
			ImageData data = PDFUtils.getImageFromPDFPage(PDFViewerSWT.page,zoomLevel,true);
			setImageData(data, true);
			this.redraw();

			break;

		case REPAN: /* translation */

			break;

		case DOCUMENT_CHANGE:

			double s = model.getScale();
			zoomLevel = (float) s;
			data = PDFUtils.getImageFromPDFPage(PDFViewerSWT.page,zoomLevel,true);
			setImageData(data, false);

			this.redraw();
			break;

		case PAGE_CHANGE:

			setImage(model.getThumb());
			break;

		default:

			//			fitHeight
			break;
		}
	}

	public AffineTransform getTransform()
	{
		return transform;
	}

	/**
	 * Dispose the garbage here
	 */
	public void dispose()
	{
		if (sourceImage != null && !sourceImage.isDisposed()) {
			sourceImage.dispose();
		}
		if (screenImage != null && !screenImage.isDisposed()) {
			screenImage.dispose();
		}
	}

	/* Paint function */
	private void paint(GC gc) 
	{
		if (!Activator.isChangeModelState()) 
		{
			viewer.setSelectedRectangle(null);
		}
		Rectangle clientRect = getClientArea(); /* Canvas' painting area */
		setBackground(ColorConstants.darkGray);

		if (sourceImage != null) 
		{
			Rectangle imageRect =
				SWT2Dutil.inverseTransformRect(transform, clientRect);
			int gap = 2; /* find a better start point to render */
			imageRect.x -= gap; imageRect.y -= gap;
			imageRect.width += 2 * gap; imageRect.height += 2 * gap;

			if (sourceImage.isDisposed()) return;

			Rectangle imageBound = sourceImage.getBounds();
			imageRect = imageRect.intersection(imageBound);
			Rectangle destRect = SWT2Dutil.transformRect(transform, imageRect);

			if (screenImage != null)
				screenImage.dispose();
			screenImage =
				new Image(getDisplay(), clientRect.width, clientRect.height);

			//draw image
			GC newGC = new GC(screenImage);	

			//draw background
			newGC.setBackground(ColorConstants.darkGray);
			newGC.setForeground(ColorConstants.darkGray);
			newGC.fillRectangle(clientRect);

			newGC.setClipping(clientRect);
			newGC.drawImage(
					sourceImage,
					imageRect.x,
					imageRect.y,
					imageRect.width,
					imageRect.height,
					destRect.x,
					destRect.y,
					destRect.width,
					destRect.height);

			ExamSheet sheet = viewer.getExamSheet();
			ExamModel model = Activator.modelControl.getExam();
			if (!hideHighlights) 
			{
				String qrCode = sheet.getQrCode();
				if (qrCode!=null && qrCode.length()>0)
				{
					newGC.setBackground(ColorConstants.orange);
					Point p = new Point(100, 170);
					Point pTrans = SWT2Dutil.transformPoint(transform, p);
					newGC.drawString(qrCode, pTrans.x, pTrans.y,false);
				}

				//draw highlights
				for (Rectangle hBox : sheet.getYellowMarkers()) 
				{
					Rectangle selRect = 
						SWT2Dutil.transformRect(
								transform, hBox);

					//draw selection
					newGC.setLineWidth(5);

					if (hBox.equals(viewer.getSelectedRectangle()))
					{
						newGC.setForeground(ColorConstants.red);
					} else {
						newGC.setForeground(ColorConstants.blue);
					}

					newGC.setBackground(ColorConstants.yellow);
					newGC.setAlpha(70);
					newGC.fillRectangle(
							selRect.x,
							selRect.y,
							selRect.width,
							selRect.height
					);
				}

				//draw highlights
				for (Rectangle hBox : sheet.getRedMarkers()) 
				{
					Rectangle selRect = 
						SWT2Dutil.transformRect(
								transform, hBox);

					//draw selection
					newGC.setLineWidth(5);
					newGC.setForeground(ColorConstants.red);
					newGC.setBackground(ColorConstants.red);
					newGC.setAlpha(70);
					newGC.fillRectangle(
							selRect.x,
							selRect.y,
							selRect.width,
							selRect.height
					);
				}

				//draw highlights
				for (Rectangle hBox : sheet.getGreenMarkers()) 
				{
					Rectangle selRect = 
						SWT2Dutil.transformRect(
								transform, hBox);

					//draw selection
					newGC.setLineWidth(5);
					newGC.setForeground(ColorConstants.blue);
					newGC.setBackground(ColorConstants.green);
					newGC.setAlpha(70);
					newGC.fillRectangle(
							selRect.x,
							selRect.y,
							selRect.width,
							selRect.height
					);
				}

				//draw highlights
				for (Rectangle hBox : sheet.getSelectionBoxes()) 
				{
					Rectangle selRect = 
						SWT2Dutil.transformRect(
								transform, hBox);

					//draw selection
					newGC.setLineWidth(5);

					if (hBox.equals(viewer.getSelectedRectangle()))
					{
						newGC.setForeground(ColorConstants.red);
					} else {
						newGC.setForeground(ColorConstants.blue);
					}
					newGC.setBackground(ColorConstants.blue);
					newGC.setAlpha(200);


					if (model.getBoxTextMap().containsKey(hBox)) 
					{
						Point p = new Point(hBox.x+4, hBox.y);
						Point pTrans = SWT2Dutil.transformPoint(transform, p);
						String text = model.getBoxTextMap().get(hBox);
						newGC.drawString(text, pTrans.x, pTrans.y, true);
					}

					newGC.setAlpha(200);
					newGC.drawRectangle(
							selRect.x,
							selRect.y,
							selRect.width,
							selRect.height
					);
				}

				//draw grades
				if (model.getGradeFields()!=null && sheet.getGrades()!=null)
				{
					List<Rectangle> grades = model.getGradeFields().get(sheet.getPageNum());
					List<java.awt.Rectangle> awtRects = new ArrayList<java.awt.Rectangle>();
					for (Rectangle grade : grades) 
					{
						Rectangle selRect = SWT2Dutil.transformRect(transform, grade);
						awtRects.add(new java.awt.Rectangle(selRect.x,selRect.y,selRect.width,selRect.height));
					}
					if (grades!=null) 
					{
						//find start index
						int i=0;
						for (int j=0; j<sheet.getPageNum();j++)
						{
							i+=model.getGradeFields().get(j).size();
						}
						Collections.sort(awtRects, new YRectangleComparator());
						Collections.reverse(awtRects);

						for (java.awt.Rectangle rect : awtRects) 
						{
							newGC.setLineWidth(5);

							if (rect.equals(viewer.getSelectedRectangle()))
							{
								newGC.setForeground(ColorConstants.red);
							} else {
								newGC.setForeground(ColorConstants.orange);
							}
							newGC.setBackground(ColorConstants.orange);
							newGC.setAlpha(200);
							newGC.fillRectangle(
									rect.x,
									rect.y,
									rect.width,
									rect.height
							);

							String text = sheet.getGrades()[i];
							newGC.setForeground(ColorConstants.black);
							newGC.setFont(new Font(getShell().getDisplay(),"Helvetica", 28, SWT.BOLD ) );
							newGC.drawString(text, rect.x+4, rect.y, true);
							i++;
						}
					}
				}
			}
			newGC.dispose();

			gc.setBackground(ColorConstants.darkGray);
			gc.setForeground(ColorConstants.darkGray);
			gc.drawRectangle(clientRect);
			gc.drawImage(screenImage, 0, 0);
		} 
		else 
		{
			gc.setClipping(clientRect);
			gc.fillRectangle(clientRect);
			initScrollBars();
		}
	}

	/* Initialize the scrollbar and register listeners. */
	private void initScrollBars()
	{
		ScrollBar horizontal = getHorizontalBar();
		horizontal.setEnabled(false);
		horizontal.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent event) {
				scrollHorizontally((ScrollBar) event.widget);
			}
		});
		ScrollBar vertical = getVerticalBar();
		vertical.setEnabled(false);
		vertical.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent event) {
				scrollVertically((ScrollBar) event.widget);
			}
		});
	}

	/* Scroll horizontally */
	private void scrollHorizontally(ScrollBar scrollBar) 
	{
		if (sourceImage == null)
			return;

		AffineTransform af = transform;
		double tx = af.getTranslateX();
		double select = -scrollBar.getSelection();
		af.preConcatenate(AffineTransform.getTranslateInstance(select - tx, 0));
		transform = af;

		af = selTransform;
		tx = af.getTranslateX();
		select = -scrollBar.getSelection();
		af.preConcatenate(AffineTransform.getTranslateInstance(select - tx, 0));
		selTransform = af;
		syncScrollBars();
	}

	/* Scroll vertically */
	private void scrollVertically(ScrollBar scrollBar) 
	{
		if (sourceImage == null)
			return;

		AffineTransform af = transform;
		double ty = af.getTranslateY();
		double select = -scrollBar.getSelection();
		af.preConcatenate(AffineTransform.getTranslateInstance(0, select - ty));
		transform = af;

		af = selTransform;
		ty = af.getTranslateY();
		select = -scrollBar.getSelection();
		af.preConcatenate(AffineTransform.getTranslateInstance(0, select - ty));
		selTransform = af;

		syncScrollBars();
	}

	/**
	 * Source image getter.
	 * @return sourceImage.
	 */
	public Image getSourceImage() 
	{
		return sourceImage;
	}

	/**
	 * Synchronize the scrollbar with the image. If the transform is out
	 * of range, it will correct it. This function considers only following
	 * factors :<b> transform, image size, client area</b>.
	 */
	public void syncScrollBars() 
	{
		if (sourceImage == null) {
			redraw();
			return;
		}

		if (sourceImage.isDisposed())
		{
			ErrorDump.debug(this, "image disposed");
			//needs reloading
			return;
		}
		AffineTransform af = transform;
		double sx = af.getScaleX(), sy = af.getScaleY();
		double tx = af.getTranslateX(), ty = af.getTranslateY();
		if (tx > 0) tx = 0;
		if (ty > 0) ty = 0;

		ScrollBar horizontal = getHorizontalBar();
		horizontal.setIncrement((int) (getClientArea().width / 100));
		horizontal.setPageIncrement(getClientArea().width);
		Rectangle imageBound = sourceImage.getBounds();
		int cw = getClientArea().width, ch = getClientArea().height;
		if (imageBound.width * sx > cw) { /* image is wider than client area */
			horizontal.setMaximum((int) (imageBound.width * sx));
			horizontal.setEnabled(true);
			if (((int) - tx) > horizontal.getMaximum() - cw)
				tx = -horizontal.getMaximum() + cw;
		} else { /* image is narrower than client area */
			horizontal.setEnabled(false);
			tx = (cw - imageBound.width * sx) / 2; //center if too small.
		}

		ScrollBar vertical = getVerticalBar();
		vertical.setIncrement((int) (getClientArea().height / 100));
		vertical.setPageIncrement((int) (getClientArea().height));
		if (imageBound.height * sy > ch) { /* image is higher than client area */
			vertical.setMaximum((int) (imageBound.height * sy));
			vertical.setEnabled(true);
			if (((int) - ty) > vertical.getMaximum() - ch)
				ty = -vertical.getMaximum() + ch;
		} else { /* image is less higher than client area */
			vertical.setEnabled(false);
			ty = (ch - imageBound.height * sy) / 2; //center if too small.
		}

		/* update transform. */
		af = AffineTransform.getScaleInstance(sx, sy);
		af.preConcatenate(AffineTransform.getTranslateInstance(tx, ty));
		transform = af;
		//selTransform = af;

		AffineTransform af2 = transform;
		sx = af2.getScaleX();
		sy = af2.getScaleY();
		tx = af2.getTranslateX();
		ty = af2.getTranslateY();
		if (tx > 0) tx = 0;
		if (ty > 0) ty = 0;
		if (imageBound.width * sx > cw) { /* image is wider than client area */
			horizontal.setMaximum((int) (imageBound.width * sx));
			horizontal.setEnabled(true);
			if (((int) - tx) > horizontal.getMaximum() - cw)
				tx -=horizontal.getMaximum() + cw;
		} else { /* image is narrower than client area */
			horizontal.setEnabled(false);
			tx = (cw - imageBound.width * sx) / 2; //center if too small.
		}
		if (imageBound.height * sy > ch) { /* image is higher than client area */
			vertical.setMaximum((int) (imageBound.height * sy));
			vertical.setEnabled(true);
			if (((int) - ty) > vertical.getMaximum() - ch)
				ty -= vertical.getMaximum() + ch;
		} else { /* image is less higher than client area */
			vertical.setEnabled(false);
			ty = (ch - imageBound.height * sy) / 2; //center if too small.
		}
		double x = tx - dtx;
		double y = ty - dty;

		af2 = AffineTransform.getScaleInstance(sx, sy);
		af2.preConcatenate(AffineTransform.getTranslateInstance(x, y));
		selTransform = af2;

		vertical.setSelection((int) (-ty));
		vertical.setThumb((int) (getClientArea().height));

		horizontal.setSelection((int) (-tx));
		horizontal.setThumb((int) (getClientArea().width));

		redraw();
	}

	/**
	 * Reload image from a file
	 * @param filename image file
	 * @return swt image created from image file
	 */
	public Image loadImage(String filename) 
	{
		if (sourceImage != null && !sourceImage.isDisposed()) {
			sourceImage.dispose();
			sourceImage = null;
		}
		sourceImage = new Image(getDisplay(), filename);
		showOriginal();
		return sourceImage;
	}

	/**
	 * Call back funtion of button "open". Will open a file dialog, and choose
	 * the image file. It supports image formats supported by Eclipse.
	 */
	public void onFileOpen() 
	{
		FileDialog fileChooser = new FileDialog(getShell(), SWT.OPEN);
		fileChooser.setText("Open image file");
		fileChooser.setFilterPath(currentDir);
		fileChooser.setFilterExtensions(
				new String[] { "*.gif; *.jpg; *.png; *.ico; *.bmp" });
		fileChooser.setFilterNames(
				new String[] { "SWT image" + " (gif, jpeg, png, ico, bmp)" });
		String filename = fileChooser.open();
		if (filename != null){
			loadImage(filename);
			currentDir = fileChooser.getFilterPath();
		}
	}

	/**
	 * Get the image data. (for future use only)
	 * @return image data of canvas
	 */
	public ImageData getImageData() 
	{
		return sourceImage.getImageData();
	}

	/**
	 * Reset the image data and update the image
	 * @param data image data to be set
	 */
	public void setImageData(ImageData data, boolean keepHighlights) 
	{
		setBackground(ColorConstants.darkGray);
		if (!keepHighlights) 
		{
			selectionRectangle = null;
		}

		if (sourceImage != null)
			sourceImage.dispose();
		if (data != null) 
		{
			sourceImage = new Image(getDisplay(), data);
		}

		syncScrollBars();
	}

	public void setImage (Image img) 
	{
		sourceImage = img;
		syncScrollBars();
	}

	/**
	 * Fit the image onto the canvas
	 */
	public void fitCanvas() 
	{
		if (sourceImage == null)
			return;
		Rectangle imageBound = sourceImage.getBounds();
		Rectangle destRect = getClientArea();
		double sx = (double) destRect.width / (double) imageBound.width;
		double sy = (double) destRect.height / (double) imageBound.height;
		double s = Math.min(sx, sy);
		double dx = 0.5 * destRect.width;
		double dy = 0.5 * destRect.height;
		centerZoom(dx, dy, s, new AffineTransform());
	}

	/**
	 * Show the image with the original size
	 */
	public void showOriginal() 
	{
		if (sourceImage == null)
			return;
		transform = new AffineTransform();
		syncScrollBars();
	}

	/**
	 * Perform a zooming operation centered on the given point
	 * (dx, dy) and using the given scale factor. 
	 * The given AffineTransform instance is preconcatenated.
	 * @param dx center x
	 * @param dy center y
	 * @param scale zoom rate
	 * @param af original affinetransform
	 */
	public void centerZoom(
			double dx,
			double dy,
			double scale,
			AffineTransform af) 
	{
		af.preConcatenate(AffineTransform.getTranslateInstance(-dx, -dy));
		af.preConcatenate(AffineTransform.getScaleInstance(scale, scale));
		af.preConcatenate(AffineTransform.getTranslateInstance(dx, dy));
		transform = af;
		syncScrollBars();
	}

	/**
	 * Zoom in around the center of client Area.
	 */
	public void zoomIn() 
	{
		if (sourceImage == null) {
			return;
		}
		zoomLevel *= ZOOMIN_RATE;

		Rectangle rect = getClientArea();
		int w = rect.width, h = rect.height;
		double dx = ((double) w) / 2;
		double dy = ((double) h) / 2;
		centerZoom(dx, dy, ZOOMIN_RATE, transform);
	}

	/**
	 * Zoom out around the center of client Area.
	 */
	public void zoomOut() 
	{
		if (sourceImage == null) {
			return;	
		}
		zoomLevel *= ZOOMOUT_RATE;

		Rectangle rect = getClientArea();
		int w = rect.width, h = rect.height;
		double dx = ((double) w) / 2;
		double dy = ((double) h) / 2;
		centerZoom(dx, dy, ZOOMOUT_RATE, transform);
	}

	public float getZoomLevel() 
	{
		return zoomLevel;
	}

	public void addPlotKeyListener (PlotKeyListener kl) {
		addKeyListener(kl);
	}
	/**
	 * 
	 * @param ml
	 */
	public void addPlotMouseListener (IPlotMouseListener ml) 
	{
		if (ml instanceof MouseListener) {
			addMouseListener((MouseListener) ml);
		} 
		if (ml instanceof MouseMoveListener) {
			addMouseMoveListener((MouseMoveListener) ml);
		}
		if (ml instanceof PaintListener) {
			addPaintListener((PaintListener) ml);
		}
		if (ml instanceof MouseWheelListener) {
			addMouseWheelListener((MouseWheelListener) ml);
		}
	}

	boolean hideHighlights = false;

	public void hideHighlights(boolean hide) {
		hideHighlights = hide;
		redraw();
	}


	public void increaseContrast() 
	{
		BufferedImage img = PDFUtils.convertToAWT(getImageData());
		img = PDFUtils.contrast(img);
		setImageData(PDFUtils.convertToSWT(img), true);
	}

	/**
	 * 
	 * @param input
	 * @return
	 */
	public void enhance()
	{
		String tmpFile = "/home/max/tmp.png";
		ImageLoader loader = new ImageLoader();
		loader.data = new ImageData[] {getImageData()};
		loader.save(tmpFile, SWT.IMAGE_PNG);
		
		java.awt.Image img = null;

		//load image
		ImagePlus myImPlus = IJ.openImage(tmpFile);

		try
		{
			//preprocessing
			ImageConverter convert = new ImageConverter(myImPlus);
			convert.convertToGray16();

			ImageProcessor myIp = myImPlus.getProcessor();
			
//			myIp.dilate();
//			myIp.dilate();
//			myIp.dilate();
			myIp.smooth();
//			myIp.filter(1);
			
			myIp.threshold(225);
				
			
//			myIp.dilate();
//			myIp.dilate();
//			myIp.dilate();
//			
//			myIp.erode();
//			myIp.erode();

			
			myIp.medianFilter();
//			myIp.findEdges();
//			myIp.invert();
//			myIp.smooth();
//			myIp.sharpen();
//			myIp.sharpen();
//			myIp.smooth();
//			myIp.sharpen();
			
//			myIp.dilate();
//			myIp.erode();
			
//			myIp.autoThreshold();
			

//			myIp.dilate();
//			myIp.erode();
//			myIp.dilate();
//			myIp.erode();
			
//			myIp.threshold(85);
//			
//			myIp.dilate();
//			myIp.dilate();
//			myIp.dilate();
//			
//			myIp.threshold(55);
//			myIp.medianFilter();

//			myIp.threshold(90);
			//				myIp.erode();
//			myIp.invert();
//			IJ.save(myImPlus, output);

			img = myIp.createImage();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		setImageData(PDFUtils.convertToSWT((BufferedImage) img), true);
	}

	public void binarize() 
	{
		BufferedImage img = PDFUtils.convertToAWT(getImageData());
		img = PDFUtils.binarize(img,190);
		setImageData(PDFUtils.convertToSWT(img), true);
	}

	public void contrastChange()
	{
		BufferedImage img = PDFUtils.convertToAWT(getImageData());
		RescaleOp rescale = new RescaleOp(3.6f,20.0f, null);
		img=rescale.filter(img,null);//(sourse,destination)
		setImageData(PDFUtils.convertToSWT(img), true);
	}

	public void removeBackground()
	{
		BufferedImage img = PDFUtils.convertToAWT(getImageData());

		int color = img.getRGB(20,20);
		//        for (int i=0; i<100; i++)
		//        {
		java.awt.Image image = makeColorTransparent(img, new Color(color));
		img = imageToBufferedImage(image);
		//        }


		setImageData(PDFUtils.convertToSWT(img), true);
	}

	private static BufferedImage imageToBufferedImage(java.awt.Image image)
	{

		BufferedImage bufferedImage = new BufferedImage(image.getWidth(null), image.getHeight(null), BufferedImage.TYPE_INT_ARGB);
		Graphics2D g2 = bufferedImage.createGraphics();
		g2.drawImage(image, 0, 0, null);
		g2.dispose();

		return bufferedImage;
	}

	public static java.awt.Image makeColorTransparent(BufferedImage im, final Color color) 
	{
		ImageFilter filter = new RGBImageFilter() {

			// the color we are looking for... Alpha bits are set to opaque
			public int markerRGB = color.getRGB() | 0xFF000000;

			public final int filterRGB(int x, int y, int rgb) {
				if ((rgb | 0xFF000000) == markerRGB) {
					// Mark the alpha bits as zero - transparent
					return 0x00FFFFFF & rgb;
				} else {
					// nothing to do
					return rgb;
				}
			}
		};

		ImageProducer ip = new FilteredImageSource(im.getSource(), filter);
		return Toolkit.getDefaultToolkit().createImage(ip);
	}

	public void contrastIncLUT() 
	{
		BufferedImage bi = PDFUtils.convertToAWT(getImageData());

		short brighten[] = new short[256];
		for (int i = 0; i < 256; i++) {
			short pixelValue = (short) (i * 1.2);
			if (pixelValue > 255)
				pixelValue = 255;
			else if (pixelValue < 0)
				pixelValue = 0;
			brighten[i] = pixelValue;
		}
		ShortLookupTable lut = new ShortLookupTable(0, brighten);

		LookupOp lop = new LookupOp(lut, null);
		lop.filter(bi, bi);
		setImageData(PDFUtils.convertToSWT(bi), true);
	}

	public void histogramEqualization() {

		BufferedImage original = PDFUtils.convertToAWT(getImageData());

		int red;
		int green;
		int blue;
		int alpha;
		int newPixel = 0;

		// Get the Lookup table for histogram equalization
		ArrayList<int[]> histLUT = histogramEqualizationLUT(original);

		BufferedImage histogramEQ = new BufferedImage(original.getWidth(), original.getHeight(), original.getType());

		for(int i=0; i<original.getWidth(); i++) {
			for(int j=0; j<original.getHeight(); j++) {

				// Get pixels by R, G, B
				alpha = new Color(original.getRGB (i, j)).getAlpha();
				red = new Color(original.getRGB (i, j)).getRed();
				green = new Color(original.getRGB (i, j)).getGreen();
				blue = new Color(original.getRGB (i, j)).getBlue();

				// Set new pixel values using the histogram lookup table
				red = histLUT.get(0)[red];
				green = histLUT.get(1)[green];
				blue = histLUT.get(2)[blue];

				// Return back to original format
				newPixel = colorToRGB(alpha, red, green, blue);

				// Write pixels into image
				histogramEQ.setRGB(i, j, newPixel);

			}
		}

		setImageData(PDFUtils.convertToSWT(histogramEQ), true);

	}

	// Get the histogram equalization lookup table for separate R, G, B channels
	private static ArrayList<int[]> histogramEqualizationLUT(BufferedImage input) {

		// Get an image histogram - calculated values by R, G, B channels
		ArrayList<int[]> imageHist = imageHistogram(input);

		// Create the lookup table
		ArrayList<int[]> imageLUT = new ArrayList<int[]>();

		// Fill the lookup table
		int[] rhistogram = new int[256];
		int[] ghistogram = new int[256];
		int[] bhistogram = new int[256];

		for(int i=0; i<rhistogram.length; i++) rhistogram[i] = 0;
		for(int i=0; i<ghistogram.length; i++) ghistogram[i] = 0;
		for(int i=0; i<bhistogram.length; i++) bhistogram[i] = 0;

		long sumr = 0;
		long sumg = 0;
		long sumb = 0;

		// Calculate the scale factor
		float scale_factor = (float) (255.0 / (input.getWidth() * input.getHeight()));

		for(int i=0; i<rhistogram.length; i++) {
			sumr += imageHist.get(0)[i];
			int valr = (int) (sumr * scale_factor);
			if(valr > 255) {
				rhistogram[i] = 255;
			}
			else rhistogram[i] = valr;

			sumg += imageHist.get(1)[i];
			int valg = (int) (sumg * scale_factor);
			if(valg > 255) {
				ghistogram[i] = 255;
			}
			else ghistogram[i] = valg;

			sumb += imageHist.get(2)[i];
			int valb = (int) (sumb * scale_factor);
			if(valb > 255) {
				bhistogram[i] = 255;
			}
			else bhistogram[i] = valb;
		}

		imageLUT.add(rhistogram);
		imageLUT.add(ghistogram);
		imageLUT.add(bhistogram);

		return imageLUT;

	}

	// Return an ArrayList containing histogram values for separate R, G, B channels
	public static ArrayList<int[]> imageHistogram(BufferedImage input) {

		int[] rhistogram = new int[256];
		int[] ghistogram = new int[256];
		int[] bhistogram = new int[256];

		for(int i=0; i<rhistogram.length; i++) rhistogram[i] = 0;
		for(int i=0; i<ghistogram.length; i++) ghistogram[i] = 0;
		for(int i=0; i<bhistogram.length; i++) bhistogram[i] = 0;

		for(int i=0; i<input.getWidth(); i++) {
			for(int j=0; j<input.getHeight(); j++) {

				int red = new Color(input.getRGB (i, j)).getRed();
				int green = new Color(input.getRGB (i, j)).getGreen();
				int blue = new Color(input.getRGB (i, j)).getBlue();

				// Increase the values of colors
				rhistogram[red]++; ghistogram[green]++; bhistogram[blue]++;

			}
		}

		ArrayList<int[]> hist = new ArrayList<int[]>();
		hist.add(rhistogram);
		hist.add(ghistogram);
		hist.add(bhistogram);

		return hist;

	}

	// Convert R, G, B, Alpha to standard 8 bit
	private int colorToRGB(int alpha, int red, int green, int blue) {

		int newPixel = 0;
		newPixel += alpha; newPixel = newPixel << 8;
		newPixel += red; newPixel = newPixel << 8;
		newPixel += green; newPixel = newPixel << 8;
		newPixel += blue;

		return newPixel;

	}


	public void growColor(int rgb)
	{
		BufferedImage img = PDFUtils.convertToAWT(getImageData());
		ColorModel cm = img.getColorModel();
		boolean isAlphaPreMultiplied = cm.isAlphaPremultiplied();
		WritableRaster raster = img.copyData(null);
		BufferedImage bi = new BufferedImage(cm, raster, isAlphaPreMultiplied, null);

		for (int y=0; y<img.getHeight(); y++)
		{
			for (int x=0; x<img.getWidth(); x++)
			{
				int clr = img.getRGB(x, y);
				int red = (clr & 0x00ff0000) >> 16;
			int green = (clr & 0x0000ff00) >> 8;
		int blue = clr & 0x000000ff;
		if (red>240 && green>240 && blue<240) {
			img.setRGB(x, y, colorToRGB(0, 255, 255, 255));
		} 
		else if (Math.abs(red-green)<15 && Math.abs(red-blue)<15) {
			img.setRGB(x, y, colorToRGB(0, 1, 1, 1));
		}
		else if (blue > red+green) 
		{
			int color = colorToRGB(0, 1, 1, 1);
			img.setRGB(x, y, colorToRGB(0, 1, 1, 1));
			bi.setRGB(x, y, color);
			bi.setRGB(x-1, y, color);
			bi.setRGB(x+1, y, color);
			bi.setRGB(x, y-1, color);
			bi.setRGB(x, y+1, color);
		}
		else 
		{
			//					img.setRGB(x, y, clr);
		}
			}
		}
		setImageData(PDFUtils.convertToSWT(bi), true);
	}
}