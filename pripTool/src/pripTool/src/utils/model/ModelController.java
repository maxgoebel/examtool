package utils.model;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * 
 * @author max
 *
 */
public class ModelController {

	private ExamModel exam;
	
	private List<IModelChangedListener> listeners;
	
	/**
	 * 
	 */
	public ModelController() {
		this.listeners = new ArrayList<IModelChangedListener>();
	}
	
	/**
	 * 
	 * @param exam
	 */
	public void modelChanged(ExamModel exam) 
	{
		this.exam = exam;
		
		ModelChangedEvent mce = new ModelChangedEvent();
		mce.setModel(exam);
		
		//notify observer:
		Iterator<IModelChangedListener> it = listeners.iterator();
		while (it.hasNext())
		{
			try 
			{
				IModelChangedListener listener = it.next();
				listener.modelChanged(mce);
			}
			catch (Exception e) 
			{
				break;
			}
		}
	}

	public ExamModel getExam() {
		return exam;
	}
	
	public void addModelChangedListener (IModelChangedListener listener) {
		if (!this.listeners.contains(listener)) {
			this.listeners.add(listener);
		}
	}
	
	public void removeModelChangedListener (IModelChangedListener listener) {
		if (this.listeners.contains(listener)) {
			this.listeners.remove(listener);
		}
	}

}
