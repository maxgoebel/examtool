package utils.model;

public interface IModelChangedListener 
{
	
	public void modelChanged(ModelChangedEvent event);
	
}
