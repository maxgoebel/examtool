package utils.model;

import utils.document.DocumentUpdate;

/**
 * 
 * @author max
 *
 */
public interface IModelChangeProvider {

	public void addModelChangedListener (IModelChangedListener listener);

	public void removeModelChangedListener (IModelChangedListener listener);

	public void setModelChange (DocumentUpdate update);

	public DocumentUpdate getModelChange ();

}
