package utils.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.graphics.Rectangle;

import at.tuwien.prip.core.model.graph.DocumentGraph;

import com.sun.pdfview.PDFPage;


/**
 * A single sheet of an exam.
 * 
 * @author max
 *
 */
public class ExamSheet implements Comparable<ExamSheet>
{
	private ImageData imgData;

	private DocumentGraph documentGraph;

	private PDFPage page;

	private String fileName;

	private String studentID;

	private String studentName;

	private String comment;

	private String qrCode;

	private String[] grades;
	
	int pageNum;

	int status = 0; //in progress

	private List<Rectangle> selectionBoxes = new ArrayList<Rectangle>();
	private List<Rectangle> greenMarkers = new ArrayList<Rectangle>();
	private List<Rectangle> yellowMarkers = new ArrayList<Rectangle>();
	private List<Rectangle> redMarkers = new ArrayList<Rectangle>();
	private List<Rectangle> inputBoxes = new ArrayList<Rectangle>();
	
	private String[] points;
	
	private Map<Rectangle,Integer> highlightId = new HashMap<Rectangle,Integer>();

	/**
	 * Constructor.
	 * @param id
	 * @param name
	 * @param pageNum
	 */
	public ExamSheet (String id, String name, int pageNum) 
	{
		this.studentID = id;
		this.studentName = name;
		this.pageNum = pageNum;
		this.status = 0;

		comment = "";
	}

	public void clearPoints() 
	{
		for (int i=0; i<points.length; i++)
		{
			points[i] = "e"; //empty
		}
		redMarkers.clear();
		greenMarkers.clear();
		yellowMarkers.clear();
	}
	
	public void setPoint(int idx, String val)
	{
		points[idx] = val;
		Rectangle box = selectionBoxes.get(idx);
		if (val.equals("e")) 
		{
			redMarkers.remove(box);
			greenMarkers.remove(box);
			yellowMarkers.remove(box);
		}
		else if (val.equals("r"))
		{
			redMarkers.add(box);
			greenMarkers.remove(box);
			yellowMarkers.remove(box);
		}
		else if (val.equals("g"))
		{
			redMarkers.remove(box);
			greenMarkers.add(box);
			yellowMarkers.remove(box);
		}
		if (val.equals("y"))
		{
			redMarkers.remove(box);
			greenMarkers.remove(box);
			yellowMarkers.add(box);
		}
	}

	@Override
	public String toString() 
	{
		return studentID + " - "+(pageNum+1)+ "  Points: " + sumPoints(points); 
	}
	
	public int sumPoints (String[] points) {
		int result = 0;
		for (String p : points)
		{
			if (p.equals("g")) {
				result++;
			}
		}
		return result;
	}

	public int getPageNum() {
		return pageNum;
	}
	public void setPageNum(int pageNum) {
		this.pageNum = pageNum;
	}
	public int getStatus() {
		return status;
	}
	public String getStudentName() {
		return studentName;
	}
	public String getStudentID() {
		return studentID;
	}
	public void setStudentID(String studentID) {
		this.studentID = studentID;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public void setPage(PDFPage page) {
		this.page = page;
	}
	public PDFPage getPage() {
		return page;
	}
	public ImageData getImgData() {
		return imgData;
	}
	public void setImgData(ImageData imgData) {
		this.imgData = imgData;
	}
	public DocumentGraph getDocumentGraph() {
		return documentGraph;
	}
	public String getFileName() {
		return fileName;
	}
	public void setDocumentGraph(DocumentGraph documentGraph) {
		this.documentGraph = documentGraph;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public List<Rectangle> getSelectionBoxes() {
		return selectionBoxes;
	}

	public void setSelectionBoxes(List<Rectangle> highlightBoxes) 
	{
		this.selectionBoxes = highlightBoxes;
		
		points = new String[selectionBoxes.size()];
		for (int i=0; i<points.length; i++)
		{
			points[i] = "e";
		}
	}
	
	public void setGrades(String[] grades)
	{
		this.grades = grades;
	}
	
	public String[] getGrades() 
	{
		return grades;
	}

	public List<Rectangle> getGreenMarkers() {
		return greenMarkers;
	}

	public List<Rectangle> getRedMarkers() {
		return redMarkers;
	}

	public List<Rectangle> getYellowMarkers() {
		return yellowMarkers;
	}

	public List<Rectangle> getInputBoxes() {
		return inputBoxes;
	}

	public void setInputBoxes(List<Rectangle> inputBoxes) {
		this.inputBoxes = inputBoxes;
	}

	@Override
	public int compareTo(ExamSheet o) {
		return o.toString().compareTo(toString());
	}
 
	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getComment() {
		return comment;
	}
	public void setQrCode(String qrCode) {
		this.qrCode = qrCode;
	}
	public String getQrCode() {
		return qrCode;
	}

//	public void initPointset(int nbits) {
//		this.points = new BitSet(nbits);
//	}

	public int getHighlightId(Rectangle r) {
		return highlightId.get(r);
	}
	
	public void setHighlightIds(Map<Rectangle,Integer> highIds) {
		this.highlightId = highIds;
	}
	
	public String[] getPoints() {
		return points;
	}
	
}
