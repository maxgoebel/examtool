package utils.model;

public class ModelChangedEvent {

	private ExamModel model;
	
	public void setModel(ExamModel model) {
		this.model = model;
	}
	
	public ExamModel getModel() {
		return model;
	}
}
