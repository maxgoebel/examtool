package utils.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.swt.graphics.Rectangle;

import at.tuwien.prip.common.datastructures.HashMapList;
import at.tuwien.prip.common.datastructures.MapList;
import at.tuwien.prip.common.utils.ListUtils;

/**
 * ExamModel.java
 *
 * 
 *
 * @author max
 * @date Jun 22, 2012
 */
public class ExamModel 
{

	private ExamSheet active = null;
			
	String description;
	
	private ExamSheet[] pages;
	
	private MapList<Integer,Rectangle> selectionBoxes;
	
	private MapList<Integer,Rectangle> inputFields;
	
	private MapList<Integer,Rectangle> gradeFields;
	 
	private String path;
	
	private Map<Rectangle, Integer> highlightIds =	new HashMap<Rectangle, Integer>();
	 
	private Map<Rectangle,String> boxTextMap = new HashMap<Rectangle, String>();
	
	public static String CSV_Header;
	
	/**
	 * Constructor.
	 */
	public ExamModel() 
	{
		this.inputFields = new HashMapList<Integer,Rectangle>();
		this.selectionBoxes = new HashMapList<Integer,Rectangle>();
		this.gradeFields = new HashMapList<Integer,Rectangle>();
		CSV_Header = "";
	}
	
	public String getDescription() {
		return description;
	}
	
	public ExamSheet[] getPages() {
		return pages;
	}
	
	
	public ExamSheet getActive() {
		return active;
	}
	
	public void setActive(ExamSheet active) {
		this.active = active;
	}
	
	/**
	 * 
	 * @return
	 */
	public String[] getMatNums () 
	{
		List<String> result = new ArrayList<String>();
		for (ExamSheet es : pages)
		{
			result.add(es.getStudentID());
		}
		ListUtils.unique(result);
		return ListUtils.toArray(result, String.class);
	}
	
	/**
	 * 
	 * @param studentID
	 * @param pageNum
	 * @return
	 */
	public ExamSheet findSheet (String studentID, int pageNum)
	{
		ExamSheet result = null;
		for (ExamSheet es : pages) 
		{
			if (es.getPageNum()==pageNum && es.getStudentID().equalsIgnoreCase(studentID)) {
				result = es;
				break;
			}
		}
		return result;
	}
	
	public void setPages(ExamSheet[] pages) {
		this.pages = pages;
	}
	public void setSelectionBoxes(MapList<Integer,Rectangle> evalHighlights) {
		this.selectionBoxes = evalHighlights;
	}
	public MapList<Integer,Rectangle> getSelectionBoxes() {
		return selectionBoxes;
	}
	public void setInputFields(MapList<Integer,Rectangle> inputFields) {
		this.inputFields = inputFields;
	}
	public MapList<Integer,Rectangle> getInputFields() {
		return inputFields;
	}
	public void setPath(String parentDir) {
		path = parentDir;
	}
	public String getPath() {
		return path;
	}

	public Map<Rectangle, Integer> getHighlightIds() {
		return highlightIds;
	}
	
	public void setHighlightIds(
			Map<Rectangle, Integer> highlightIds) {
		this.highlightIds = highlightIds;
	}
	
	public void setBoxTextMap(Map<Rectangle, String> boxTextMap) {
		this.boxTextMap = boxTextMap;
	}
	
	public Map<Rectangle, String> getBoxTextMap() {
		return boxTextMap;
	}
	
	public MapList<Integer, Rectangle> getGradeFields() {
		return gradeFields;
	}
	
	public void setGradeFields(MapList<Integer, Rectangle> gradeFields) {
		this.gradeFields = gradeFields;
	}

	/**
	 * 
	 * @return
	 */
	public int getNumUniqueIds() 
	{
		Set<String> ids = new HashSet<String>();
		for (ExamSheet sheet : getPages()) 
		{
			if (!ids.contains(sheet.getStudentID()))
			{
				ids.add(sheet.getStudentID());
			}
		}
		//		ListUtils.unique(ids);
		return ids.size();
	}
	
}


