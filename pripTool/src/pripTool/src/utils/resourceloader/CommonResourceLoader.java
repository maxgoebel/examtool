package utils.resourceloader;

import java.security.PrivilegedAction;

/**
 * CommonResourceLoader.java
 *
 * base class for all specialised (url, stream)
 * ResourceLoader classes
 *
 * Created: Fri Dec 14 20:04:31 2001
 *
 * @author Michal Ceresna
 * @version
 */
abstract class CommonResourceLoader
  implements PrivilegedAction<Object>
{

    private String path_to_resource;
    private ClassLoader loader;

  public CommonResourceLoader(String path_to_resource,
                              ClassLoader loader)
  {
    this.path_to_resource = path_to_resource;
    this.loader = loader;
  }


  ClassLoader getLoader() {
    return loader;
  }

  String getPathToResource() {
    return path_to_resource;
  }

}// CommonResourceLoader
