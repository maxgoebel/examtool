package utils.resourceloader;

/**
 * URLResourceLoader.java
 *
 * Finds a resource and returns an url
 * pointing at it
 *
 * Created: Fri Dec 14 19:57:29 2001
 *
 * @author Michal Ceresna
 * @version
 */
class URLResourceLoader extends CommonResourceLoader {

  public URLResourceLoader(String path_to_resource,
                           ClassLoader loader) {
    super(path_to_resource, loader);
  }

  /**
   * returns an url to the given resource
   */
  public Object run() {
    return getLoader().getResource(getPathToResource());
  }

}// URLResourceLoader
