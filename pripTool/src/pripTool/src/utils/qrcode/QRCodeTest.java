package utils.qrcode;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.BinaryBitmap;
import com.google.zxing.ChecksumException;
import com.google.zxing.FormatException;
import com.google.zxing.LuminanceSource;
import com.google.zxing.NotFoundException;
import com.google.zxing.Result;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.common.GlobalHistogramBinarizer;
import com.google.zxing.qrcode.QRCodeReader;
import com.google.zxing.qrcode.QRCodeWriter;

/**
 * 
 * @author max
 *
 */
public class QRCodeTest {

	public static void main(String[] args) {
		
		QRCodeWriter writer = new QRCodeWriter();
		
		try 
		{
			BitMatrix bm = writer.encode("A B C D E", BarcodeFormat.QR_CODE, 300, 300);
			MatrixToImageWriter.writeToFile(bm, "PNG", new File("/home/max/qrTest.png"));
						
			BufferedImage image = ImageIO.read(new File("/home/max/qrTest.png"));
			LuminanceSource source = new BufferedImageLuminanceSource(image);
			BinaryBitmap bitmap = new BinaryBitmap(new GlobalHistogramBinarizer(source));
			QRCodeReader reader = new QRCodeReader();
			Result result = reader.decode(bitmap);
			System.out.println("Result: "+result.getText());
		} 
		catch (IOException e) {
			e.printStackTrace();
		} catch (WriterException e) {
			e.printStackTrace();
		} catch (NotFoundException e) {
			e.printStackTrace();
		} catch (ChecksumException e) {
			e.printStackTrace();
		} catch (FormatException e) {
			e.printStackTrace();
		}

	}
}
