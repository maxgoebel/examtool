package utils;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.ProgressBar;

/**
 * LoadExamProgressMonitor.java
 *
 * 
 *
 * @author max
 * @date Jul 3, 2012
 */
public class LoadExamProgressMonitor implements IProgressMonitor
{
	private ProgressBar progressBar;
	
	/**
	 * 
	 * @param parent
	 */
	public LoadExamProgressMonitor(Composite parent) 
	{
		createControls(parent);
//		progressBar = new ProgressBar(parent, SWT.SMOOTH);
	}
	
	public void createControls(Composite parent) 
	{
		System.out.println("ToolItem");
		
		progressBar = new ProgressBar(parent, SWT.SMOOTH);
		progressBar.setBounds(100, 10, 200, 20);
		progressBar.setMaximum(100);
		progressBar.setState(SWT.NORMAL);
		progressBar.setVisible(true);
		progressBar.setEnabled(true);
	}

	@Override
	public void worked(final int work) 
	{
		Display.getDefault().asyncExec(new Runnable() {
			@Override
			public void run() {
				progressBar.setSelection(progressBar.getSelection() + work);
			}
		});
	}

	@Override
	public void setTaskName(String name) {

	}

	@Override
	public void setCanceled(boolean value) {

	}

	@Override
	public boolean isCanceled() {
		return false;
	}

	@Override
	public void internalWorked(double work) {
	}

	@Override
	public void done() {
		System.out.println("Done");

	}

	@Override
	public void beginTask(String name, int totalWork) {
		System.out.println("Starting");
	}

	@Override
	public void subTask(String name) {
		
	}
}
