package utils.document;


public class DocumentUpdateEvent {

	private DocumentUpdate documentUpdate;
	
	private IDocumentUpdateProvider provider;
	
	public DocumentUpdateEvent() {
	}
	
	public DocumentUpdate getDocumentUpdate() {
		return documentUpdate;
	}

	public void setDocumentUpdate(DocumentUpdate update) {
		this.documentUpdate = update;
	}
	
	public IDocumentUpdateProvider getDocumentUpdateProvider() {
		return provider;
	}

	public void setDocumentUpdatedProvider(IDocumentUpdateProvider provider) {
		this.provider = provider;
	}


}
