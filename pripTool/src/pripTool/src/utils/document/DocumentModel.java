package utils.document;

import java.awt.geom.Rectangle2D;
import java.util.Date;

import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.swt.graphics.Image;

import com.sun.pdfview.PDFFile;

/**
 * 
 * DocumentModel.java
 *
 *
 * created: Sep 13, 2009
 * @author Max Goebel
 */
public class DocumentModel 
{
	/* the document entry */
//	private DocumentEntry de;
	
	private int x, y;
	
	private int pageNum;
	
	private int numPages;
	
	private double scale = 0.6d;
	
	private PDFFile pdfFile;

	private Image thumb;
	
	
	/* document timestamp */
	private Date timeStamp;
	
	/**
	 * Constructor.
	 */
	public DocumentModel()
	{

	}

//	public DocumentEntry getDocumentEntry() {
//		return de;
//	}
//
//	public void setDocumentEntry(DocumentEntry de) {
//		this.de = de;
//	}

	public void setTimeStamp(Date timeStamp) {
		this.timeStamp = timeStamp;
	}

	public Date getTimeStamp() {
		return timeStamp;
	}

	public double getScale() {
		return scale;
	}


	public void setScale(double scale) {
		this.scale = scale;
	}

	/**
	 * 
	 * @return
	 */
	public Dimension getDimension() 
	{
		Rectangle2D pageBox = pdfFile.getPage(pageNum).getPageBox();
		return new Dimension(
				(int)pageBox.getWidth(), 
				(int)pageBox.getHeight());
	}

	/**
	 * 
	 * @return
	 */
	public Rectangle getBounds() 
	{
		Rectangle2D pageBox = pdfFile.getPage(pageNum).getPageBox();
		return new Rectangle(
				(int)pageBox.getX(), 
				(int)pageBox.getY(), 
				(int)pageBox.getWidth(), 
				(int)pageBox.getHeight());
	}

	public PDFFile getPdfFile() {
		return pdfFile;
	}


	public void setPdfFile(PDFFile pdfFile) {
		this.pdfFile = pdfFile;
	}
	
	public int getX() {
		return x;
	}
	
	public void setX(int x) {
		this.x = x;
	}
	
	public int getY() {
		return y;
	}
	
	public void setY(int y) {
		this.y = y;
	}

	public Image getThumb() {
		return thumb;
	}


	public void setThumb(Image thumb) {
		this.thumb = thumb;
	}
	
	public int getPageNum() {
		return pageNum;
	}
	
	public void setPageNum(int pageNum) {
		this.pageNum = pageNum;
	}
	
	
	public int getNumPages() {
		return numPages;
	}
	
	public void setNumPages(int numPages) {
		this.numPages = numPages;
	}
	
}
