package utils.document;


public interface IDocumentUpdateProvider 
{
	public void addDocumentUpdateListener (IDocumentUpdateListener listener);
	
	public void removeDocumentUpdateListener (IDocumentUpdateListener listener);
	
	public void setDocumentUpdate (DocumentUpdate update);
	
	public DocumentUpdate getDocumentUpdate ();
	
}
