package utils.document;

import java.util.ArrayList;
import java.util.List;

import utils.document.DocumentUpdate.UpdateType;



/**
 * 
 * DocumentController.java
 *
 *
 * @author mcg <mcgoebel@gmail.com>
 * @date Oct 1, 2011
 */
public class DocumentController implements IDocumentUpdateProvider 
{
	
	/* the SINGLETON document model contains all document relevant information */
	public static final DocumentModel docModel = new DocumentModel();

	private List<IDocumentUpdateListener> listeners;
	
	private DocumentUpdate lastUpdate;

	
	/**
	 * Constructor.
	 */
	public DocumentController()
	{
		listeners = new ArrayList<IDocumentUpdateListener>();
	}

	public int getCurrentDocumentPage()
	{
		return docModel.getPageNum();
	}
	
	
	public DocumentModel getDocModel() {
		return docModel;
	}
	
//	public DocumentEntry getCurrentDocumentEntry()
//	{
//		return docModel.getDocumentEntry();
//	}

	public void nextPage()
	{
		docModel.setPageNum(docModel.getPageNum()+1);
		DocumentUpdate update = new DocumentUpdate();
		update.setType(UpdateType.PAGE_CHANGE);
		update.setUpdate(docModel);
		setDocumentUpdate(update);
	}
	
	public void prevPage()
	{
		docModel.setPageNum(docModel.getPageNum()-1);
		DocumentUpdate update = new DocumentUpdate();
		update.setType(UpdateType.PAGE_CHANGE);
		update.setUpdate(docModel);
		setDocumentUpdate(update);	
	}
	
	/**
	 * 
	 */
	@Override
	public void setDocumentUpdate(DocumentUpdate update) 
	{
		DocumentModel model = update.getUpdate();
		if (!update.equals(lastUpdate))
		{
			if (update.getType()==UpdateType.DOCUMENT_CHANGE)
			{
//				if (model.getDocumentEntry()!=null) 
//				{
//					docModel.setDocumentEntry(model.getDocumentEntry());
//				} else {
//					model.setDocumentEntry(docModel.getDocumentEntry());
//				}
				if(model.getPageNum()==0) {
					docModel.setPageNum(1); //reset for new documents
				} else {
					docModel.setPageNum(model.getPageNum());
					docModel.setNumPages(model.getNumPages());
				}
				if (model.getScale()>0)
				{
					docModel.setScale(model.getScale());
				}
				if (model.getPdfFile()!=null)
				{
					docModel.setPdfFile(model.getPdfFile());
				}
				if (model.getThumb()!=null)
				{
					docModel.setThumb(model.getThumb());
				}
			}
			else if (update.getType()==UpdateType.PAGE_CHANGE) 
			{
				docModel.setPageNum(model.getPageNum()); 
			}
			else if (update.getType()==UpdateType.RESIZE)
			{
				docModel.setScale(model.getScale());
			}
			
			update.setUpdate(docModel);
			
			DocumentUpdateEvent ev = new DocumentUpdateEvent();
			ev.setDocumentUpdate(update);
			for (IDocumentUpdateListener listener : listeners) 
			{
				listener.documentUpdated(ev);
			}
		}
		lastUpdate = update;
	}

	@Override
	public DocumentUpdate getDocumentUpdate() {
		return lastUpdate;
	}
	
	@Override
	public void addDocumentUpdateListener(IDocumentUpdateListener listener) 
	{
		if (!listeners.contains(listener)) {
			listeners.add(listener);
		}
	}

	@Override
	public void removeDocumentUpdateListener(IDocumentUpdateListener listener) 
	{
		if (listeners.contains(listener)) {
			listeners.remove(listener);
		}
	}

}
