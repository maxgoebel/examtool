package utils.document;


public interface IDocumentUpdateListener {

	public void documentUpdated (DocumentUpdateEvent ev); 
}
