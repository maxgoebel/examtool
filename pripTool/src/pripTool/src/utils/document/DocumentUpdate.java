package utils.document;


public class DocumentUpdate {

	/* describe possible document updates */
	public enum UpdateType
	{
		PAGE_CHANGE,
		DOCUMENT_CHANGE,
		REPAN,
		RESIZE,
		GRAPH_CHANGE
	}
	
	private DocumentModel update;
	
	private UpdateType type;
	
	private Object provider;

	/**
	 * Constructor.
	 */
	public DocumentUpdate () {
		
	}
	
	public Object getProvider() {
		return provider;
	}

	public void setProvider(Object provider) {
		this.provider = provider;
	}

	public void setUpdate(DocumentModel update) {
		this.update = update;
	}

	public DocumentModel getUpdate() {
		return update;
	}

	public void setType(UpdateType type) {
		this.type = type;
	}

	public UpdateType getType() {
		return type;
	};
	
}
