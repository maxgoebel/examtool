package utils;

public class PropertyLoadException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public PropertyLoadException() {
		super();
	}

	public PropertyLoadException(String message, Throwable cause) {
		super(message, cause);
	}

	public PropertyLoadException(String message) {
		super(message);
	}

	public PropertyLoadException(Throwable cause) {
		super(cause);
	}

}
