package utils;
public class RGBColor
{
    private int farbe;
   
    public RGBColor()
    {
    }
 
    public void setColor(int rgb)
    {
        farbe = rgb;
    }
 
    public int getRed()
    {
        return (farbe >> 16) & 0xFF;
    }
 
    public int getGreen()
    {
        return (farbe >> 8) & 0xFF;
    }
 
    public int getBlue()
    {
        return (farbe >> 0) & 0xFF;
    }
 
    public void setRGB(int r, int g, int b)
    {
        farbe = ((255 & 0xFF) << 24) | ((r & 0xFF) << 16) | ((g & 0xFF) << 8)  | ((b & 0xFF) << 0);
    }
 
    public int getRGB()
    {
        return farbe;
    }   
}