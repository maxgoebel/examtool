package utils;


import org.eclipse.jface.dialogs.IMessageProvider;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

/**
 * ImportPointsDialog.java
 *
 * 
 *
 * @author max
 * @date May 17, 2012
 */
public class ImportPointsDialog extends TitleAreaDialog 
{

	private Text firstNameText;
	private Text lastNameText;
	private String firstName;
	private String lastName;

	
	/**
	 * Constructor.
	 * @param parent
	 */
	public ImportPointsDialog(Shell parent) 
	{
		super(parent);
	}
	
	
	@Override
	public void create() 
	{
		super.create();
		setTitle("Noten Importieren");
		setMessage("Importieren aus CSV Datei", IMessageProvider.INFORMATION);
		
	}
	
	@Override
	protected Control createDialogArea(Composite parent) 
	{
		final Composite comp = parent;
		GridLayout layout = new GridLayout();
		layout.numColumns = 3;
		// layout.horizontalAlignment = GridData.FILL;
		parent.setLayout(layout);

		// The text fields will grow with the size of the dialog
		GridData gridData = new GridData();
		gridData.grabExcessHorizontalSpace = true;
		gridData.horizontalAlignment = GridData.FILL;

		Label label1 = new Label(parent, SWT.NONE);
		label1.setText("CSV Datei");

		firstNameText = new Text(parent, SWT.BORDER);
		firstNameText.setLayoutData(gridData);
		
		Button button = new Button(parent, SWT.PUSH);
		button.setText("Browse");
		button.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				FileDialog dialog = new FileDialog(comp.getShell(), SWT.NULL);
				dialog.setFilterExtensions(new String[]{"*.csv"});
				String path = dialog.open();
				if (path != null) {
					firstNameText.setText(path);
				}
			}
		});
		  
		return parent;
	}
	@Override
	protected void createButtonsForButtonBar(Composite parent) 
	{
		GridData gridData = new GridData();
		gridData.verticalAlignment = GridData.FILL;
		gridData.horizontalSpan = 3;
		gridData.grabExcessHorizontalSpace = true;
		gridData.grabExcessVerticalSpace = true;
		gridData.horizontalAlignment = SWT.CENTER;

		parent.setLayoutData(gridData);
		// Create Add button
		// Own method as we need to overview the SelectionAdapter
		createOkButton(parent, OK, "Import", true);
		// Add a SelectionListener

		// Create Cancel button
		Button cancelButton = createButton(parent, CANCEL, "Cancel", false);
		// Add a SelectionListener
		cancelButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				setReturnCode(CANCEL);
				close();
			}
		});
	}

	protected Button createOkButton(Composite parent, int id, String label,
			boolean defaultButton) {
		// increment the number of columns in the button bar
		((GridLayout) parent.getLayout()).numColumns++;
		Button button = new Button(parent, SWT.PUSH);
		button.setText(label);
		button.setFont(JFaceResources.getDialogFont());
		button.setData(new Integer(id));
		button.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent event) {
				if (isValidInput()) {
					okPressed();
				}
			}
		});
		if (defaultButton) {
			Shell shell = parent.getShell();
			if (shell != null) {
				shell.setDefaultButton(button);
			}
		}
		setButtonLayoutData(button);
		return button;
	}

	private boolean isValidInput() {
		boolean valid = true;
		if (firstNameText.getText().length() == 0) {
			setErrorMessage("Bitte gültigen CSV Dateinamen angeben");
			valid = false;
		}
		return valid;
	}
	
	@Override
	protected boolean isResizable() {
		return true;
	}

	// We need to have the textFields into Strings because the UI gets disposed
	// and the Text Fields are not accessible any more.
	private void saveInput() {
		firstName = firstNameText.getText();
	}

	@Override
	protected void okPressed() {
		System.out.println(firstNameText.getText());
		saveInput();
		super.okPressed();
	}

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

}
