package utils;

import java.awt.Rectangle;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class XMLUtils 
{

	/**
	 * 
	 * @param fileName
	 */
	public static void processXML(String fileName) 
	{
		try 
		{
			File file = new File(fileName);
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = dbf.newDocumentBuilder();
			Document doc = db.parse(file);
			doc.getDocumentElement().normalize();
			
			//first read the inputs
			List<Rectangle> inputs = new ArrayList<Rectangle>();
			NodeList nodeLst = doc.getElementsByTagName("input");
			for (int s = 0; s < nodeLst.getLength(); s++) 
			{
				Node fstNode = nodeLst.item(s);
				if (fstNode.getNodeType() == Node.ELEMENT_NODE)
				{
					Element fstElmnt = (Element) fstNode;
					int x = (int) Double.parseDouble(fstElmnt.getAttribute("x"));
					int y = (int) Double.parseDouble(fstElmnt.getAttribute("y"));
					int width = (int) Double.parseDouble(fstElmnt.getAttribute("w"));
					int height = (int) Double.parseDouble(fstElmnt.getAttribute("h"));
					inputs.add(new Rectangle(x, y, width, height));
				}
			}
			
			//then read the evaluation markups
			List<Rectangle> evals = new ArrayList<Rectangle>();
			nodeLst = doc.getElementsByTagName("eval");
			for (int s = 0; s < nodeLst.getLength(); s++) 
			{
				Node fstNode = nodeLst.item(s);
				if (fstNode.getNodeType() == Node.ELEMENT_NODE)
				{
					Element fstElmnt = (Element) fstNode;
					int x = (int) Double.parseDouble(fstElmnt.getAttribute("x"));
					int y = (int) Double.parseDouble(fstElmnt.getAttribute("y"));
					int width = (int) Double.parseDouble(fstElmnt.getAttribute("w"));
					int height = (int) Double.parseDouble(fstElmnt.getAttribute("h"));
					evals.add(new Rectangle(x, y, width, height));
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
