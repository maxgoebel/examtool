package utils;

import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.ui.IWorkbenchWindow;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import utils.model.ExamModel;
import utils.model.ExamSheet;
import utils.qrcode.BufferedImageLuminanceSource;
import annotationide.Activator;
import at.tuwien.prip.common.datastructures.HashMapList;
import at.tuwien.prip.common.datastructures.MapList;
import at.tuwien.prip.common.utils.ListUtils;
import at.tuwien.prip.common.utils.SimpleTimer;

import com.google.zxing.BinaryBitmap;
import com.google.zxing.DecodeHintType;
import com.google.zxing.LuminanceSource;
import com.google.zxing.ReaderException;
import com.google.zxing.Result;
import com.google.zxing.common.HybridBinarizer;
import com.google.zxing.qrcode.QRCodeReader;
import com.sun.pdfview.PDFFile;
import com.sun.pdfview.PDFPage;

/**
 * LoadExamJob.java
 *
 * 
 *
 * @author max
 * @date Jul 3, 2012
 */
public class LoadExamJob extends Job 
{
	private final IWorkbenchWindow window;
	
	public static String SEPARATOR = ",";
	
	private String fileName = "";

//	private 
	/**
	 * 
	 * @param name
	 * @param parentDir
	 */
	public LoadExamJob(IWorkbenchWindow window, String name, String fileName) 
	{
		super(name);
		
		this.window = window;
		this.fileName = fileName;
	}

	@Override
	protected IStatus run(IProgressMonitor monitor) 
	{
		final ExamModel model = new ExamModel();
		String xmlFile = "geo.xml";
		String parentDir = null;

		List<String> pdfFiles = new ArrayList<String>();

		//		BusyIndicator.showWhile(get, runnable);

		if (fileName==null || !new File(fileName).exists())
		{
			return Status.CANCEL_STATUS;
		}
		
		if (fileName.endsWith(".csv") || fileName.endsWith(".CSV"))
		{
			parentDir = new File(fileName).getParent();
		} 
		else 
		{
			try 
			{
				parentDir = FileUtils.extractZip(fileName);
				model.setPath(parentDir);
			} 
			catch (ZipException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		System.out.println("Parent dir: "+parentDir);
		if (parentDir!=null && new File(parentDir).exists() && new File(parentDir).isDirectory())
		{
			//find and load XML geometric model
			xmlFile = parentDir + File.separator + xmlFile;
		}

		String[] dirFiles = new File(parentDir).list(new FilenameFilter() {

			@Override
			public boolean accept(File dir, String name) {
				if (name.endsWith(".pdf") || name.endsWith(".PDF"))
				{
					return true;
				}
				return false;
			}
		});

		//load each PDF file in directory
		for (String dirFile : dirFiles) { pdfFiles.add(parentDir + File.separator + dirFile); }

		if (!new File(xmlFile).exists())
		{
			xmlFile = "geo.xml";
			xmlFile = Activator.loadResource(xmlFile).getAbsolutePath();
		}
		processXML(xmlFile, model);

		monitor.beginTask("Loading exams", pdfFiles.size());
		
		//load PDF files
		List<ExamSheet> sheets = new ArrayList<ExamSheet>();
		for (String fileName : pdfFiles)
		{
			monitor.subTask("Processing exam: "+fileName);
			int pageNum = 0;
			String fname = fileName;
			SimpleTimer timer = new SimpleTimer();
			timer.startTask(0);
			//look for accompanying dot file
			String name[] = fileName.split("\\.")[0].split("\\-");
			if (name.length>0) 
			{
				fname = name[0].trim();
				if (name.length==2) 
				{
					try
					{
						pageNum = Integer.parseInt(name[1].trim());
					}
					catch (NumberFormatException e) {
						continue;
					}
				}
			} 

			//load PDF page and image
			PDFPage page = null;
			try 
			{
				//load page
				timer.startTask(1);
				RandomAccessFile raf = new RandomAccessFile (fileName, "r");
				FileChannel fc = raf.getChannel ();
				ByteBuffer buf = fc.map (FileChannel.MapMode.READ_ONLY, 0, fc.size ());
				PDFFile pdfFile = new PDFFile (buf);
				page = pdfFile.getPage(pageNum-1);
				timer.stopTask(1);
				System.out.print("loaded document "+fileName+" in "+timer.getTimeMillis(1)+"ms\n");


				if (page!=null)
				{
					String studentId = fname.substring(fname.lastIndexOf(File.separator)+1);
					ExamSheet sheet = processPdf(studentId, page, pageNum, model);							
					sheet.setFileName(fname);

					sheet.setStudentID(studentId);
					sheet.setPageNum(pageNum);
					sheets.add(sheet);
				}		
				monitor.worked(1);
			}
			catch (FileNotFoundException e) {
				e.printStackTrace();
			} 
			catch (IOException e) {
				e.printStackTrace();
			}

			timer.stopTask(0);
			System.out.println("Processed document "+fileName+ " in " + timer.getTimeMillis(0) + "ms\n");
		}
		
		Collections.sort(sheets);
		Collections.reverse(sheets);
		model.setPages((ExamSheet[]) ListUtils.toArray(sheets, ExamSheet.class));
		
		if (fileName.endsWith(".csv"))
		{
			//read in selections from CSV
			try
			{
				// Open the file that is the first 
				// command line parameter
				FileInputStream fstream = new FileInputStream(fileName);
				// Get the object of DataInputStream
				DataInputStream in = new DataInputStream(fstream);
				BufferedReader br = new BufferedReader(new InputStreamReader(in));
				String strLine;
				//Read File Line By Line
				while ((strLine = br.readLine()) != null)  
				{
					if (!strLine.contains(",")) {
						if (strLine.contains(";")) {
							SEPARATOR = ";";
						}
					}
					String[] entries = strLine.split(SEPARATOR);
					String matString = entries[0];
					try 
					{
						Integer.parseInt(matString);
					} 
					catch (NumberFormatException e)
					{
						continue; //possibly header?
					}
					while (matString.length()!=7) 
					{
						matString = "0"+matString; //prepend zeros until length is 7
					}

					//find affected sheets
					ExamSheet s1 = model.findSheet(matString, 0);
					ExamSheet s2 = model.findSheet(matString, 1);

					//update colors
					s1.clearPoints();

					String qrCode = entries[2].trim();

					s1.setPoint(0, entries[1]); //matrikelnr.
					s1.setQrCode(qrCode);   //qr code

					if (entries[1].equals("g")) {
						s1.setStatus(3); //mark good
					}

					int idx = 1;
					String[] quests = qrCode.split("\\s");
					for (String quest : quests) 
					{
						int q = Integer.parseInt(quest);
						s1.setPoint(idx, entries[q+3]);
						idx++;
					}

					if (entries.length>=32) {
						s1.setComment(entries[31]);
					}

					int j=13;
					s2.clearPoints();
					for (int i=0; i<s2.getPoints().length; i++) 
					{
						s2.setPoint(i, entries[j+i]);
					}

					if (entries.length==33) {
						s2.setComment(entries[32]);
					}

					if (entries[13].equals("g")) {
						s2.setStatus(3); //mark good
					}
				}
				//Close the input stream
				in.close();
			}catch (Exception e)
			{ //Catch exception if any
				System.err.println("Error: " + e.getMessage());
				e.printStackTrace();
			}
		}

		window.getShell().getDisplay().asyncExec(
				new Runnable() 
				{
					public void run() 
					{
						Activator.modelControl.modelChanged(model);
					}
				}
				);
		return Status.OK_STATUS;
	}

	/**
	 * 
	 * @param fileName
	 * @param model
	 * @return
	 */
	public ExamSheet processPdf(String fileName, PDFPage page, int pageNum, ExamModel model) 
	{		
		List<org.eclipse.swt.graphics.Rectangle> evalRectSWT = new ArrayList<org.eclipse.swt.graphics.Rectangle>();
		List<org.eclipse.swt.graphics.Rectangle> inputBoxes = new ArrayList<org.eclipse.swt.graphics.Rectangle>();
		String code = null;
		
		//extract input fields
		if (model!=null) 
		{
			//process inputs...	
			List<Rectangle> inputs = model.getInputFields().get(pageNum);
			if (inputs!=null)
			{
				Rectangle2D r2d = page.getBBox ();
//				int i=0;
				for (Rectangle input : inputs)
				{
					Rectangle2D clip = new Rectangle2D.Double(input.x, r2d.getHeight() - input.y - input.height, input.width, input.height);
					BufferedImage img = PDFUtils.getImageFromPDFPageAWT(page,1,clip);
					
					inputBoxes.add(new org.eclipse.swt.graphics.Rectangle(input.x, input.y, input.width, input.height));
					
//					CropImageFilter filter =  new CropImageFilter(input.x, input.y, input.width, input.height);
//					Toolkit tk = Toolkit.getDefaultToolkit();
//					java.awt.Image cropImage = tk.createImage(new FilteredImageSource(img.getSource(),filter));
//					img = PDFUtils.toBufferedImage(cropImage);
//					img = resizeImage(img, BufferedImage.TYPE_INT_ARGB);
//					img = PDFUtils.binarize(img);
//					try {
//					    // retrieve image
//					    File outputfile = new File("/home/max/qrcode-"+fileName+".png");
//					    ImageIO.write(img, "png", outputfile);
//					} 
//					catch (IOException e) 
//					{
//						e.printStackTrace();
//					}
					
//					i++;
					
					//convert the image to a binary bitmap source
					LuminanceSource source = new BufferedImageLuminanceSource(img);
					BinaryBitmap bitmap = new BinaryBitmap(new HybridBinarizer(source));

					//decode the QR code
					QRCodeReader reader = new QRCodeReader();
					Result result = null;
					try 
					{
						Hashtable<DecodeHintType, String> hints = new Hashtable<DecodeHintType, String>(2);
						hints.put(DecodeHintType.CHARACTER_SET, "UTF-8");
						result = reader.decode(bitmap, hints);
					} 
					catch (ReaderException e) 
					{
						e.printStackTrace();
					}     
					if (result!=null) {
						code = result.getText();
					}
				}
				
			}
			
			//process selections...
			List<Rectangle> evals = model.getSelectionBoxes().get(pageNum);
			if (evals!=null)
			{
				for (Rectangle eval : evals)
				{
					evalRectSWT.add(new org.eclipse.swt.graphics.Rectangle(eval.x, eval.y, eval.width, eval.height));	       
				}
			}
		}

//		ImageData imgData = PDFUtils.getImageFromPDFPage(page,1);

		ExamSheet sheet = new ExamSheet("", "", pageNum);
		//sheet.initPointset(evalRectSWT.size());
		sheet.setPage(page);
//		sheet.setImgData(imgData);
		sheet.setSelectionBoxes(evalRectSWT);
		sheet.setInputBoxes(inputBoxes);
		sheet.setQrCode(code);
		return sheet;
	}
	
	/**
	 * 
	 * @param fileName
	 * @param model
	 */
	public static void processXML(String fileName, ExamModel model) 
	{
		try 
		{
			File file = new File(fileName);
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = dbf.newDocumentBuilder();
			Document doc = db.parse(file);
			doc.getDocumentElement().normalize();

			MapList<Integer,Rectangle> inputs = new HashMapList<Integer,Rectangle>();
			MapList<Integer,Rectangle> evals = new HashMapList<Integer,Rectangle>();
			MapList<Integer,Rectangle> grades = new HashMapList<Integer,Rectangle>();

			Map<Rectangle, String> boxTextMap = new HashMap<Rectangle, String>();
			Map<Rectangle, Integer> highlightIds = new HashMap<Rectangle,Integer>();

			//read the document
			Rectangle bounds = null;
			String text = null;

			NodeList docList = doc.getElementsByTagName("document");
			if (docList.getLength()==1) 
			{
				Node n = docList.item(0);
				int x = Integer.parseInt(n.getAttributes().getNamedItem("x").getNodeValue());
				int y = Integer.parseInt(n.getAttributes().getNamedItem("y").getNodeValue());
				int w = Integer.parseInt(n.getAttributes().getNamedItem("w").getNodeValue());
				int h = Integer.parseInt(n.getAttributes().getNamedItem("h").getNodeValue());

				bounds = new Rectangle(x, y, w, h);
			}

			//read the pages
			NodeList pageList = doc.getElementsByTagName("page");
			for (int p=0; p < pageList.getLength(); p++) 
			{
				Node page = pageList.item(p);
				String sNum = page.getAttributes().getNamedItem("number").getNodeValue();
				int num = Integer.parseInt(sNum);

				//selection boxes
				NodeList nodeLst = ((Element)page).getElementsByTagName("eval");
				for (int s = 0; s < nodeLst.getLength(); s++) 
				{
					Node node = nodeLst.item(s);
					if (node.getNodeType() == Node.ELEMENT_NODE)
					{
						if ("eval".equals(node.getNodeName()))
						{
							Element fstElmnt = (Element) node;
							int x = (int) Double.parseDouble(fstElmnt.getAttribute("x"));
							int y = (int) Double.parseDouble(fstElmnt.getAttribute("y"));
							int width = (int) Double.parseDouble(fstElmnt.getAttribute("w"));
							int height = (int) Double.parseDouble(fstElmnt.getAttribute("h"));

							Rectangle r = new Rectangle(x+20,y-20,width,height);
							r = PDFUtils.flipReverseRectangle(r, bounds);
							evals.putmore(num, r);

							text = fstElmnt.getTextContent();
							if (text!=null && text.length()>0) {
								boxTextMap.put(r, text);
							}
						}
					}
				}		

				//input boxes
				NodeList ins = ((Element)page).getElementsByTagName("input");
				for (int q=0; q<ins.getLength(); q++) 
				{
					Node in = ins.item(q);

					if ("input".equals(in.getNodeName())) 
					{
						Element fstElmnt = (Element) in;
						int x = (int) Double.parseDouble(fstElmnt.getAttribute("x"));
						int y = (int) Double.parseDouble(fstElmnt.getAttribute("y"));
						int width = (int) Double.parseDouble(fstElmnt.getAttribute("w"));
						int height = (int) Double.parseDouble(fstElmnt.getAttribute("h"));
						Rectangle r = new Rectangle(x,y,width,height);
						r = PDFUtils.flipReverseRectangle(r, bounds);
						inputs.putmore(num, r);
					}
				}

				//grade boxes
				NodeList grds = ((Element)page).getElementsByTagName("grade");
				for (int q=0; q<grds.getLength(); q++) 
				{
					Node grade = grds.item(q);

					if ("grade".equals(grade.getNodeName())) 
					{
						Element fstElmnt = (Element) grade;
						int x = (int) Double.parseDouble(fstElmnt.getAttribute("x"));
						int y = (int) Double.parseDouble(fstElmnt.getAttribute("y"));
						int width = (int) Double.parseDouble(fstElmnt.getAttribute("w"));
						int height = (int) Double.parseDouble(fstElmnt.getAttribute("h"));
						Rectangle r = new Rectangle(x,y,width,height);
						r = PDFUtils.flipReverseRectangle(r, bounds);
						grades.putmore(num, r);
					}
				}
			}

			model.setInputFields(inputs);
			model.setSelectionBoxes(evals);	
			model.setGradeFields(grades);

			model.setHighlightIds(highlightIds);
			model.setBoxTextMap(boxTextMap);
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
	}
}
