package utils;
public class LUT
{
    private int[] value;
    private double a = 1;
    private double b = 0;
   
    public LUT()
    {
        value = new int[256];
        create();
    }
 
    public int getLUTValue(int x)
    {
        return value[x];
    }
 
    public void setA(double a)
    {
        this.a = a;
        create();
    }
 
    public void setB(double b)
    {
        this.b = b;
        create();
    }
 
    private void create()
    {
        for (int x = 0; x <= 255; x++)
        {
            value[x] = (int)(a * x + b);
           
            if (value[x] > 255)
            {
                value[x] = 255;
            }
           
            if (value[x] < 0)
            {
                value[x] = 0;
            }
        }
    }
}