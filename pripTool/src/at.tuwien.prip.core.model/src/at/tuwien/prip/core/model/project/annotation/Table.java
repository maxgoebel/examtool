package at.tuwien.prip.core.model.project.annotation;

import java.util.List;

public class Table {

	List<Region> regions;
	
	int id;
	
	public Table(int id) {
		this.id = id;
	}
	
	public void setRegions(List<Region> regions) {
		this.regions = regions;
	}
	
	public List<Region> getRegions() {
		return regions;
	}
	
	public int getId() {
		return id;
	}
	
}
