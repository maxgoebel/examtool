package at.tuwien.prip.core.model.document.layout;

import java.awt.Rectangle;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.List;

import at.tuwien.prip.core.model.graph.DocNode;

public class LayoutObject {

	protected List<DocNode> elements = new ArrayList<DocNode>();

	private Rectangle bounds = new Rectangle(0,0,0,0);

	public List<DocNode> getElements() {
		return elements;
	}

	public Rectangle getBounds() {
		return bounds;
	}

	public void addElement (DocNode node) {
		if (!elements.contains(node)) {

			if (elements.size()==0) {
				Rectangle2D r2d = node.getBoundingBox();
				bounds = new Rectangle(r2d.getBounds());
			} else {
				bounds.add(node.getBoundingBox());
			}
			elements.add(node);
		}
	}
}
