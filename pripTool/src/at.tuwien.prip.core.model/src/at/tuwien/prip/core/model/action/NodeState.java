package at.tuwien.prip.core.model.action;

import at.tuwien.prip.core.model.graph.base.BaseNode;

/**
 * NodeState.java
 *
 *
 * 
 * @author: Max Goebel <mcgoebel@gmail.com>
 * @date: Mar 29, 2011
 */
public class NodeState implements IState {

	private BaseNode node;

	public NodeState(BaseNode node) {
		this.node = node;
	}
	
	public void setNode(BaseNode node) {
		this.node = node;
	}

	public BaseNode getNode() {
		return node;
	}
	
	@Override
	public String toString() {
		return node.toString();
	}
}
