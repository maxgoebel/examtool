package at.tuwien.prip.core.model.agent.relations;

/**
 * AlignmentType.java
 * 
 *
 *
 * @author mcg <mcgoebel@gmail.com>
 * Jul 15, 2012
 */
public enum AlignmentType 
{

	ALL_HORIZ, ALL_VERT, MIXED,
	HORIZ_CENTER, HORIZ_TOP, HORIZ_BOTTOM,
	VERT_CENTER, VERT_RIGHT, VERT_LEFT
	
}

