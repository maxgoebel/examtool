package at.tuwien.prip.core.model.graph.hier.level;

import at.tuwien.prip.core.model.graph.DocEdge;
import at.tuwien.prip.core.model.graph.DocNode;
import at.tuwien.prip.core.model.graph.DocumentConstants;

/**
 * CharLevel.java
 *
 *
 * 
 * @author: Max Goebel <mcgoebel@gmail.com>
 * @date: Mar 4, 2011
 */
public class CharLevel extends AbstractLevel<DocNode,DocEdge>
implements IGraphLevel<DocNode,DocEdge> 
{
	
	/**
	 * Constructor.
	 * @param stack
	 */
	public CharLevel(LevelGraph<DocNode,DocEdge> stack) {
		this.parent = stack;
	}
	
	@Override
	public int getLevel() {
		return DocumentConstants.CHAR_LEVEL;
	}

}
