package at.tuwien.prip.core.model.document.segments;



/**
 * Segments which are used as spacers, e.g. BlankCell, indentation
 * 
 * @author Tamir Hassan, pdfanalyser@tamirhassan.com
 * @version PDF Analyser 0.9
 */
public interface IBlankSegment
{
}
