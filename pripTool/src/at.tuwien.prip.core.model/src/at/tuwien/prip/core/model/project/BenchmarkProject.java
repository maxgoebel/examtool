package at.tuwien.prip.core.model.project;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import at.tuwien.prip.core.model.project.bench.Benchmark;
import at.tuwien.prip.core.model.project.bench.BenchmarkItem;
import at.tuwien.prip.core.model.project.document.AbstractDocument;

/**
 * BenchmarkProject.java
 * 
 *
 *
 * @author mcg <mcgoebel@gmail.com>
 * Nov 8, 2012
 */
@Entity
public class BenchmarkProject 
implements IProject
{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	private Benchmark benchmark;
	
	private String name;
	
	private String projectResource;
	
	/**
	 * Constructor.
	 */
	public BenchmarkProject()
	{
		
	}
	
	/**
	 * Constructor.
	 */
	public BenchmarkProject(String name, String projectResource) 
	{
		this.name = name;
		this.projectResource = projectResource;
		this.benchmark = new Benchmark(name);
	}
	
	public Benchmark getBenchmark() {
		return benchmark;
	}
		
	@Override
	public String getName() {
		return name;
	}
	
//	protected abstract ProjectPage createProjectPage();
	
	@Override
	public void addDocument(AbstractDocument document) 
	{
		if (benchmark!=null && document instanceof BenchmarkItem)
		{
			BenchmarkItem item = (BenchmarkItem) document;
			benchmark.getItems().add(item);
		}
	}

	@Override
	public List<AbstractDocument> getDocuments() {
		List<AbstractDocument> result = new ArrayList<AbstractDocument>();
		for (AbstractDocument doc : benchmark.getItems())
		{
			result.add(doc);
		}
		return result;
	}
	
	public void setBenchmark(Benchmark benchmark) {
		this.benchmark = benchmark;
	}

	@Override
	public void removeDocument(AbstractDocument document) 
	{
		if (benchmark!=null && document instanceof BenchmarkItem)
		{
			BenchmarkItem item = (BenchmarkItem) document;
			benchmark.getItems().remove(item);
		}
	}
	
	@Override
	public String getProjectResource() {
		return projectResource;
	}

	public void setProjectResource(String projectResource) {
		this.projectResource = projectResource;
	}
	
	@Override
	public void setName(String name) {
		this.name = name;
	}
	
}
