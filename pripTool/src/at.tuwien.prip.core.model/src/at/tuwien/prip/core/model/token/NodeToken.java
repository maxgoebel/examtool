package at.tuwien.prip.core.model.token;

import at.tuwien.prip.core.model.graph.base.BaseNode;
import at.tuwien.prip.docwrap.core.distance.eval.NodeMatcher;

/**
 * DocNodeToken.java
 *
 *
 * 
 * @author: Max Goebel <mcgoebel@gmail.com>
 * @date: Apr 2, 2011
 */
public class NodeToken extends GraphToken 
{

	private BaseNode node;
	
	public NodeToken(BaseNode node) {
		super(node.toString());
		this.node = node;
	}

	public BaseNode getNode() {
		return node;
	}

	@Override
	public String toString() {
		return node.toString();
	}
	
	@Override
	public boolean equals(Object obj) 
	{
		if (!(obj instanceof NodeToken)) 
		{
			return false;
		}

		NodeToken other = (NodeToken) obj;
		double score = NodeMatcher.matchNodesSim(other.node, this.node);
		return score>0.5;
	}
	
}
