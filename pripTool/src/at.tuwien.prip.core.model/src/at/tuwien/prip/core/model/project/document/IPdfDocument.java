package at.tuwien.prip.core.model.project.document;

import at.tuwien.prip.core.model.graph.ISegmentGraph;

import com.sun.pdfview.PDFFile;

public interface IPdfDocument extends IDocument
{
	PDFFile getPdfFile();

	int getPageNum();

	int getNumPages();

	void setDocumentGraph(int pageNum, ISegmentGraph hGraph);

	void setPageNum(int i);

	void setNumPages(int numPages);

	void setPdfFile(PDFFile pdfFile);

}
