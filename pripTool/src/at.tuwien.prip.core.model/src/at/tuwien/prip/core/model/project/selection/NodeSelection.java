package at.tuwien.prip.core.model.project.selection;

import javax.persistence.Entity;
import javax.persistence.Transient;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import at.tuwien.prip.core.model.utils.DOMHelper;

/**
 * NodeSelection.java
 * 
 *
 *
 * @author mcg <mcgoebel@gmail.com>
 * Nov 12, 2012
 */
@Entity
public class NodeSelection extends AbstractSelection
{	
	@Transient
	private Document document;
	
	private String documentUri;
	
	private String targetXPath;

	private String rootXPath;
	
	@Transient
	private Node selectedNode;
	
	@Transient
	private Node root;
	
	public NodeSelection() 
	{
	}
	
	public NodeSelection(Node node)
	{
		this.selectedNode = node;
		this.targetXPath = DOMHelper.XPath.getExactXPath(node);
	}
	
	public void setTargetXPath(String targetXpath) {
		this.targetXPath = targetXpath;
	}

	public String getTargetXPath() {
		return targetXPath;
	}

	public void setSelectedNode(Node selectedNode) {
		this.selectedNode = selectedNode;
	}

	public Node getSelectedNode() {
		return selectedNode;
	}

	public String getRootXPath() {
		return rootXPath;
	}
	
	public void setRootXPath(String rootXPath) {
		this.rootXPath = rootXPath;
	}
	
	public String getDocumentUri() {
		return documentUri;
	}
	
	public void setDocumentUri(String documentUri) {
		this.documentUri = documentUri;
	}

	public void setDocument(Document doc) {
		this.document = doc;
	}
	
	public Document getDocument() {
		return document;
	}

	public void setRoot(Element root) {
		this.root = root;
	}
	
	public Node getRoot() {
		return root;
	}
}
