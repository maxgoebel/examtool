package at.tuwien.prip.core.model.agent.relations;

import at.tuwien.prip.core.model.agent.labels.ILabel;
import at.tuwien.prip.core.model.agent.states.AgentState;
import at.tuwien.prip.core.model.document.layout.LayoutRelationType;

/**
 * LayoutRelation.java
 * 
 *
 *
 * @author mcg <mcgoebel@gmail.com>
 * Aug 25, 2012
 */
public abstract class LayoutRelation implements ILabel
{
	private LayoutRelationType relType;
	
	private String relation;
	/*
	 *  a 0-1 confidence for the proposed 
	 * layout to be good, with 1 being best
	 */
	private double confidence;	
	
	private AgentState fromState;
	
	private AgentState toState;
	
	/**
	 * Constructor
	 */
	public LayoutRelation(LayoutRelationType relType,
			String relation,
			double confidence,
			AgentState fromState,
			AgentState toState) 
	{
		this.relType = relType;
		this.relation = relation;
		this.confidence = confidence;
		this.fromState = fromState;
		this.toState = toState;
	}
	
	public String getRelation() {
		return relation;
	}
	
	public double getConfidence() {
		return confidence;
	}
	
	public LayoutRelationType getRelType() {
		return relType;
	}
	
	public AgentState getFromState() {
		return fromState;
	}
	
	public AgentState getToState() {
		return toState;
	}
}
