package at.tuwien.prip.core.model.graph.comparators;

import java.util.Comparator;

import at.tuwien.prip.core.model.document.segments.GenericSegment;

/**
 * 
 * WidthComparator.java
 *
 *
 * @author mcg <mcgoebel@gmail.com>
 * @date Sep 6, 2011
 */
public class WidthComparator implements Comparator<GenericSegment>
{
	public int compare(GenericSegment obj1, GenericSegment obj2)
	{
		// sorts in x order
		double x1 = obj1.getX2() - obj1.getX1();
		double x2 = obj2.getX2() - obj2.getX1();

		return (int) (x1 - x2);
	}

	public boolean equals(Object obj)
	{
		return obj.equals(this);
	}
}