package at.tuwien.prip.core.model.graph.base;

import java.util.ArrayList;
import java.util.List;

import at.tuwien.prip.core.model.agent.constraint.EdgeConstraint;
import at.tuwien.prip.core.model.graph.EEdgeRelation;
import at.tuwien.prip.core.model.graph.EdgeRelation;

public class BaseEdge<S extends BaseNode> 
implements Cloneable
{
	protected S from;
	
	protected S to;
	
    protected String relation;
    protected List<EdgeRelation> edgeRelations;
    protected List<EdgeConstraint> constraints;
    protected float confidence;
	
    /**
     * Constructor.
     */
    public BaseEdge() 
    { 
    	this.edgeRelations = new ArrayList<EdgeRelation>();
    	this.constraints = new ArrayList<EdgeConstraint>();
    }
    
    /**
     * Constructor.
     * 
     * @param from
     * @param to
     * @param relation
     */
    public BaseEdge (S from, S to, String relation)
    {
    	this.from = from;
    	this.to = to;
    	this.relation = relation;
    }
    
    /**
     * Copy constructor.
     * 
     * @param other
     */
    public BaseEdge (BaseEdge<S> other) 
    {
    	this.edgeRelations = new ArrayList<EdgeRelation>(other.getEdgeRelations());
    	this.confidence = other.confidence;
    	this.constraints = new ArrayList<EdgeConstraint>();
    	this.relation = other.relation;
    	this.from = other.getFrom();
    	this.to = other.getTo();
    }
    
    public S getFrom() {
		return from;
	}

	public void setFrom(S from) {
		this.from = from;
	}

	public S getTo() {
		return to;
	}

	public void setTo(S to) {
		this.to = to;
	}
	
	public boolean hasRelation (EEdgeRelation relation) {
		for (EdgeRelation rel : edgeRelations) {
			if (rel.getRelation().equals(relation)) {
				return true;
			}
		}
		return false;
	}
	
	public String getRelation() {
		return relation;
	}

	public void setRelation(String relation) {
		this.relation = relation;
	}

	public List<EdgeRelation> getEdgeRelations() {
		return edgeRelations;
	}

	public float getConfidence() {
		return confidence;
	}

	public void setConfidence(float confidence) {
		this.confidence = confidence;
	}
	
	@Override
	protected Object clone() 
	{
		try {
			return super.clone();
		}
		catch (CloneNotSupportedException e) {
			// This should never happen
			throw new InternalError(e.toString());
		}
	}
}
