package at.tuwien.prip.core.model.project.annotation;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import at.tuwien.prip.core.model.project.selection.AbstractSelection;

/**
 * AnnotationPage.java
 * 
 *
 *
 * @author mcg <mcgoebel@gmail.com>
 * Nov 12, 2012
 */
@Entity
public class AnnotationPage 
{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	private int pageNum;
	
	@OneToMany
	private List<AbstractSelection> items;
	
	/**
	 * Constructor.
	 */
	public AnnotationPage()	{ }
	
	/**
	 * Constructor
	 * @param pageNum
	 */
	public AnnotationPage(int pageNum)
	{
		this.pageNum = pageNum;
		items = new ArrayList<AbstractSelection>();
	}
	
	public int getPageNum() {
		return pageNum;
	}
	
	public void setPageNum(int pageNum) {
		this.pageNum = pageNum;
	}

	public List<AbstractSelection> getItems() {
		return items;
	}
	
	public void setItems(List<AbstractSelection> items) {
		this.items = items;
	}
}
