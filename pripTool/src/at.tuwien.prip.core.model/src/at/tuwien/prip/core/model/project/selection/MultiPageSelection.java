package at.tuwien.prip.core.model.project.selection;

import java.util.List;

import javax.persistence.OneToMany;

import at.tuwien.prip.core.model.project.annotation.AnnotationPage;

/**
 * MultiPageSelection.java
 * 
 *
 *
 * @author mcg <mcgoebel@gmail.com>
 * Nov 19, 2012
 */
public class MultiPageSelection extends AbstractSelection
{
	@OneToMany
	private List<AnnotationPage> pages;
	
	public List<AnnotationPage> getPages() {
		return pages;
	}
	
	public void setPages(List<AnnotationPage> pages) {
		this.pages = pages;
	}
}
