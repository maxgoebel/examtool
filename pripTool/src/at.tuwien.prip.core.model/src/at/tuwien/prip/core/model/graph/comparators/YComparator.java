package at.tuwien.prip.core.model.graph.comparators;

import java.util.Comparator;

import at.tuwien.prip.core.model.document.segments.GenericSegment;

/**
 * @author Tamir Hassan, pdfanalyser@tamirhassan.com
 * @version PDF Analyser 0.9
 */
public class YComparator implements Comparator<GenericSegment>
{
	public int compare(GenericSegment obj1, GenericSegment obj2)
	{
		// sorts in y order
		int y1 = (int) obj1.getY1();
		int y2 = (int) obj2.getY1();

		return (int) (y2 - y1);
	}

}