package at.tuwien.prip.core.model.graph;

/**
 * EEdgeRelation.java
 *
 * Describes an edge relation between two nodes as an 
 * concrete relationship.
 * 
 * @author: Max Goebel <mcgoebel@gmail.com>
 * @date: Apr 8, 2011
 */
public enum EEdgeRelation 
{

	LEFT_OF, RIGHT_OF, ABOVE, BELOW, 
	INFERIOR_TO, SUPERIOR_TO, 
	KEY_VALUE, SYMBOL_REL,
	CENTER_ALIGNED, LEFT_ALIGNED, RIGHT_ALIGNED,
	EQUAL_HEIGHT, EQUAL_WIDTH,
	EQUAL_SEGMENT, EQUAL_SEMANTIC, EQUAL_TEXT,
	READING_ORDER_BEFORE, READING_ORDER_AFTER

}
