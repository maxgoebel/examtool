package at.tuwien.prip.core.model.document.semantics;

public class OrderedPattern 
{
	public WordSemantics[] semantics;
	
	public OrderedPattern(int size) 
	{
		semantics = new WordSemantics[size];
	}
	
	@Override
	public String toString() 
	{
		StringBuffer sb = new StringBuffer();
		if (semantics==null) return sb.toString();
		
		for (WordSemantics ws : semantics) {
			sb.append(ws.name() + " ");
		}
		return sb.toString().trim();
	}
}
