package at.tuwien.prip.core.model.agent.states;

import java.awt.Font;
import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

import at.tuwien.prip.common.utils.ListUtils;
import at.tuwien.prip.core.model.agent.AC;
import at.tuwien.prip.core.model.agent.IAgent;
import at.tuwien.prip.core.model.agent.attributes.AgentStateAttribute;
import at.tuwien.prip.core.model.agent.attributes.AttributeType;
import at.tuwien.prip.core.model.agent.attributes.SemanticAttribute;
import at.tuwien.prip.core.model.agent.constraint.EdgeConstraint;
import at.tuwien.prip.core.model.agent.labels.LayoutLabel;
import at.tuwien.prip.core.model.document.segments.SegmentType;
import at.tuwien.prip.core.model.document.semantics.SemanticText;
import at.tuwien.prip.core.model.document.semantics.WordSemantics;
import at.tuwien.prip.core.model.graph.DocNode;
import at.tuwien.prip.core.model.graph.DocumentGraph;
import at.tuwien.prip.core.model.graph.ISegmentGraph;
import at.tuwien.prip.core.model.graph.base.BaseNode;

/**
 * AgentState.java
 * 
 *
 *
 * @author mcg <mcgoebel@gmail.com>
 * Oct 24, 2011
 */
public abstract class AgentState extends BaseNode
implements IAgentState, java.io.Serializable 
{
	/* */
	private static final long serialVersionUID = 7360338855762161145L;

	/* a name */
	protected String name;
	
	/* a list of attributes */
	protected transient List<AgentStateAttribute> attributes;
	
	/* the 'owning' agent */
	private transient IAgent owner;

	/* remember conflicting states */
	private transient List<AgentState> conflictingStates;
	
	/* the supporting agents */
	private List<IAgent> support;
		
	/* the (sub) graph this state affects */
	private transient ISegmentGraph graph;

	private boolean reconsider;

	/* */
	private List<AgentState> parents;

	/* */
	private List<AgentState> children = new ArrayList<AgentState>();
	
	/* */
	private List<AgentState> decendents = new ArrayList<AgentState>();

	/* */
	private List<AgentState> dependents = new ArrayList<AgentState>();
	
	/* */
	private LayoutLabel annotation;

	/* the (edge) constraints associated with this state */
	private List<EdgeConstraint> constraints;

	protected SegmentType segType;
	
	/* Text specific information */
	protected TextContent textContent;

	
	/* a numeric type associated with this state */
	protected int stateLevel;

	private boolean dirty;

	private boolean blocked;

	private transient IAgent blockedBy;

	protected Rectangle bounds;
	
	protected volatile double utility;
	
	protected float confidence;
	
	protected double complexity;

	/**
	 * Constructor.
	 */
	private AgentState() 
	{
		this.attributes = new ArrayList<AgentStateAttribute>();
		this.parents = new ArrayList<AgentState>();
		this.constraints = new ArrayList<EdgeConstraint>();
		this.conflictingStates = new ArrayList<AgentState>();
		this.support = new ArrayList<IAgent>();
		owner = null;
	}
	
	/**
	 * Constructor.
	 * @param stateLevel
	 * @param segType
	 * @param layoutType
	 * @param owner
	 * @param bounds
	 * @param reconsider
	 */
	public AgentState (
			int stateLevel,
			SegmentType segType, 
			double complexity,
			double confidence,
			IAgent owner, 
			Rectangle bounds,
			boolean reconsider)
	{
		this();
		this.stateLevel = stateLevel;
		this.segType = segType;
		this.owner = owner;
		this.reconsider = reconsider;
		this.bounds = bounds;
	}

	/**
	 * Constructor.
	 * @param ll
	 * @param stateLevel
	 * @param type
	 * @param owner, the agent that owns this state
	 * @param reconsider, a flag to tell agent to reconsider even if owner
	 */
	public AgentState(
			LayoutLabel ll, 
			IAgent owner, 
			boolean reconsider)
	{
		this();
		this.stateLevel = ll.getStateLevel();
		this.segType = ll.getSegmentType();
		this.owner = owner;
		this.reconsider = reconsider;
		this.setAnnotation(ll);
		this.setConstraints(ll.getConstraints());

		// compute dimensions
		Rectangle dimensions = null;
		for (AgentState state : ll.getAffectedStates())
		{
			if (dimensions==null)
			{
				dimensions = state.getBounds();
			} 
			else
			{
				dimensions = dimensions.union(state.getBounds());
			}
		}
		this.bounds = dimensions;
		
		String text = "";
		Font font = null;
		for (AgentState affected : ll.getAffectedStates())
		{
			this.decendents.addAll(affected.getDecendents());
			this.decendents.add(affected);
			text += affected.getTextContent().getText()+" ";
			font = affected.getTextContent().getFont();
		}
		this.textContent = new TextContent(text.trim(), font);
		ListUtils.unique(decendents);
	}

	/**
	 * Constructor.
	 * Initialize the state with a graph. 
	 * @param graph
	 */
	public AgentState(ISegmentGraph dg) 
	{
		this();
		this.graph = dg;
		this.stateLevel = AC.A.PAGE; 
		this.bounds = dg.getDimensions();
	}
	
	/**
	 * Constructor.
	 * 
	 * @param node
	 */
	public AgentState(DocNode node) 
	{
		this();
		this.graph = new DocumentGraph();
		List<DocNode> nodes = new ArrayList<DocNode>();
		nodes.add(node);
		((DocumentGraph)graph).setNodes(nodes);
		if (node.getSegType()==SegmentType.Block) {
			this.stateLevel = AC.A.TEXTLINE; //basic
		}
		else if (node.getSegType()==SegmentType.Word) {
			this.stateLevel = AC.A.WORD; //basic
		}

		this.bounds = node.getBoundingBox().getBounds();
		this.segType = node.getSegType();
		this.name = node.getSegText();
		this.confidence = 1f;
		this.complexity = 1d;
		
		this.textContent = new TextContent(
				node.getSegText(), node.getFont());
	}
	
	/**
	 * 
	 * @param agent
	 * @return
	 */
	public boolean needsProcessing(IAgent agent)
	{
		if (getOwner()!=null && getOwner().getName().equals(agent.getName()))
		{
			return false;
		}
//		if (getAcceptors().contains(agent)) 
//		{
//			return false;
//		}
//		for (IAgent pb : processedBy)
//		{
//			if (pb.getName().equals(agent.getName()))
//			{
//				return false;
//			}
//		}
		return true;
	}

	/**
	 * 
	 * @return
	 */
	public List<AgentState> getLeafChildren ()
	{
		List<AgentState> leafs = new ArrayList<AgentState>();
		Stack<AgentState> stack = new Stack<AgentState>();
		stack.push(this);
		
		while (!stack.isEmpty())
		{
			AgentState top = stack.pop();
			List<AgentState> children = top.getChildren();
			for (AgentState child : children)
			{
				if (child.getChildren().size()>0)
				{
					stack.push(child);
				}
				else
				{
					leafs.add(child);
				}
			}
		}
		return leafs;
	}
	
	/**
	 * 
	 * @param state
	 */
	public void propagateUtility (AgentState state)
	{
		if (!this.dependents.contains(state))
		{
			dependents.add(state);
		}
		owner.computeUtility(this);
	}
	
	public double getComplexity() {
		return complexity;
	}
	
	public float getConfidence() {
		return confidence;
	}
	
	public Rectangle getBounds() {
		return bounds;
	}

	public List<EdgeConstraint> getConstraints() {
		return constraints;
	}

	public void setConstraints(List<EdgeConstraint> constraints) {
		this.constraints = constraints;
	}

	public void updateConstraints(List<EdgeConstraint> constraints) {
		synchronized (this) {
			this.constraints = constraints;
		}
	}

	public TextContent getTextContent() {
		return textContent;
	}
	
	public void setTextContent(TextContent textContent) {
		this.textContent = textContent;
	}
	
	public IAgent getOwner() {
		return owner;
	}

	public int getStateLevel() {
		return stateLevel;
	}

	public void setDirty (boolean dirty) {
		synchronized (this) {
			this.dirty = dirty;
		}
	}

	public ISegmentGraph getGraph () {
		return this.graph;
	}

//	public List<IAgent> getAcceptors() {
//		return acceptors;
//	}
//
//	public List<IAgent> getProcessedBy() {
//		return processedBy;
//	}
//	
//	public void addAcceptor(IAgent agent) {
//		if (!acceptors.contains(agent))	{
//			acceptors.add(agent);
//		}
//	}
//
//	public void clearAcceptors() {
//		acceptors.clear();
//	}

	public boolean isDirty() {
		return dirty;
	}

	public LayoutLabel getAnnotation() {
		return annotation;
	}

	public void setAnnotation(LayoutLabel annotation) {
		this.annotation = annotation;
	}

	public void setBlocked(boolean blocked) {
		this.blocked = blocked;
	}

	public boolean isBlocked() {
		return blocked;
	}

	public void setBlockedBy(IAgent blockedBy) {
		this.blockedBy = blockedBy;
	}

	public IAgent getBlockedBy() {
		return blockedBy;
	}

	public void setOwner(IAgent owner) {
		this.owner = owner;
	}

	/**
	 * 
	 * @param child
	 */
	public void addAsChild (AgentState child) 
	{
		if (!children.contains(child))
		{
			children.add(child);
		}
		if (!child.parents.contains(this))
		{
			child.parents.add(this);
		}
	}

	public void removeChild (AgentState child) 
	{
		children.remove(child);
	}

	public List<AgentState> getChildren() {
		return children;
	}
	
	public List<AgentState> getDecendents() {
		return decendents;
	}

	public List<AgentState> getParents() {
		return parents;
	}

	public SegmentType getSegType() {
		return segType;
	}

	public void setSegType(SegmentType segType) {
		this.segType = segType;
	}

	public boolean isReconsider() {
		return reconsider;
	}

	public void setReconsider(boolean reconsider) {
		this.reconsider = reconsider;
	}

	public int getX1() 	{
		return getBounds().x;
	}

	public int getX2() 	{
		return getBounds().x + getBounds().width;
	}

	public int getY1() 	{
		return getBounds().y;
	}

	public int getY2() 	{
		return getBounds().y + getBounds().height;
	}

	@Override
	public String toString() {
		if (this.name!=null) 
		{
			return this.name;
		}
		return super.toString();
	}
	
	/**
	 * 
	 */
	public synchronized double computeUtility () 
	{
		this.utility = 0d;
		for (AgentState child : children)
		{
			utility += child.getUtility();
		}
		return Math.max(0, utility);
	}
	
	/**
	 * 
	 * @param attribute
	 * @return
	 */
	public boolean containsAttribute(AttributeType attType)
	{
		for (AgentStateAttribute att : getAttributes())
		{
			if (att.getKey()==attType)
			{
				return true;
			}
		}	 
		return false;
	}
	
	/**
	 * 
	 * @return
	 */
	public boolean containsSemanticAttribute (WordSemantics semantics) 
	{
		for (AgentStateAttribute att : getAttributes())
		{
			if (att.getKey()==AttributeType.SEMANTIC)
			{
				SemanticAttribute semAtt = (SemanticAttribute) att;
				List<SemanticText> semList = semAtt.getSemantics();
				for (SemanticText semText : semList)
				{
					if (semText.containsSemantics(semantics))
					{
						return true;
					}
				}
			}
		}
		return false;
	}
	
	public void setUtility(double utility) {
		this.utility = utility;
	}
	
	public double getUtility() {
		return utility;
	}
	
	public List<AgentState> getDependents() {
		return dependents;
	}
	
	
	public void addAttribute (AgentStateAttribute attribute)
	{
		this.attributes.add(attribute);
	}
	
	public void removeAttribute (AgentStateAttribute attribute)
	{
		this.attributes.remove(attribute);
	}

	public List<AgentStateAttribute> getAttributes() {
		return attributes;
	}
	
	public List<IAgent> getSupport() {
		return support;
	}
	
	public List<AgentState> getConflictingStates() {
		return conflictingStates;
	}
	
	/**
	 * Add an agent as support
	 * @param agent
	 */
	public void addSupport (IAgent agent)
	{
		if (!this.support.contains(agent))
		{
			this.support.add(agent);
		}
	}
	
	/**
	 * Add a conflicting state
	 * @para m state
	 */
	public void addConflictingState (AgentState state)
	{
		if (!this.conflictingStates.contains(state))
		{
			this.conflictingStates.add(state);
		}
	}
	
}
