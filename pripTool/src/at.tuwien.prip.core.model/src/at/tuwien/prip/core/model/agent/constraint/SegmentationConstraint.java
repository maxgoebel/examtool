package at.tuwien.prip.core.model.agent.constraint;

/**
 * SegmentationConstraint.java
 * 
 * Segmentation constraint implementation.
 *
 * @author mcg <mcgoebel@gmail.com>
 * Jan 22, 2012
 */
public class SegmentationConstraint implements IConstraint
{
	private ConstraintType type;

	private double confidence = 0d;

	private ConstraintSource source;
	
	/**
	 * Constructor.
	 */
	public SegmentationConstraint() 
	{

	}
	
	@Override
	public boolean isConflicting(IConstraint other) {
		// TODO Auto-generated method stub
		return false;
	}
	
}
