package at.tuwien.prip.core.model.project.selection;

import javax.persistence.Entity;


/**
 * ExtractionItem.java
 * 
 *
 *
 * @author mcg <mcgoebel@gmail.com>
 * Nov 12, 2012
 */
@Entity
public class ExtractionItem extends SinglePageSelection 
{
	private String key;
	
	private String value;
	
	public String getKey() {
		return key;
	}
	
	public String getValue() {
		return value;
	}
	
	public void setValue(String value) {
		this.value = value;
	}
	
	public void setKey(String key) {
		this.key = key;
	}
}
