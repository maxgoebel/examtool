package at.tuwien.prip.core.model.document.semantics;

public class SemanticPattern {

	private WordSemantics semantic;
	
	private String text;
	
	/**
	 * 
	 * @param text
	 * @param semantic
	 */
	public SemanticPattern(String text, WordSemantics semantic)
	{
		this.semantic = semantic;
		this.text = text;
	}
	
	public WordSemantics getSemantic() {
		return semantic;
	}
	
	public String getText() {
		return text;
	}
	
	@Override
	public String toString() {
		return semantic + ":" + text;
	}
}
