package at.tuwien.prip.core.model.project.example;

import at.tuwien.prip.core.model.BinaryClass;

/**
 * IExample.java
 * 
 *
 *
 * @author mcg <mcgoebel@gmail.com>
 * Jul 7, 2012
 */
public interface IExample 
{
	public BinaryClass getClassification();
}
