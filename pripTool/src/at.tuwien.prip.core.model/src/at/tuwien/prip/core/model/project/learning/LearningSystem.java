package at.tuwien.prip.core.model.project.learning;

import java.util.List;

import at.tuwien.prip.core.model.project.document.IDocument;
import at.tuwien.prip.docwrap.wrapper.learners.ILearner;

/**
 * LearningSystem.java
 * 
 *
 *
 * @author mcg <mcgoebel@gmail.com>
 * Jun 30, 2012
 */
public class LearningSystem
{
	private List<IDocument> documentCollection;
	
	private ILearner learner;
	
	public List<IDocument> getDocumentCollection() {
		return documentCollection;
	}
	
	public ILearner getLearner() {
		return learner;
	}
	
	public void setDocumentCollection(List<IDocument> documentCollection) {
		this.documentCollection = documentCollection;
	}
	
	public void setLearner(ILearner learner) {
		this.learner = learner;
	}
}
