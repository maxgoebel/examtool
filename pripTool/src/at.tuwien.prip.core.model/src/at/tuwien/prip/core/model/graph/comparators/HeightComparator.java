package at.tuwien.prip.core.model.graph.comparators;

import java.util.Comparator;

import at.tuwien.prip.core.model.document.segments.GenericSegment;

public class HeightComparator implements Comparator<GenericSegment>
{
	public int compare(GenericSegment obj1, GenericSegment obj2)
	{
		// sorts in height order
		double y1 = obj1.getY2() - obj1.getY1();
		double y2 = obj2.getY2() - obj2.getY1();

		return (int) (y1 - y2);
	}

	public boolean equals(Object obj)
	{
		return obj.equals(this);
	}
}
