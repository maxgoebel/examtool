package at.tuwien.prip.core.model.agent.relations;

import at.tuwien.prip.core.model.agent.states.AgentState;
import at.tuwien.prip.core.model.document.layout.LayoutRelationType;

public class NeighbourRelation extends LayoutRelation
{

	/**
	 * Constructor.
	 * @param relation
	 * @param confidence
	 * @param fromState
	 * @param toState
	 */
	public NeighbourRelation(String relation,
			double confidence, AgentState fromState, AgentState toState) 
	{
		super(LayoutRelationType.Neighbourhood, relation, confidence, fromState, toState);
	}

}
