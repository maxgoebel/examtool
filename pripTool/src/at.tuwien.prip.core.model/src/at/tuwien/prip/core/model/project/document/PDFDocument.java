package at.tuwien.prip.core.model.project.document;

import java.awt.Rectangle;
import java.awt.geom.Rectangle2D;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.Transient;

import at.tuwien.prip.core.model.graph.ISegmentGraph;

import com.sun.pdfview.PDFFile;

/**
 * PDFDocument.java
 * 
 *
 *
 * @author mcg <mcgoebel@gmail.com>
 * May 20, 2012
 */
public class PDFDocument extends AbstractDocument
implements IPdfDocument
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 7892878062409340943L;
	
	private int pageNum = 1;
	
	private int numPages;
	
	@Transient
	private PDFFile pdfFile;

//	private Image thumb;

	private Map<Integer, ISegmentGraph> page2graphMap;
	/**
	 * Constructor.
	 */
	public PDFDocument() 
	{
		this.setFormat(DocumentFormat.PDF);
		this.page2graphMap = new HashMap<Integer, ISegmentGraph>();
	}
	
	@Override
	public Rectangle getBounds()
	{
		if (pdfFile==null) 
			return null;
		Rectangle2D pageBox = pdfFile.getPage(pageNum).getPageBox();
		return new Rectangle(
				(int)pageBox.getX(), 
				(int)pageBox.getY(), 
				(int)pageBox.getWidth(), 
				(int)pageBox.getHeight());
	}

	public int getPageNum() {
		return pageNum;
	}

	public void setPageNum(int pageNum) {
		this.pageNum = pageNum;
	}

	public int getNumPages() {
		return numPages;
	}

	public void setNumPages(int numPages) {
		this.numPages = numPages;
	}

	public PDFFile getPdfFile() {
		return pdfFile;
	}

	public void setPdfFile(PDFFile pdfFile) {
		this.pdfFile = pdfFile;
	}

//	public Image getThumb() {
//		return thumb;
//	}
//
//	public void setThumb(Image thumb) {
//		this.thumb = thumb;
//	}
	
	public ISegmentGraph getDocumentGraph(int pageNum)
	{
		return page2graphMap.get(pageNum);
	}
	
	public void setDocumentGraph(int pageNum, ISegmentGraph g)
	{
		this.page2graphMap.put(pageNum, g);
	}
	
}
