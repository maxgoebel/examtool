package at.tuwien.prip.core.model.graph;

import java.awt.Rectangle;

import at.tuwien.prip.core.model.agent.states.AgentEdgeState;
import at.tuwien.prip.core.model.agent.states.AgentState;
import at.tuwien.prip.core.model.graph.base.IGraph;

/**
 * 
 * IStateGraph.java
 * 
 *
 *
 * @author mcg <mcgoebel@gmail.com>
 * Oct 7, 2012
 */
public interface IStateGraph extends IGraph<AgentState, AgentEdgeState> 
{

}
