package at.tuwien.prip.core.model.agent;

import java.util.List;

import at.tuwien.prip.core.model.agent.states.AgentState;
import at.tuwien.prip.core.model.agent.states.IAgentState;

/**
 * IAgent.java
 *
 * Contract of all agents.
 *
 * @author mcg <mcgoebel@gmail.com>
 * @date Sep 19, 2011
 */
public interface IAgent extends Runnable
{
	public void start ();
	
	public void pause ();
	
	public void resume ();
	
	public void stop();

	public String getName();

	public List<IAgentState> getOpen();
	
	public boolean isActive();
	
	public boolean hasFinished();

	public void computeUtility(AgentState state);

	public void runOnce();
	
	public int getNumIterations();
	
	public int getStatesInput();
	
	public int getStatesCreated();
}
