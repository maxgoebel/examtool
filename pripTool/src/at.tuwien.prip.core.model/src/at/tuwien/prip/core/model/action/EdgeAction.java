package at.tuwien.prip.core.model.action;

import at.tuwien.prip.core.model.graph.base.BaseEdge;


/**
 * EdgeAction.java
 *
 *
 * 
 * @author: Max Goebel <mcgoebel@gmail.com>
 * @date: Mar 29, 2011
 */
public class EdgeAction implements Action {

	private BaseEdge<?> edge;
	
	public EdgeAction(BaseEdge<?> edge) {
		this.edge = edge;
	}
	
	@Override
	public String toString() {
		return getName();
	}
	
	@Override
	public String getName() {
		return edge.getRelation();
	}

	public BaseEdge<?> getEdge() {
		return edge;
	}

	public void setEdge(BaseEdge<?> edge) {
		this.edge = edge;
	}

}
