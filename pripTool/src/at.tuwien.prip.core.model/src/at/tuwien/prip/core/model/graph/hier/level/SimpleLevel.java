package at.tuwien.prip.core.model.graph.hier.level;

import at.tuwien.prip.core.model.graph.base.BaseEdge;
import at.tuwien.prip.core.model.graph.base.BaseNode;

public class SimpleLevel extends AbstractLevel<BaseNode, BaseEdge<BaseNode>> 
implements IGraphLevel<BaseNode, BaseEdge<BaseNode>> 
{
	int level = -1;

	@Override
	public int getLevel() 
	{
		return level;
	}
	
	public void setLevel(int level) 
	{
		this.level = level;
	}

}
