package at.tuwien.prip.core.model.utils;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.Properties;

import at.tuwien.prip.common.exceptions.PropertyLoadException;
import at.tuwien.prip.common.utils.BasePropertiesLoader;
import at.tuwien.prip.common.utils.ListUtils;

/**
 * 
 * CoreProperties.java
 *
 *
 *
 * Created: Jun 19, 2009 8:57:12 PM
 *
 * @author mcg <goebel@gmail.com>
 * @version 1.0
 */
@SuppressWarnings("unchecked")
public class CoreProperties {

	public static Properties coreProps;
	
	public static List<String> validHTML;
	public static List<String> invisibleHTML;
	
	public static List<String> documentHTML;
	public static List<String> textHTML;
	public static List<String> tableHTML;
	public static List<String> listHTML;
	public static List<String> formHTML;
	public static List<String> generalHTML;
	public static List<String> frameHTML;
	public static List<String> tableRootCandidateHTML;
	
	public static String corePros = "/home/max/dev/docwrap/development/docwrap.properties";
	
	
	static {
		
		coreProps = loadProperties2();
		
		String[] tmp = ((String) coreProps.get("textHTML")).split("\\ ");
		textHTML = ListUtils.toList(tmp);
		
		tmp = ((String) coreProps.get("tableHTML")).split("\\ ");
		tableHTML = ListUtils.toList(tmp);
		
		tmp = ((String) coreProps.get("listHTML")).split("\\ ");
		listHTML = ListUtils.toList(tmp);
		
		tmp = ((String) coreProps.get("formHTML")).split("\\ ");
		formHTML = ListUtils.toList(tmp);
		
		tmp = ((String) coreProps.get("generalHTML")).split("\\ ");
		generalHTML = ListUtils.toList(tmp);
		
		tmp = ((String) coreProps.get("frameHTML")).split("\\ ");
		frameHTML = ListUtils.toList(tmp);
		
		tmp = ((String) coreProps.get("documentHTML")).split("\\ ");
		documentHTML = ListUtils.toList(tmp);
	
		tmp = ((String) coreProps.get("tableCandidateHTML")).split("\\ ");
		tableRootCandidateHTML = ListUtils.toList(tmp);
		
		tmp = ((String) coreProps.get("invalidHTML")).split("\\ ");
		invisibleHTML = ListUtils.toList(tmp);
		
		validHTML = ListUtils.union(textHTML, tableHTML, listHTML, formHTML, generalHTML, frameHTML, documentHTML);
		
	}

	/**
	 * 
	 * @return
	 */
	public static Properties loadProperties2 ()
	{
		Properties result = null;
		try 
		{
			result = BasePropertiesLoader.loadAllProperties();
		}
		catch (PropertyLoadException e2) 
		{
			e2.printStackTrace();
		}
		return result;
	}
	
	/**
	 *
	 * Load the docWrap project properties.
	 *
	 * try reading from mysql.properties file
	 * the file should be located within the root of the plugins
	 *
	 * @return
	 */
	public static Properties loadProperties ()  {
		
		Properties properties = null;
		FileInputStream fstream = null;
		try {
			fstream = new FileInputStream(corePros);
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		}
		

		if (fstream != null)
		{
			properties = new Properties ();
			try {
				properties.load (fstream);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return properties;
	}
	
}//CoreProperties
