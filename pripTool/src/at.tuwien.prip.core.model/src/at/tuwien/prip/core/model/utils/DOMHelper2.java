package at.tuwien.prip.core.model.utils;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import at.tuwien.prip.common.utils.ListUtils;
import at.tuwien.prip.common.utils.StringUtils;
import at.tuwien.prip.core.model.utils.DOMHelper.Tree.Parent;
import at.tuwien.prip.core.model.utils.DOMHelper.Tree.Sibling;



/**
 * 
 * DOMHelper2.java
 *
 *
 * @author mcg <mcgoebel@gmail.com>
 * @date May 22, 2011
 */
public class DOMHelper2 {


	/**
	 * 
	 * Print a dom tree to error out console.
	 * 
	 * @param root
	 * @param indent
	 */
	public static void printDOMtags(Element root, String indent) {

		if (root!=null) {
			StringBuffer sb = new StringBuffer();
			sb.append(indent+root.getNodeName());
			System.err.println(sb.toString());

			indent = indent+"    ";
			List<Element> childList = DOMHelper.Tree.Children.getChildElements(root);
			for (Element child : childList) {
				printDOMtags(child, indent);
			}
		}
	}

	/**
	 * 
	 * Descend to the deepest single-child branches of the subtree
	 * rooted at the given element.
	 * 
	 * @param n
	 * @return
	 */
	public static Element descendAlongAllSingleBranches (Element e) {
		List<Element> children = DOMHelper.Tree.Children.getChildElements(e);
		while (children.size()==1) {
			e = children.get(0);
			children = DOMHelper.Tree.Children.getChildElements(e);
		}
		return e;
	}

	/**
	 * 
	 * Move up the DOM tree until a tree tag is observed.
	 * 
	 * Three rules:
	 * 1) no single childs
	 * 2) move up as long as valid (non-empty) text children are available
	 * 2) if parent is formatting, move up (as in 1)
	 * 
	 * @param n
	 * @return
	 */
	public static Node getClosestSensibleTableAncestor (Node n) {

		if (containsNodeTextAndFormatElementsOnly(n)) {
			n = DOMHelper.Tree.Parent.getParentElement(n);
		}

		n = getHighestStillTextValueAncestor(n);
		n = getClosestNonSiblingAncestor(n);
		return n;
	}

	/**
	 * Check if the table is a leaf table.
	 * @param e
	 * @return true if it is a leaf table
	 */
	public static boolean isLeafTable (Element e) {
		if (!e.getNodeName().toUpperCase().equals("TABLE")) return false;

		List<Element> descendants = DOMHelper.Tree.Descendant.getDescendantsNotSelfElements(e);
		for (Element desc : descendants) {
			if (desc.getNodeName().toUpperCase().equals("TABLE"))
				return false;
		}
		return true;
	}

	/**
	 *
	 * @param root
	 * @return
	 */
	public static String getDelimiterFromString (Element root) {
		StringBuffer sBuffer = new StringBuffer();
		String previous = "";
		Map<Integer, String> scoreMap = new HashMap<Integer, String>();
		int score = 0;

		List<Element> children = DOMHelper.Tree.Children.getChildElements(root);
		if (children.size()==0) return null;

		for (Element child : children) {
			String tag = child.getNodeName().toUpperCase();
			sBuffer.append("/"+tag);
			if (tag.equals("BR")) {
				previous+="/"+tag;
				score ++;
			}
			else if (tag.equals("P")) {
				previous+="/"+tag;
				score += 2; // counts more
			}
			else {
				if (!previous.equals(""))
					scoreMap.put(score, previous);
				previous = "";
				score=0;
			}
		}
		// sort by score
		Object[] scores = null;
		if (scoreMap.keySet().size()>0) {
			scores = scoreMap.keySet().toArray();
			java.util.Arrays.sort(scores);
		}

		if (scores==null) return null;

		//TODO: FIXME
		//		for (int i=scores.length-1; i>=0; i--) {
		//		String delimiter = scoreMap.get(scores[i]);
		//		String[] split = sBuffer.toString().split(delimiter);

		//		}
		//		// Check how evenly each delimiter splits the string
		//		Map2<String,String,List<List<Element>>> tables =
		//		new HashMap2<String,String,List<List<Element>>>(null,null,null);

		Map<Integer,String> ldMap = new HashMap<Integer,String>();
		Iterator<Integer> it = scoreMap.keySet().iterator();
		while (it.hasNext()) { // for each delimiter
			int ldScore = 0;
			String del = scoreMap.get(it.next());
			String[] split = sBuffer.toString().split(del);

			for (int i=1; i<split.length; i++) {
				//				if (tables.get(del, split[i])==null) {
				//				tables.put(del, split[i], new LinkedList<String>());
				//				} else {
				//				tables.get(del, split[i]).add(split[i]);
				//				}

				ldScore += StringUtils.Levenshtein.LD(split[i-1], split[i]);
			}
			ldMap.put(ldScore/split.length, del);
		}
		// sort in ASCENDING order
		if (ldMap.keySet().size()>0) {
			Object[] ldScores = ldMap.keySet().toArray();
			java.util.Arrays.sort(ldScores);

			if (((Integer)ldScores[0])<2)
				return ldMap.get(ldScores[ldScores.length-1]);
		}
		return null;
	}

	/**
	 * 
	 * Is this element an image? Heuristic...
	 * 
	 * @param e
	 * @return
	 */
	public static boolean isImage (Element e) {
		if (e.getTagName().equalsIgnoreCase("IMG")) return true;

		while (isTableRootCandidate(e) || isLink(e)) {
			List<Element> children = DOMHelper.Tree.Children.getProperTagChildElements(e);
			if (children.size()!=1) return false;
			e = children.get(0);
			if (e.getTagName().equalsIgnoreCase("IMG")) return true;
		}
		return false;
	}

	/**
	 * 
	 * Is this element a link element? Heuristic...
	 * 
	 * @param e
	 * @return
	 */
	public static boolean isLink (Element e) {
		if (e.getTagName().equals("A")) return true;

		while (isFormatting(e)) {
			if (e.getTagName().equals("A")) return true;

			List<Element> children = DOMHelper.Tree.Children.getProperTagChildElements(e);
			if (children.size()!=1) return false;
			e = children.get(0);
		}
		return false;
	}

	/**
	 *
	 * Is this element a formatting element?
	 *
	 * @param e
	 * @return
	 */
	public static boolean isFormatting (Element e) {
		return CoreProperties.textHTML.contains(e.getTagName());
	}

	/**
	 *
	 * Is this element a candidate for a table root?
	 *
	 * @param e
	 * @return
	 */
	public static boolean isTableRootCandidate (Element e) {
		return CoreProperties.tableRootCandidateHTML.contains(e.getTagName());
	}

	/**
	 *
	 * Is this element a table element (e.g. TR, etc)?
	 *
	 * @param e
	 * @return
	 */
	public static boolean isTableElement (Element e) {
		return CoreProperties.tableHTML.contains(e.getTagName());
	}

	/**
	 *
	 * Return all nodes in this element's subtree that have
	 * a valid text content.
	 * 
	 * @param e
	 * @return
	 */
	public static List<Node> getTextSubNodes (Element e) {
		List<Node> result = new LinkedList<Node> ();
		List<Element> leafs = DOMHelper.Tree.Descendant.getLeafDescendants(e);
		for (Element leaf : leafs) {
			if (isFormatting(leaf) && leaf.getTextContent().length()>0) {
				result.add(leaf);
			} else if (leaf.getChildNodes()!=null) {
				Node n = leaf.getChildNodes().item(0);
				if (n!=null && n.getTextContent()!=null && n.getTextContent().length()>0)
					result.add(n);
			}
		}
		//also add leaf text nodes directly under e
		List<Node> tmp = (DOMHelper.Tree.Descendant.getLeafTextNodes(e));
		for (Node n : tmp) {
			if (!result.contains(n))
				result.add(n);
		}
		return result;
	}

	/**
	 *
	 * Are all elements in the input list siblings?
	 *
	 * @param elements
	 * @return
	 */
	public static boolean areSiblings ( List<Element> elements ) {
		if (elements.size()==0) return false;
		Element parent = DOMHelper.Tree.Parent.getParentElement(elements.get(0));
		if (parent==null) return false;
		for (Element e : elements) {
			while (DOMHelper.Tree.Sibling.getSiblingCount(e)==0) {
				e = DOMHelper.Tree.Parent.getParentElement(e);
			}
			Element other_parent = DOMHelper.Tree.Parent.getParentElement(e);
			if (!parent.equals(other_parent))
				return false;
		}
		return true;
	}

	/**
	 *
	 * Get the highest ancestor of e that is still a single child.
	 *
	 * @param n
	 * @return
	 */
	public static Node getClosestNonSiblingAncestor (Node n) {
		while (Parent.getParentElement(n)!=null &&
				Sibling.getSiblingNodeCount(n)==0)
			n = Parent.getParentElement(n);
		return n;
	}

	/**
	 *
	 * Get the highest ancestor of e still containing text content.
	 *
	 * @param n
	 * @return
	 */
	public static Node getHighestStillTextValueAncestor (Node n) {
		boolean containsTextNode = true;
		while (Parent.getParentElement(n)!=null && containsTextNode) {
			containsTextNode = false;
			Element parent = Parent.getParentElement(n);
			containsTextNode = containsNodeText(parent);
			if (containsTextNode)
				n = parent;
		}
		return n;
	}

	/**
	 *
	 * Check if this node contains a node text.
	 *
	 * @param e
	 * @return
	 */
	protected static boolean containsNodeText (Element e) {
		NodeList nl = e.getChildNodes();
		for (int i=0; i<nl.getLength(); i++) {
			Node n = nl.item(i);
			String content = n.getTextContent().trim().replaceAll("[\\r\\f\\n]","");
			if (n.getNodeType()==Node.TEXT_NODE && content.length()>0) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Visible here means that element is either image or textnode..
	 * @param e
	 * @return
	 */
	public static boolean isVisible(Element e) {
		boolean result = false;
		if (containsNodeText(e)) {
			result = true;
		} else if (isImage(e)) {
			result = true;
		}
		if (isTableElement(e)) {
			result = false;
		}
		return result;
	}

	/**
	 *
	 * Check if the subtree of the given element contains
	 * nodes with node text content or if the given element
	 * belongs to the format tag group.
	 *
	 * @param e
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static boolean containsNodeTextAndFormatElementsOnly (Node e) {

		if (e.getNodeType()==Node.TEXT_NODE) {
			String content = e.getTextContent().trim().replaceAll("[\\r\\f\\n]","");
			if (content.length()>0) {
				return true;
			}
		} else if (e.getNodeType()==Node.ELEMENT_NODE) {
			List<String> txtTags = 
				ListUtils.union(CoreProperties.textHTML,CoreProperties.listHTML,CoreProperties.generalHTML);
			if (!txtTags.contains(((Element)e).getTagName().toUpperCase())) {
				return false;
			}
		} 
		return e.getTextContent().trim().replaceAll("[\\r\\f\\n]","").length()>0;

	}

	/**
	 *
	 * Check if the given element lies in a subtree that is rooted
	 * under a 'bad' tag (e.g. <script>, etc).
	 *
	 * @param e
	 * @param tagName
	 * @return
	 */
	public static boolean elementInSubtreeWithBadTag (Node n) {
		while (n!=null) {
			if (n.getNodeType()==Node.ELEMENT_NODE) {
				if (CoreProperties.invisibleHTML.contains(((Element)n).getTagName())) {
					return true;
				}

				n = DOMHelper.Tree.Parent.getParentElement(n);
			}
		}
		return false;
	}

}//DOMHelper2


