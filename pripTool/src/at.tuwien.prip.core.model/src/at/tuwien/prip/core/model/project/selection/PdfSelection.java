package at.tuwien.prip.core.model.project.selection;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.OneToMany;


/**
 * PdfSelection.java
 * 
 *
 *
 * @author mcg <mcgoebel@gmail.com>
 * Nov 12, 2012
 */
@Entity
public class PdfSelection extends AbstractSelection 
{
	@OneToMany
	protected List<PDFInstruction> instructions;
	
	protected String text;
	
	/**
	 * Constructor.
	 */
	public PdfSelection() {
		this.instructions = new ArrayList<PDFInstruction>();
	}
	
	public List<PDFInstruction> getInstructions() {
		return instructions;
	}
	
	public void setInstructions(List<PDFInstruction> instructions) {
		this.instructions = instructions;
	}
	
	public String getText() {
		return text;
	}
	
	public void setText(String text) {
		this.text = text;
	}
}
