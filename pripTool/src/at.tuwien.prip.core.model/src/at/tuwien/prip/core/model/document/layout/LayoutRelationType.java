package at.tuwien.prip.core.model.document.layout;

/**
 * LayoutRelationType.java
 * 
 *
 *
 * @author mcg <mcgoebel@gmail.com>
 * Aug 25, 2012
 */
public enum LayoutRelationType {

	Relation, Alignment, Neighbourhood, Indentation
	
}
