package at.tuwien.prip.core.model.graph;

import java.util.List;

/**
 * PixelMatrix.java
 * 
 *
 *
 * @author mcg <mcgoebel@gmail.com>
 * Sep 5, 2012
 */
public class PixelMatrix extends AdjacencyMatrix<Pixel>
{

	public PixelMatrix(List<Pixel> nodes) {
		super(nodes);
	}
	

}
