package at.tuwien.prip.core.model.document.layout;

import java.util.ArrayList;
import java.util.List;

import at.tuwien.prip.core.model.graph.DocNode;


/**
 * LayoutList.java
 *
 *
 * @author mcg <mcgoebel@gmail.com>
 * @date Aug 31, 2011
 */
public class LayoutList extends LayoutObject
{

	private LayoutObject header;
	
	private List<LayoutObject> items;
	
	/**
	 * Constructor.
	 */
	public LayoutList() {
		items = new ArrayList<LayoutObject>();
	}
	
	public double getAverageLineSpacing () {
		double prev = -1;
		double sum = 0;
		int i=0;
		for (DocNode o : elements)
		{
			double curr = o.getBoundingBox().getCenterY();
			if (prev<0) {
				prev = curr;
			} else {
				sum += curr - prev;
				i++;
			}
		}
		if (i>0) {
			return sum/i;
		}
		else return 0;
	}

	public void setItems(List<LayoutObject> items) {
		this.items = items;
	}

	public List<LayoutObject> getItems() {
		return items;
	}

	public void setHeader(LayoutObject header) {
		this.header = header;
	}

	public LayoutObject getHeader() {
		return header;
	}
}
