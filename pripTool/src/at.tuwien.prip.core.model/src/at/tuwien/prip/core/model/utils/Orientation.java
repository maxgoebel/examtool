package at.tuwien.prip.core.model.utils;

/**
 * Orientation.java
 *
 *
 * 
 * @author: Max Goebel <mcgoebel@gmail.com>
 * @date: Feb 20, 2011
 */
public enum Orientation {
	
	HORIZONTAL, VERTICAL, HIERARCHICAL, DIAGONAL, BOTH
	
}
