package at.tuwien.prip.core.model.project.result;

import at.tuwien.prip.core.model.project.selection.AbstractSelection;

/**
 * 
 * SelectionResult.java
 * 
 *
 *
 * @author mcg <mcgoebel@gmail.com>
 * Aug 27, 2012
 */
public class SelectionResult implements IResult
{

	private AbstractSelection selection;
	
	/**
	 * Constructor.
	 * @param selection
	 */
	public SelectionResult(AbstractSelection selection)
	{
		this.selection = selection;
	}
	
	public AbstractSelection getSelection() {
		return selection;
	}
	
}
