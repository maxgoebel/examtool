package at.tuwien.prip.core.model.agent.attributes;

import at.tuwien.prip.core.model.agent.IAgent;

public class AgentStateAttribute {

	private AttributeType key;
	
	private String value;
	
	private IAgent owner;
	
	/**
	 * Constructor.
	 * @param key
	 * @param value
	 */
	public AgentStateAttribute(AttributeType key, String value, IAgent owner) 
	{
		this.key = key;
		this.value = value;
		this.owner = owner;
	}
	
	public IAgent getOwner() {
		return owner;
	}
	
	public AttributeType getKey() {
		return key;
	}
	
	public String getValue() {
		return value;
	}
}
