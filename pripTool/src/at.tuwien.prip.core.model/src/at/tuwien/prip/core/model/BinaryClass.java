package at.tuwien.prip.core.model;


/**
 * BinaryClass.java
 *
 *
 * 
 * @author: Max Goebel <mcgoebel@gmail.com>
 * @date: Feb 17, 2011
 */
public enum BinaryClass {
	
	POSITIVE(0), 
	NEGATIVE(1);
	
	private int code;
	private BinaryClass(int code) {

		this.code = code;
	}
	public int getCode() {
		return code;
	}
	public void setCode(int code) {
		this.code = code;
	}
	public static BinaryClass valueOf(int i){
		for (BinaryClass s : values()){
			if (s.code == i){
				return s;
			}
		}
		throw new IllegalArgumentException("No matching constant for " + i);
	}
}
