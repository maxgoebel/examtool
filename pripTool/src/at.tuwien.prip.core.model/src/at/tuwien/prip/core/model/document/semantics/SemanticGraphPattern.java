package at.tuwien.prip.core.model.document.semantics;

import java.awt.Rectangle;

import at.tuwien.prip.core.model.document.segments.GenericSegment;
import at.tuwien.prip.core.model.document.segments.SemanticSegment;
import at.tuwien.prip.core.model.graph.base.IGraph;

/**
 * GraphPattern.java
 * 
 *
 *
 * @author mcg <mcgoebel@gmail.com>
 * Dec 15, 2011
 */
public class SemanticGraphPattern
{
	private IGraph<?, ?> graph;
	
	private WordSemantics patternName;
	
	private GenericSegment segment;
	
	private String textContent;
	
	/**
	 * Constructor.
	 * 
	 * @param graph
	 * @param txt
	 * @param name
	 */
	public SemanticGraphPattern(IGraph<?, ?> graph, String txt, WordSemantics name) 
	{
		this.graph = graph;
		this.patternName = name;
		this.textContent = txt;
		
		/* create semantic segment */	
		Rectangle bounds = graph.getDimensions();
		segment = new SemanticSegment(
				bounds.x,
				bounds.x + bounds.width, 
				bounds.y, 
				bounds.y + bounds.height);
		SemanticText st = new SemanticText(txt, name);
		((SemanticSegment) segment).addSemanticAnnotation(st);
	}
	
	public String getTextContent() {
		return textContent;
	}
	
	public IGraph<?,?> getGraph() {
		return graph;
	}
	
	public WordSemantics getPatternName() {
		return patternName;
	}
	
	public GenericSegment getSegment() {
		return segment;
	}
	
}
