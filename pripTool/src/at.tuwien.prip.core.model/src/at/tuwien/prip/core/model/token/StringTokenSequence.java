package at.tuwien.prip.core.model.token;

/**
 * 
 * @author max
 *
 */
public class StringTokenSequence extends TokenSequence<StringToken> {

	protected final String SEPARATOR = " ";

	public StringTokenSequence() {
		super();
	}
	
	/**
	 * Constructor.
	 * @param input
	 */
	public StringTokenSequence(String input) {
		String[] words = input.split("\\s");

		for (int i=0; i<words.length; i++) {
			String word = words[i];
//			word = word.replaceAll("\\W", ""); //remove non word characters
			
			if (word.length()>1) {
				StringToken wordToken = new StringToken(word);
				add(wordToken);
			}
				
		}
	}
	
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		for (StringToken token : tokenList) {
			sb.append(token.name+SEPARATOR);
		}
		return sb.toString();
	}
}
