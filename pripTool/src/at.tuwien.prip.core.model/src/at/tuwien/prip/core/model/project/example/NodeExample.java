package at.tuwien.prip.core.model.project.example;

import org.w3c.dom.Node;

import at.tuwien.prip.core.model.BinaryClass;
import at.tuwien.prip.core.model.project.selection.NodeSelection;

public class NodeExample extends AbstractBinaryExample
{

	NodeSelection nodeSelection;
	
	/**
	 * Constructor.
	 * @param node
	 */
	public NodeExample(NodeSelection selection, BinaryClass classification)
	{
//		this.selectedNode = node;
//		this.targetXpath = DOMHelper.XPath.getExactXPath(node);
		this.nodeSelection = selection;
		this.classification = classification;
	}
	
	public NodeSelection getNodeSelection() {
		return nodeSelection;
	}
	
	public Node getSelectedNode() {
		return nodeSelection.getSelectedNode();
	}
}
