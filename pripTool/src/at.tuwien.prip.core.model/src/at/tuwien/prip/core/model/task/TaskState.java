package at.tuwien.prip.core.model.task;

import java.util.Map;

import at.tuwien.prip.core.model.task.actions.TaskAction;

/**
 * TaskState.java
 *
 *
 * 
 * @author: Max Goebel <mcgoebel@gmail.com>
 * @date: Feb 17, 2011
 */
public class TaskState {

	private String name;
	
	private String description;
	
	private boolean isFinished;
	
	private Map<TaskAction, TaskState> transitionMatrix;
	
	private TaskState previous;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public boolean isFinished() {
		return isFinished;
	}

	public void setFinished(boolean isFinished) {
		this.isFinished = isFinished;
	}

	public Map<TaskAction, TaskState> getTransitionMatrix() {
		return transitionMatrix;
	}

	public void setTransitionMatrix(Map<TaskAction, TaskState> transitionMatrix) {
		this.transitionMatrix = transitionMatrix;
	}

	public TaskState getPrevious() {
		return previous;
	}

	public void setPrevious(TaskState previous) {
		this.previous = previous;
	}
		
}
