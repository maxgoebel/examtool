package at.tuwien.prip.core.model.project.annotation;

import java.util.ArrayList;
import java.util.List;

import at.tuwien.prip.core.model.project.selection.AbstractSelection;
import at.tuwien.prip.core.model.project.selection.ExtractionResult;

public class ExtractionAnnotation extends Annotation 
{
	/**
	 * Constructor.
	 * @param uri
	 */
	public ExtractionAnnotation(String uri) 
	{
		super(uri, AnnotationType.EXTRACTION);
	}
	
	/**
	 * 
	 * @return
	 */
	public List<ExtractionResult> getExtractionItems()
	{
		List<ExtractionResult> extractions = new ArrayList<ExtractionResult>();
		for (AbstractSelection item : items)
		{
			if (item instanceof ExtractionResult)
			{
				extractions.add((ExtractionResult) item);
			}
		}
		return extractions;
	}
	
}
