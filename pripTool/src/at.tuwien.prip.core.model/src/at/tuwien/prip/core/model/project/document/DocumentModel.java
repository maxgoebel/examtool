package at.tuwien.prip.core.model.project.document;

import java.util.ArrayList;
import java.util.List;

import at.tuwien.prip.core.model.project.example.SelectionExample;


/**
 * 
 * DocumentModel.java
 *
 *
 * created: Sep 13, 2009
 * @author Max Goebel
 */
public class DocumentModel 
{
	/* the document entry */
	private IDocument documentItem;
	
	private List<SelectionExample> examples;
	
	/**
	 * Constructor.
	 */
	public DocumentModel()
	{
		this.examples = new ArrayList<SelectionExample>();
	}
		
	public IDocument getDocumentItem() {
		return documentItem;
	}
	
	public void setDocumentItem(IDocument documentItem) {
		this.documentItem = documentItem;
	}
	
	public List<SelectionExample> getExamples() {
		return examples;
	}
	
	public void setExamples(List<SelectionExample> examples) {
		this.examples = examples;
	}
	
}
