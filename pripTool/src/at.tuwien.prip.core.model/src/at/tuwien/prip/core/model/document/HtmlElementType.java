package at.tuwien.prip.core.model.document;

/**
 * HtmlElementType.java
 *
 *
 * 
 * @author: Max Goebel <mcgoebel@gmail.com>
 * @date: Feb 20, 2011
 */
public enum HtmlElementType {

	LINK, FORM, IMAGE, TEXT
	
}
