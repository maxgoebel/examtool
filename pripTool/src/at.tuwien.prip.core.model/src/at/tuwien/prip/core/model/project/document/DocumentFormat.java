package at.tuwien.prip.core.model.project.document;

/**
 * DocumentFormat.java
 *
 *
 * 
 * @author: Max Goebel <mcgoebel@gmail.com>
 * @date: Mar 2, 2011
 */
public enum DocumentFormat {

	PDF, HTML, TIFF, UNKNOWN
	
}
