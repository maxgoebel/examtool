package at.tuwien.prip.core.model.project.selection;

import java.awt.Rectangle;
import java.util.List;

/**
 * SinglePageSelection.java
 * 
 *
 *
 * @author mcg <mcgoebel@gmail.com>
 * Nov 19, 2012
 */
public class SinglePageSelection extends AbstractSelection
{
	protected Rectangle bounds;
	
	protected List<AbstractSelection> items;
	
	public Rectangle getBounds() {
		return bounds;
	}
	
	public void setBounds(Rectangle bounds) {
		this.bounds = bounds;
	}
	
	public List<AbstractSelection> getItems() {
		return items;
	}
	
	public void setItems(List<AbstractSelection> items) {
		this.items = items;
	}
}
