package at.tuwien.prip.core.model.project.selection;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.OneToMany;

import at.tuwien.prip.core.model.document.segments.GenericSegment;

/**
 * SegmentSelection.java
 * 
 *
 *
 * @author mcg <mcgoebel@gmail.com>
 * Nov 12, 2012
 */
@Entity
public class SegmentSelection extends AbstractSelection 
{
	
	@OneToMany
	private List<GenericSegment> segments;
	
	/**
	 * Constructor.
	 */
	public SegmentSelection() 
	{
		this.segments = new ArrayList<GenericSegment>();
	}
	
	public List<GenericSegment> getSegments() {
		return segments;
	}
}
