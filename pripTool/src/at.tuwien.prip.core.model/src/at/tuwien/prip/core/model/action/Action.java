package at.tuwien.prip.core.model.action;

/**
 * Action.java
 *
 * A simple action
 * 
 * @author: Max Goebel <mcgoebel@gmail.com>
 * @date: Mar 29, 2011
 */
public interface Action {

	public String getName();

	
}
