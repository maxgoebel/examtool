package at.tuwien.prip.core.model.graph;

/**
 * DocumentConstants.java
 *
 * A collection of constants related to the document
 * model.
 * 
 * @author: Max Goebel <mcgoebel@gmail.com>
 * @date: Mar 31, 2011
 */
public class DocumentConstants {

	/**
	 * The hierarchy levels
	 */
	public static final int TEXTBLOCK_LEVEL = 3;
	public static final int TEXTLINE_LEVEL = 2;
	public static final int WORD_LEVEL = 1;
	public static final int CHAR_LEVEL = 0;
	
}
