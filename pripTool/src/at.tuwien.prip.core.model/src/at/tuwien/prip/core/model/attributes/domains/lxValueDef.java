package at.tuwien.prip.core.model.attributes.domains;


/**
 * lxValueDef.java
 *
 *
 * Created: Tue Aug 12 14:48:17 2003
 *
 * @author Michal Ceresna
 * @version 1.0
 */
public interface lxValueDef 
{
  public abstract boolean matches(String v);

  public String toString();

  public String getAttName();
  
} // lxValueDef
