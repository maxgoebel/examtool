package at.tuwien.prip.core.model.project.annotation;

public enum AnnotationType {

	EXTRACTION, TABLE, LIST, LABEL, EXAMPLE, FRAGMENT
}
