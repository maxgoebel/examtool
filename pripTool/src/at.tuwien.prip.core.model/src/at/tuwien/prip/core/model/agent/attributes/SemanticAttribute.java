package at.tuwien.prip.core.model.agent.attributes;

import java.util.List;

import at.tuwien.prip.core.model.agent.IAgent;
import at.tuwien.prip.core.model.document.semantics.SemanticText;

/**
 * 
 * SemanticAttribute.java
 * 
 *
 *
 * @author mcg <mcgoebel@gmail.com>
 * Oct 8, 2012
 */
public class SemanticAttribute extends AgentStateAttribute 
{

	private List<SemanticText> semantics;

	/**
	 * Constructor.
	 * @param value
	 * @param owner
	 */
	public SemanticAttribute(String text, List<SemanticText> semantics,  IAgent owner)
	{
		super(AttributeType.SEMANTIC, text, owner);
		this.semantics = semantics;
	}
	
	public List<SemanticText> getSemantics() {
		return semantics;
	}
	
	public void setSemantics(List<SemanticText> semantics) {
		this.semantics = semantics;
	}

}
