package at.tuwien.prip.core.model.project.selection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.persistence.PostLoad;
import javax.persistence.PrePersist;
import javax.persistence.Transient;

import at.tuwien.prip.core.model.project.annotation.AnnotationLabel;

/**
 * LabelSelection.java
 * 
 *
 *
 * @author mcg <mcgoebel@gmail.com>
 * Nov 12, 2012
 */
@Entity
public class LabelSelection extends SinglePageSelection 
{
	@Transient
	private AnnotationLabel label; //actual enum; not stored in db
	
	@Column(name="LABEL")  
	private int labelCode; // enum code gets stored in db

	@PrePersist
	void populateDBFields(){
		labelCode = label.getCode();
	}

	@PostLoad
	void populateTransientFields(){
		label = AnnotationLabel.valueOf(labelCode);
	}


	private String subLabel;

	@OneToOne
	private AbstractSelection selection;

	public AnnotationLabel getLabel() {
		return label;
	}

	public String getSubLabel() {
		return subLabel;
	}

	public void setLabel(AnnotationLabel label) {
		this.label = label;
	}

	public void setSubLabel(String subLabel) {
		this.subLabel = subLabel;
	}

	public AbstractSelection getSelection() {
		return selection;
	}

	public void setSelection(AbstractSelection selection) {
		this.selection = selection;
	}
}
