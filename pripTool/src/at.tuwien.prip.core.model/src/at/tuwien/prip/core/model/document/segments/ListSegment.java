package at.tuwien.prip.core.model.document.segments;


/**
 * ListSegment.java
 *
 *
 * @author mcg <mcgoebel@gmail.com>
 * @date Jun 8, 2011
 */
public class ListSegment extends GenericSegment 
{
	/**
	 * Constructor.
	 * 
	 * @param x1
	 * @param x2
	 * @param y1
	 * @param y2
	 */
	public ListSegment(      
			float x1,
			float x2,
			float y1,
			float y2
	)
	{
		super(x1, x2, y1, y2);
	}

}
