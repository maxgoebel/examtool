package at.tuwien.prip.core.model.graph.hier;

import at.tuwien.prip.core.model.graph.DocEdge;
import at.tuwien.prip.core.model.graph.DocNode;

/**
 * DocEdgeHier.java
 * 
 * A hierarchical document edge.
 * 
 * @author: Max Goebel <mcgoebel@gmail.com>
 * @date: May 10, 2011
 * 
 */
public abstract class DocEdgeHier extends DocEdge {

	boolean isActive = false;
	
	public DocEdgeHier(DocNode nodeFrom, DocNode nodeTo) {
		super(nodeFrom, nodeTo);
	}
	
	public void activate () {
		isActive = true;
	}
	
	public void deactivate () {
		isActive = false;
	}

}
