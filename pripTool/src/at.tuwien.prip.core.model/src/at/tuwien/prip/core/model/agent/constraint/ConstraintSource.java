package at.tuwien.prip.core.model.agent.constraint;

/**
 * ConstraintSource.java
 *
 *
 * @author mcg <mcgoebel@gmail.com>
 * @date Sep 12, 2011
 */
public enum ConstraintSource 
{

	UNDEFINED, 
	LAYOUT_TD, 
	LAYOUT_BU, 
	LAYOUT_LIST,
	SEMANTIC
	
}
