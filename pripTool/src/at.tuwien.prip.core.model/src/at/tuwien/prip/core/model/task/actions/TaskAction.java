package at.tuwien.prip.core.model.task.actions;

/**
 * TaskAction.java
 *
 *
 * 
 * @author: Max Goebel <mcgoebel@gmail.com>
 * @date: Feb 17, 2011
 */
public abstract class TaskAction {

	protected String name;
	
	protected String description;
	
	public abstract boolean execute ();
	
}
