package at.tuwien.prip.core.model.graph.hier.level;

import at.tuwien.prip.core.model.graph.DocEdge;
import at.tuwien.prip.core.model.graph.DocNode;
import at.tuwien.prip.core.model.graph.DocumentConstants;

/**
 * WordLevel.java
 *
 *
 * @author mcg <mcgoebel@gmail.com>
 * @date Aug 19, 2011
 */
public class WordLevel extends AbstractLevel<DocNode,DocEdge>
implements IGraphLevel<DocNode,DocEdge>
{

	/**
	 * Constructor.
	 * 
	 * @param stack
	 */
	public WordLevel(SegLevelGraph stack) {
		this.parent = stack;
	}
	
	@Override
	public int getLevel() {
		return DocumentConstants.WORD_LEVEL;
	}

}
