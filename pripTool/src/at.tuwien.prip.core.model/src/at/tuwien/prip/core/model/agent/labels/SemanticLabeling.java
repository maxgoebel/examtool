package at.tuwien.prip.core.model.agent.labels;

import java.util.List;

import at.tuwien.prip.core.model.agent.AC;
import at.tuwien.prip.core.model.agent.constraint.EdgeConstraint;
import at.tuwien.prip.core.model.agent.states.AgentState;
import at.tuwien.prip.core.model.document.segments.SegmentType;
import at.tuwien.prip.core.model.document.semantics.WordSemantics;

public class SemanticLabeling extends LayoutLabel
{

	private WordSemantics semantics;
	
	private String textContent;
	
	/**
	 * Constructor.
	 * @param label
	 * @param stateLevel
	 * @param subLabel
	 * @param confidence
	 * @param complexity
	 * @param affectedStates
	 * @param utilizedStates
	 * @param constraints
	 */
	public SemanticLabeling(
			WordSemantics semantics,
			String textContent,
			double confidence, double complexity, AgentState[] affectedStates,
			AgentState[] utilizedStates, List<EdgeConstraint> constraints)
	{
		super(SegmentType.Semantic, 
				LabelType.LOGICAL,
				AC.D.SEMANTIC, "", 
				confidence, 
				complexity, 
				affectedStates,
				utilizedStates, 
				constraints);

		this.semantics = semantics;
		this.textContent = textContent;
	}
	
	public WordSemantics getSemantics() {
		return semantics;
	}
	
	public String getTextContent() {
		return textContent;
	}

}
