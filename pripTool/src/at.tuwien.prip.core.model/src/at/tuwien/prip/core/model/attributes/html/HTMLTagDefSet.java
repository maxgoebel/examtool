package at.tuwien.prip.core.model.attributes.html;

/**
 * HTMLTagDefSet.java
 *
 * immutable (constant) set of tag definitions
 *
 * Created: Sun Jul 27 17:56:05 2003
 *
 * @author Michal Ceresna
 * @version 1.0
 */
public interface HTMLTagDefSet {

  public boolean contains(String tag_name);

} // HTMLTagDefSet
