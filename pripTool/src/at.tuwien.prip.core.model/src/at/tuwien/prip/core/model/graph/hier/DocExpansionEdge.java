package at.tuwien.prip.core.model.graph.hier;

import at.tuwien.prip.core.model.graph.DocNode;

/**
 * DocExpansionEdge.java
 * 
 * @author Max Goebel <mcgoebel@gmail.com>
 * @date
 */
public class DocExpansionEdge extends DocEdgeHier {

	/**
	 * Constructor.
	 * 
	 * @param nodeFrom
	 * @param nodeTo
	 */
	public DocExpansionEdge(DocNode nodeFrom, DocNode nodeTo) {
		super(nodeFrom, nodeTo);
	}

}
