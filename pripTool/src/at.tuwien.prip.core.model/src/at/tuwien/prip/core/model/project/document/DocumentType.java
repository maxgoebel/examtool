package at.tuwien.prip.core.model.project.document;

public enum DocumentType {

	PDF, HTML, TIFF
}
