package at.tuwien.prip.core.model.project.annotation;

import java.util.List;

import at.tuwien.prip.core.model.project.selection.PDFInstruction;
import at.tuwien.prip.core.model.project.selection.TableCell;

public class Region {

	List<PDFInstruction> instructions;
	
	List<TableCell> cells;
	
	int pageNum;
	
	int id;
	
	int colIncrement;
	
	int rowIncrement;
	
	public Region(int pageNum, int id) 
	{
		this.pageNum = pageNum;
		this.id = id;
		this.colIncrement = -1;
		this.rowIncrement = -1;
	}
	
	public void setInstructions(List<PDFInstruction> instructions) {
		this.instructions = instructions;
	}
	
	public void setColIncrement(int colIncrement) {
		this.colIncrement = colIncrement;
	}
	
	public void setRowIncrement(int rowIncrement) {
		this.rowIncrement = rowIncrement;
	}
	
	public void setCells(List<TableCell> cells) {
		this.cells = cells;
	}
	
	public List<TableCell> getCells() {
		return cells;
	}
	
	public int getColIncrement() {
		return colIncrement;
	}
	
	public int getRowIncrement() {
		return rowIncrement;
	}
	
	public int getId() {
		return id;
	}
	
	public List<PDFInstruction> getInstructions() {
		return instructions;
	}
	
	public int getPageNum() {
		return pageNum;
	}
	
}
