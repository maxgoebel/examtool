package at.tuwien.prip.core.model.project.example;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import at.tuwien.prip.core.model.BinaryClass;

/**
 * 
 * AbstractBinaryExample.java
 * 
 *
 *
 * @author mcg <mcgoebel@gmail.com>
 * Aug 3, 2012
 */
@Entity
public class AbstractBinaryExample 
implements IExample 
{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	protected BinaryClass classification;
	
	/**
	 * Constructor.
	 */
	public AbstractBinaryExample() {
		// TODO Auto-generated constructor stub
	}
	
	public BinaryClass getClassification() {
		return classification;
	}
	
	public void setClassification(BinaryClass classification) {
		this.classification = classification;
	}
	
}
