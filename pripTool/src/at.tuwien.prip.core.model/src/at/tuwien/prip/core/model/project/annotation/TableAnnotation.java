package at.tuwien.prip.core.model.project.annotation;

import java.util.ArrayList;
import java.util.List;

import at.tuwien.prip.core.model.project.selection.AbstractSelection;
import at.tuwien.prip.core.model.project.selection.TableSelection;

/**
 * TableAnnotation.java
 * 
 *
 *
 * @author mcg <mcgoebel@gmail.com>
 * Nov 19, 2012
 */
public class TableAnnotation extends Annotation 
{

	/**
	 * Constructor.
	 */
	public TableAnnotation(String uri) 
	{
		super(uri, AnnotationType.TABLE);
	}
	
	/**
	 * 
	 * @return
	 */
	public List<TableSelection> getTableItems() 
	{
		List<TableSelection> tables = new ArrayList<TableSelection>();
		for (AbstractSelection item : items)
		{
			if (item instanceof TableSelection)
			{
				tables.add((TableSelection) item);
			}
		}
		return tables;
	}
	
//	/**
//	 * 
//	 * @return
//	 */
//	public List<TableSelection> getAllTables() 
//	{
//		List<TableSelection> tables = new ArrayList<TableSelection>();
//		for (AnnotationPage page : pages)
//		{
//			for (AbstractSelection sel : page.getItems())
//			{
//				if (sel instanceof TableSelection)
//				{
//					tables.add((TableSelection) sel);
//				}
//			}
//		}
//		return tables;
//	}
}
