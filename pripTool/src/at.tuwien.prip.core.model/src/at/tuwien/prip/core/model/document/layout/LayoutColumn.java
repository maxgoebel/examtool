package at.tuwien.prip.core.model.document.layout;


/**
 * LayoutColumn.java
 *
 *
 * @author mcg <mcgoebel@gmail.com>
 * @date Aug 9, 2011
 */
public class LayoutColumn extends LayoutObject 
{
	public LayoutColumn(Alignment alignment) 
	{
		this.alignment = alignment;
	}
	
	private Alignment alignment;
	
	public Alignment getAlignment() {
		return alignment;
	}
	
}//LayoutColumn
