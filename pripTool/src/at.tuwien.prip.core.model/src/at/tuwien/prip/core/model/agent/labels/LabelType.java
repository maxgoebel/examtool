package at.tuwien.prip.core.model.agent.labels;

public enum LabelType {

	LOGICAL, LAYOUT, RELATION
}
