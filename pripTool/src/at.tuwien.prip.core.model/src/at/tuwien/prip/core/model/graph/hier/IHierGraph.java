package at.tuwien.prip.core.model.graph.hier;

import java.util.List;

import at.tuwien.prip.core.model.graph.base.BaseEdge;
import at.tuwien.prip.core.model.graph.base.BaseNode;
import at.tuwien.prip.core.model.graph.base.IGraph;

/**
 * IHierGraph.java
 * 
 * An interface for hierarchical graph representations.
 * 
 * @author: Max Goebel <mcgoebel@gmail.com>
 * @date: May 10, 2011
 */
public interface IHierGraph<S extends BaseNode, T extends BaseEdge<S>> 
extends IGraph<S,T>
{
	public List<S> getAllNodes();
	
	public IGraph<S,T> getBaseGraph ();
	
	public List<S> getContractionFamily(S a);
	
	public S contractNode (S a);
	
	public List<S> expandNode (S a);
	
	public IHierGraph<S,T> extractNeighSubGraph(T e);
	
	public IHierGraph<S,T> extractNeighSubGraph(S n);
	
}
