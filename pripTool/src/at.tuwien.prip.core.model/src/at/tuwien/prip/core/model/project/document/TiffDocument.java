package at.tuwien.prip.core.model.project.document;

import java.awt.Dimension;
import java.awt.Rectangle;

import org.eclipse.swt.graphics.Image;

/**
 * TiffDocument.java
 * 
 *
 *
 * @author mcg <mcgoebel@gmail.com>
 * Jul 6, 2012
 */
public class TiffDocument extends AbstractDocument
implements IDocument
{

	private Image image;
	
	public TiffDocument() {
		this.setFormat(DocumentFormat.TIFF);
	}
	
	@Override
	public Rectangle getBounds() {
		if (image!=null)
		{
			org.eclipse.swt.graphics.Rectangle b = image.getBounds();
			Rectangle result = new Rectangle(b.x,b.y,b.width,b.height);
			return result;
		}
		return null;
	}

	public Dimension getDimension() {
		if (image!=null)
		{
			return new Dimension(image.getBounds().width, image.getBounds().height);
		}
		return null;
	}

	public void setImage(Image image) {
		this.image = image;
	}
	
	public Image getImage() {
		return image;
	}
	
}
