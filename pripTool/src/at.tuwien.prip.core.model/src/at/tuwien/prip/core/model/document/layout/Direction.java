package at.tuwien.prip.core.model.document.layout;

/**
 * Direction.java
 *
 *
 * 
 * @author: Max Goebel <mcgoebel@gmail.com>
 * @date: Feb 20, 2011
 */
public enum Direction {
	
	NORTH, SOUTH, EAST, WEST
	
}
