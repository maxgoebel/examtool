package at.tuwien.prip.core.model.project.selection;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;


/**
 * RegionSelection.java
 * 
 *
 *
 * @author mcg <mcgoebel@gmail.com>
 * Nov 12, 2012
 */
@Entity
public class RegionSelection extends SinglePageSelection
{	
	@OneToMany
	private List<CellSelection> cells;
	
	@OneToMany
	private List<PDFInstruction> instructions;
	
	@OneToOne
	private TextSelection text;
	
	private int pageNum;
	
	public RegionSelection() {
		this.cells = new ArrayList<CellSelection>();
		this.instructions = new ArrayList<PDFInstruction>();
	}
	
	public List<CellSelection> getCells() {
		return cells;
	}
	
	public void setCells(List<CellSelection> cells) {
		this.cells = cells;
	}
	
	public TextSelection getText() {
		return text;
	}
	
	public void setText(TextSelection text) {
		this.text = text;
	}
	
	public List<PDFInstruction> getInstructions() {
		return instructions;
	}
	
	public void setInstructions(List<PDFInstruction> instructions) {
		this.instructions = instructions;
	}
	
	public int getPageNum() {
		return pageNum;
	}
	
	public void setPageNum(int pageNum) {
		this.pageNum = pageNum;
	}
}
