package at.tuwien.prip.core.model.token;

import at.tuwien.prip.core.model.token.Token;



/**
 * GraphToken.java
 *
 * A GraphToken can either represent a DocNode or a DocEdge.
 * 
 * @author: Max Goebel <mcgoebel@gmail.com>
 * @date: Apr 2, 2011
 */
public abstract class GraphToken extends Token {

	public GraphToken(String name) {
		super(name);
	}
	
	/**
	 * Delegate to respective method of class docnode/docedge.
	 */
	@Override
	public boolean equals(Object obj) 
	{
		if (this.getClass().equals(obj.getClass()) && obj instanceof NodeToken)
		{
			return ((NodeToken)obj).equals(this);
		} 
		else if (this.getClass().equals(obj.getClass()) && obj instanceof EdgeToken) 
		{
			return ((EdgeToken)obj).equals(this);
		}
		return false;
	}
	
}
