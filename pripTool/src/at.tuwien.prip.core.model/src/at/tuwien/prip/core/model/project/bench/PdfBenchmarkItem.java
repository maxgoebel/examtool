package at.tuwien.prip.core.model.project.bench;

import java.util.HashMap;
import java.util.Map;

import javax.persistence.Entity;

import org.eclipse.swt.graphics.ImageData;

import at.tuwien.prip.core.model.graph.ISegmentGraph;
import at.tuwien.prip.core.model.project.document.DocumentFormat;
import at.tuwien.prip.core.model.project.document.IPdfDocument;
import at.tuwien.prip.core.model.project.document.PDFDocument;

import com.sun.pdfview.PDFFile;
import com.sun.pdfview.PDFPage;

/**
 * PdfBenchmarkItem.java
 * 
 *
 *
 * @author mcg <mcgoebel@gmail.com>
 * Aug 3, 2012
 */
@Entity
public class PdfBenchmarkItem extends BenchmarkItem
implements IPdfDocument
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -1474288718668348609L;

	private int numPages;
	
	private int pageNum = 1;
	
	private ImageData imgData;

	private PDFFile pdfFile;
	
	private PDFPage page;
	
	private Map<Integer, ISegmentGraph> page2graphMap;
//	PDDocument document;
	
	/**
	 * Constructor.
	 */
	public PdfBenchmarkItem (PDFDocument doc)
	{
		this.numPages = doc.getNumPages();
		this.pageNum = doc.getPageNum();
		this.pdfFile = doc.getPdfFile();
		this.bounds = doc.getBounds();
		this.name = doc.getName();
		this.uri = doc.getUri();
		this.page2graphMap = new HashMap<Integer, ISegmentGraph>();
		for (int i=0;i<doc.getNumPages();i++)
		{
			this.page2graphMap.put(i, doc.getDocumentGraph(i));
		}
	}
	public PdfBenchmarkItem() 
	{
		this.format = DocumentFormat.PDF;
		this.page2graphMap = new HashMap<Integer, ISegmentGraph>();
	}
	public ImageData getImgData() {
		return imgData;
	}
	
	public PDFPage getPage() {
		return page;
	}
	
	public int getNumPages() {
		return numPages;
	}
	
	public void setNumPages(int numPages) {
		this.numPages = numPages;
	}
	
	public void setImgData(ImageData imgData) {
		this.imgData = imgData;
	}
	
	public void setPage(PDFPage page) {
		this.page = page;
	}
	
	public void setPdfFile(PDFFile pdfFile) {
		this.pdfFile = pdfFile;
	}
	
	public PDFFile getPdfFile() {
		return pdfFile;
	}
	
	public int getPageNum() {
		return pageNum;
	}
	
	public void setPageNum(int pageNum) {
		this.pageNum = pageNum;
	}
	
	public ISegmentGraph getDocumentGraph(int pageNum)
	{
		return page2graphMap.get(pageNum);
	}
	
	public void setDocumentGraph(int pageNum, ISegmentGraph g)
	{
		this.page2graphMap.put(pageNum, g);
	}
	
	@Override
	public String toString() {
		return "PDF Benchmark Item: " + name;
	}
}
