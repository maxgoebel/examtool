package at.tuwien.prip.core.model.document.semantics;

import at.tuwien.prip.common.datastructures.Pair;


/**
 * 
 * WGKeyPair.java
 *
 *
 *
 * Created: Jul 8, 2009 8:15:23 PM
 *
 * @author mcg <goebel@gmail.com>
 * @version 1.0
 */
public class WGKeyPair extends Pair<SemanticText, SemanticText> {

	public WGKeyPair(SemanticText a, SemanticText b) {
		super(a, b);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 6175525552762000971L;

}
