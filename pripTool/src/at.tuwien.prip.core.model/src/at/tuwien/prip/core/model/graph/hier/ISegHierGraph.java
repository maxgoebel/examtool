package at.tuwien.prip.core.model.graph.hier;

import at.tuwien.prip.core.model.graph.DocEdge;
import at.tuwien.prip.core.model.graph.DocNode;
import at.tuwien.prip.core.model.graph.ISegmentGraph;

/**
 * 
 * ISegHierGraph.java
 * 
 *
 *
 * @author mcg <mcgoebel@gmail.com>
 * Jan 5, 2012
 */
public interface ISegHierGraph extends IHierGraph<DocNode, DocEdge>
{
	@Override
	public ISegmentGraph getBaseGraph();

}
