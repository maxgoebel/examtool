package at.tuwien.prip.core.model.project.document;

import at.tuwien.prip.core.model.project.example.ExampleSet;


/**
 * WrapperDocument.java
 * 
 *
 *
 * @author mcg <mcgoebel@gmail.com>
 * Jun 30, 2012
 */
public class WrapperDocument 
{

	private IDocument documentItem;
	
	private ExampleSet examples;
	
	/**
	 * 
	 * @param document
	 */
	public WrapperDocument(IDocument document) 
	{
		this.documentItem = document;
		examples = new ExampleSet();
	}
	
	public IDocument getDocumentItem() {
		return documentItem;
	}
	
	public void setDocumentItem(IDocument documentItem) {
		this.documentItem = documentItem;
	}
	
	public ExampleSet getExamples() {
		return examples;
	}
}
