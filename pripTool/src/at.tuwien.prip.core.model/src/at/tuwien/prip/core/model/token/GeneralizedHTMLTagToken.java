package at.tuwien.prip.core.model.token;

import java.util.ArrayList;
import java.util.List;

import org.w3c.dom.Element;

/**
 * 
 * GeneralizedHTMLTagToken.java
 *
 *
 *
 * Created: Apr 27, 2009 11:52:46 PM
 *
 * @author mcg <goebel@gmail.com>
 * @version 1.0
 */
public class GeneralizedHTMLTagToken extends HTMLTagToken {

	List<Element> elements;
	
	/**
	 * 
	 * Constructor.
	 * 
	 * @param elements
	 */
	public GeneralizedHTMLTagToken(Element...elements) {
		super("");
		this.elements = new ArrayList<Element>();
		for (Element e : elements) {
			addElement(e);
		}
	}
	
	public GeneralizedHTMLTagToken(HTMLTagToken...tokens) {
		super("");
		this.elements = new ArrayList<Element>();
		for (HTMLTagToken e : tokens) {
			if (e.getElement()!=null) {
				addElement(e.getElement());
			}
		}
	}
	
	public void addElement(Element e) {
		elements.add(e);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public List<Element> getElements() {
		return elements;
	}

	
}
