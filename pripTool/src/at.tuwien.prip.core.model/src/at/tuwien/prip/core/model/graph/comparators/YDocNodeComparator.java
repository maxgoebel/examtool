package at.tuwien.prip.core.model.graph.comparators;

import java.util.Comparator;

import at.tuwien.prip.core.model.graph.DocNode;



/**
 * YDocNodeComparator.java
 *
 *
 * 
 * @author: Max Goebel <mcgoebel@gmail.com>
 * @date: Mar 9, 2011
 */
public class YDocNodeComparator implements Comparator<DocNode> {

	@Override
	public int compare(DocNode o1, DocNode o2) {
		// sorts in y order
		double y1 = o1.getSegY1();
		double y2 = o2.getSegY1();

		return (int) (y2 - y1);
	}
	
	@Override
	public boolean equals(Object obj)
	{
		return obj.equals(this);
	}

}
