package at.tuwien.prip.core.model.project.result;

import java.util.HashSet;

/**
 * ResultSet.java
 * 
 *
 *
 * @author mcg <mcgoebel@gmail.com>
 * Aug 27, 2012
 */
public class ResultSet extends HashSet<IResult>
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3779186254665978212L;

}
