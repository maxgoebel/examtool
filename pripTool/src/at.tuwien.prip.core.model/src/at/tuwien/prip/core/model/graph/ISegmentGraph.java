package at.tuwien.prip.core.model.graph;

import java.awt.Rectangle;
import java.util.List;
import java.util.Map;

import at.tuwien.prip.core.model.document.segments.GenericSegment;
import at.tuwien.prip.core.model.graph.base.IGraph;

/**
 * ISegmentGraph.java
 * 
 *
 *
 * @author mcg <mcgoebel@gmail.com>
 * Jan 21, 2012
 */
public interface ISegmentGraph extends IGraph<DocNode,DocEdge> 
{
//	public Rectangle getDimensions();
		
	public void computeDimensions();
	
	public Map<DocNode, GenericSegment> getNodeSegHash();

	public Map<GenericSegment, DocNode> getSegNodeHash();
	
	public GenericSegment getSegment (DocNode node);
	
	public int getNumPages();
	
	public double getLineSpaceing();
	
	public String serializeText();
	
	@Override
	public ISegmentGraph getSubGraph(List<DocNode> neighNodes);
}
