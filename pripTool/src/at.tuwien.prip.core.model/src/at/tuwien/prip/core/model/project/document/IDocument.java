package at.tuwien.prip.core.model.project.document;

import java.awt.Rectangle;

/**
 * IDocument.java
 * 
 *
 *
 * @author mcg <mcgoebel@gmail.com>
 * May 20, 2012
 */
public interface IDocument 
{
	public void setUri(String uri);
	
	public String getUri();
	
	public String getName ();
	
	public void setName(String name);
	
	public DocumentFormat getFormat();
	
	public void setFormat(DocumentFormat format);
	
	public double getScale();
	
	public void setScale(double scale);
	
	public Rectangle getBounds();	
}
