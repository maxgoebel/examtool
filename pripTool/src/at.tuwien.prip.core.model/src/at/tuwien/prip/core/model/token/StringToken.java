package at.tuwien.prip.core.model.token;

public class StringToken extends Token {

	public StringToken(String name) {
		super(name);
	}

	@Override
	public String toString() {
		return name;
	}
}
