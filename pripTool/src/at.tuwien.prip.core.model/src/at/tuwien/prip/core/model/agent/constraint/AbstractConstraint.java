package at.tuwien.prip.core.model.agent.constraint;

/**
 * AbstractConstraint.java
 * 
 *
 *
 * @author mcg <mcgoebel@gmail.com>
 * Nov 20, 2012
 */
public class AbstractConstraint 
{

	protected ConstraintSource source;
	
	protected ConstraintType type;
}
