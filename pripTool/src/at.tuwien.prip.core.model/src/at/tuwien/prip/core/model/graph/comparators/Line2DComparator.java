package at.tuwien.prip.core.model.graph.comparators;

import java.awt.geom.Line2D;
import java.util.Comparator;

/**
 * 
 * Line2DComparator.java
 *
 *
 * @author mcg <mcgoebel@gmail.com>
 * @date Jul 6, 2011
 */
public class Line2DComparator implements Comparator<Line2D> 
{

	@Override
	public int compare(Line2D o1, Line2D o2) {
		double l1 = o1.getP1().distance(o1.getP2());
		double l2 = o2.getP1().distance(o2.getP2());
		if (l1<l2){
			return -1;
		} else if (l1==l2) {
			return 0;
		}
		return 1;
	}

}
