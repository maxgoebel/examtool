package at.tuwien.prip.core.model.document.layout;

public enum Alignment
{
	LEFT, RIGHT, TOP, BOTTOM, X_CENTER, Y_CENTER, MIXED, BOTH
}
