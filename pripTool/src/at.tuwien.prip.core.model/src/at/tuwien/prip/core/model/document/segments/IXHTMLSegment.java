package at.tuwien.prip.core.model.document.segments;

import org.w3c.dom.Document;
import org.w3c.dom.Element;


/**
 * Segments which can be output to XHTML should implement this interface.
 *
 * @author Tamir Hassan, pdfanalyser@tamirhassan.com
 * @version PDF Analyser 0.9
 */
public interface IXHTMLSegment
{
    public abstract void addAsXHTML(Document resultDocument, Element parent);
}
