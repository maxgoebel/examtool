package at.tuwien.prip.core.model.project.example;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.OneToMany;

import at.tuwien.prip.core.model.project.selection.AbstractSelection;

/**
 * SelectionExample.java
 * 
 *
 *
 * @author mcg <mcgoebel@gmail.com>
 * Nov 19, 2012
 */
@Entity
public class SelectionExample extends AbstractBinaryExample
{
	@OneToMany
	protected List<AbstractSelection> selections;

	public SelectionExample() {
		this.selections = new ArrayList<AbstractSelection>();
	}
	
	public List<AbstractSelection> getSelections() {
		return selections;
	}
	
	public void setSelections(List<AbstractSelection> selections) {
		this.selections = selections;
	}
	
}
