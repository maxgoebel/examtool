package at.tuwien.prip.core.model.project.bench;

import javax.persistence.Entity;

import org.w3c.dom.Document;

import at.tuwien.prip.core.model.project.document.DocumentFormat;
import at.tuwien.prip.core.model.project.document.IHtmlDocument;

/**
 * HTMLBenchmarkItem.java
 * 
 *
 *
 * @author mcg <mcgoebel@gmail.com>
 * Nov 9, 2012
 */
@Entity
public class HTMLBenchmarkItem extends BenchmarkItem
implements IHtmlDocument
{
	private Document cachedJavaDOM;
	
	/**
	 * 
	 */
	public HTMLBenchmarkItem() 
	{
		this.format = DocumentFormat.HTML;
	}
	
	public Document getCachedJavaDOM() {
		return cachedJavaDOM;
	}
	
	public void setCachedJavaDOM(Document cachedJavaDOM) {
		this.cachedJavaDOM = cachedJavaDOM;
	}
}
