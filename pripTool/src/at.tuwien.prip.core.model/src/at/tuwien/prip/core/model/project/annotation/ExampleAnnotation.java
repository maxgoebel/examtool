package at.tuwien.prip.core.model.project.annotation;

import java.util.ArrayList;
import java.util.List;

import at.tuwien.prip.core.model.project.selection.AbstractSelection;
import at.tuwien.prip.core.model.project.selection.ExampleSelection;

/**
 * ExampleAnnotation.java
 * 
 *
 *
 * @author mcg <mcgoebel@gmail.com>
 * Nov 19, 2012
 */
public class ExampleAnnotation extends Annotation 
{
	/**
	 * 
	 * @param uri
	 */
	public ExampleAnnotation(String uri) 
	{
		super(uri, AnnotationType.EXAMPLE);
	}

	public List<ExampleSelection> getExampleItems() {
		List<ExampleSelection> examples = new ArrayList<ExampleSelection>();
		for (AbstractSelection item : items)
		{
			if (item instanceof ExampleSelection)
			{
				examples.add((ExampleSelection) item);
			}
		}
		return examples;
	}
	
}