package at.tuwien.prip.core.model.action;

/**
 * IState.java
 *
 * A basic state.
 * 
 * @author: Max Goebel <mcgoebel@gmail.com>
 * @date: Mar 29, 2011
 */
public interface IState {

}
