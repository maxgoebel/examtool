package at.tuwien.prip.core.model.action;

/**
 * 
 * ActionState.java
 *
 * An action state is the combination of a state and an action.
 * 
 * @author: Max Goebel <mcgoebel@gmail.com>
 * @date: Mar 29, 2011
 */
public class ActionState {

	private IState state;

	private Action action;

	public IState getState() {
		return state;
	}

	public void setState(IState state) {
		this.state = state;
	}

	public void setAction(Action action) {
		this.action = action;
	}

	public Action getAction() {
		return action;
	}
	
	@Override
	public String toString() {
		return state.toString() + "\nAction: " +action.getName();
	}

}