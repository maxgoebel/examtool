package at.tuwien.prip.core.model.document.segments;



/**
 * Blank table cell document element
 * 
 * @author Tamir Hassan, pdfanalyser@tamirhassan.com
 * @version PDF Analyser 0.9
 */
public class BlankCell extends TableCell implements IBlankSegment
{
    
    /**
     * Constructor.
     */
    public BlankCell(int colspan, int rowspan)
    {
		this.setColspan(colspan);
		this.setRowspan(rowspan);
	}
    
    public String getText()
    {
    	//return ("<blank" + colspan +"," + rowspan + ">");
    	return ("");//"<blank>");
    }
    
    public String toString()
    {
    	return "BlankCell";
    }
}
