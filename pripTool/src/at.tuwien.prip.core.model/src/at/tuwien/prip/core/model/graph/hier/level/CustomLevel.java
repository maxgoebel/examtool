package at.tuwien.prip.core.model.graph.hier.level;

import at.tuwien.prip.core.model.graph.DocEdge;
import at.tuwien.prip.core.model.graph.DocNode;

public class CustomLevel extends AbstractLevel<DocNode,DocEdge> 
implements IGraphLevel<DocNode,DocEdge> 
{
	private int level = -1;
	
	/**
	 * Constructor.
	 * 
	 * @param level
	 * @param parent
	 */
	public CustomLevel(int level, SegLevelGraph parent) {
		this.level = level;
		this.parent = parent;
	}

	@Override
	public int getLevel() {
		return level;
	}

}
