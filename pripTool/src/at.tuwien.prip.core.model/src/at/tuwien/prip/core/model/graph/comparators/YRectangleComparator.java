package at.tuwien.prip.core.model.graph.comparators;

import java.awt.Rectangle;
import java.util.Comparator;

/**
 * YRectangleComparator.java
 *
 *
 * @author mcg <mcgoebel@gmail.com>
 * @date Sep 12, 2011
 */
public class YRectangleComparator implements Comparator<Rectangle> {

	@Override
	public int compare(Rectangle o1, Rectangle o2) {
		// sorts in y order
		double y1 = o1.getMinY();
		double y2 = o2.getMinY();

		return (int) (y2 - y1);
	}
	
	@Override
	public boolean equals(Object obj)
	{
		return obj.equals(this);
	}

}
