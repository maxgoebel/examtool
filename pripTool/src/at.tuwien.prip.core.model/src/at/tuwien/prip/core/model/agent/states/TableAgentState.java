package at.tuwien.prip.core.model.agent.states;

import java.util.List;

import at.tuwien.prip.common.utils.ListUtils;
import at.tuwien.prip.core.model.agent.IAgent;
import at.tuwien.prip.core.model.agent.labels.LayoutLabeling;

/**
 * TableAgentState.java
 * 
 *
 *
 * @author mcg <mcgoebel@gmail.com>
 * Aug 8, 2012
 */
public class TableAgentState extends AgentState
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -6286366351809362470L;

	private List<AgentState> tableCells;
	/**
	 * Constructor.
	 * @param ll
	 * @param owner
	 * @param reconsider
	 */
	public TableAgentState(LayoutLabeling ll, IAgent owner, boolean reconsider)
	{
		super(ll, owner, reconsider);
		this.tableCells = ListUtils.toList(ll.getUtilizedStates(),AgentState.class);
	}

	public List<AgentState> getTableCells() {
		return tableCells;
	}
	
	public void setTableCells(List<AgentState> tableCells) {
		this.tableCells = tableCells;
	}
}
