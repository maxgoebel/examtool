package at.tuwien.prip.core.model.agent.states;

import at.tuwien.prip.core.model.agent.IAgent;
import at.tuwien.prip.core.model.agent.labels.LayoutLabel;

public class LogicalState extends AgentState 
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -2003715998224607812L;


	public LogicalState(LayoutLabel ll, IAgent owner, boolean reconsider) {
		super(ll, owner, reconsider);
	}
	
	@Override
	public String toString() {
		return "Logical State - "+getSegType()+": "+getTextContent().getText();
	}
	
}
