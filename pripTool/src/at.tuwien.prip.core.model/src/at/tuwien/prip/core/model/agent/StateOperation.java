package at.tuwien.prip.core.model.agent;

public enum StateOperation 
{
	SPLIT, MERGE
}
