package at.tuwien.prip.core.model.project.annotation;

import java.util.ArrayList;
import java.util.List;

import at.tuwien.prip.core.model.project.selection.AbstractSelection;
import at.tuwien.prip.core.model.project.selection.LabelSelection;

/**
 * 
 * LabelAnnotation.java
 * 
 *
 *
 * @author mcg <mcgoebel@gmail.com>
 * Nov 19, 2012
 */
public class LabelAnnotation extends Annotation 
{
	private AnnotationLabel label;
	
	public LabelAnnotation(String uri) 
	{
		super(uri, AnnotationType.LABEL);
	}
	
	/**
	 * 
	 * @return
	 */
	public List<LabelSelection> getLabelItems()
	{
		List<LabelSelection> labels = new ArrayList<LabelSelection>();
		for (AbstractSelection item : items)
		{
			if (item instanceof LabelSelection)
			{
				labels.add((LabelSelection) item);
			}
		}
		return labels;
	}
	
	public void setLabel(AnnotationLabel label) {
		this.label = label;
	}
	
	public AnnotationLabel getLabel() {
		return label;
	}
}
