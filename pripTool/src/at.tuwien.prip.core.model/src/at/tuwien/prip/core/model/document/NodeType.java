package at.tuwien.prip.core.model.document;

public enum NodeType { 
	
	Element, Text, Node, Document, Attribute, Comment, Image, Input
	
};