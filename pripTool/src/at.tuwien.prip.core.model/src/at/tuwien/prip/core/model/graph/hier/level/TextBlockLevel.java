package at.tuwien.prip.core.model.graph.hier.level;

import at.tuwien.prip.core.model.graph.DocEdge;
import at.tuwien.prip.core.model.graph.DocNode;
import at.tuwien.prip.core.model.graph.DocumentConstants;

/**
 * TextBlockLevel.java
 *
 *
 * 
 * @author: Max Goebel <mcgoebel@gmail.com>
 * @date: Mar 4, 2011
 */
public class TextBlockLevel extends AbstractLevel<DocNode,DocEdge>
implements IGraphLevel<DocNode,DocEdge>
{

	/**
	 * Constructor.
	 * @param stack
	 */
	public TextBlockLevel (SegLevelGraph stack) {
		this.parent = stack;
	}
	
	@Override
	public int getLevel() {
		return DocumentConstants.TEXTBLOCK_LEVEL;
	}

}