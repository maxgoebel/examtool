package at.tuwien.prip.core.model.graph.hier;

import at.tuwien.prip.core.model.graph.DocNode;



/**
 * DocContractionEdge.java
 * 
 * @author Max Goebel <mcgoebel@gmail.com>
 * @date
 */
public class DocContractionEdge extends DocEdgeHier {

	/**
	 * Constructor.
	 * 
	 * @param nodeFrom
	 * @param nodeTo
	 */
	public DocContractionEdge(DocNode nodeFrom, DocNode nodeTo) {
		super(nodeFrom, nodeTo);
	}

}
