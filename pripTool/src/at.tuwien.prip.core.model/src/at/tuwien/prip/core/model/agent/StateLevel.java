package at.tuwien.prip.core.model.agent;

public enum StateLevel 
{
	LOW, MEDIUM, HIGH
}
