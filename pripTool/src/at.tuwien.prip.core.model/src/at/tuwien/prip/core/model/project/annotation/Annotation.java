package at.tuwien.prip.core.model.project.annotation;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import at.tuwien.prip.core.model.project.selection.AbstractSelection;

/**
 * Annotation.java
 * 
 *
 *
 * @author mcg <mcgoebel@gmail.com>
 * Nov 12, 2012
 */
@Entity
public class Annotation 
{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	protected Long id;
	
	protected String uri;
	
	protected AnnotationType type;
	
	@OneToMany
	protected List<AbstractSelection> items;
	
	/**
	 * Constructor.
	 */
	public Annotation(String uri, AnnotationType type) 
	{
		this.uri = uri;
		this.type = type;
		this.items = new ArrayList<AbstractSelection>();
	}
		
	public AnnotationType getType() {
		return type;
	}
	
	public void setItems(List<AbstractSelection> items) {
		this.items = items;
	}
	
	public List<AbstractSelection> getItems() {
		return items;
	}
	
	public void addSelection (AbstractSelection selection)
	{
		items.add(selection);
	}
}
