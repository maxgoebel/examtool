package at.tuwien.prip.core.model.project.selection;

import javax.persistence.Entity;

/**
 * TableSelection.java
 * 
 *
 *
 * @author mcg <mcgoebel@gmail.com>
 * Nov 12, 2012
 */
@Entity
public class TableSelection extends MultiPageSelection
{	
	/**
	 * Constructor.
	 */
	public TableSelection() {

	}
}
