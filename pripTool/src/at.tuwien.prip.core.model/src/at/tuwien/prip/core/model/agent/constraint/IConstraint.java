package at.tuwien.prip.core.model.agent.constraint;

/**
 * 
 * IConstraint.java
 * 
 *
 *
 * @author mcg <mcgoebel@gmail.com>
 * Jul 16, 2012
 */
public interface IConstraint 
{

	public boolean isConflicting(IConstraint other);
	
}
