package at.tuwien.prip.core.model.document;

/**
 * MetaInfo.java
 *
 *
 * 
 * @author: Max Goebel <mcgoebel@gmail.com>
 * @date: Feb 20, 2011
 */
public enum MetaInfo {
	
	SELECTION, SEGMENT, TABLE, EXTRACTION
	
}
