package at.tuwien.prip.core.model.agent.attributes;

import java.util.List;

import at.tuwien.prip.core.model.agent.IAgent;
import at.tuwien.prip.core.model.agent.states.AgentState;
import at.tuwien.prip.core.model.document.layout.Alignment;
import at.tuwien.prip.core.model.document.semantics.WordSemantics;

/**
 * RowColModel.java
 * 
 *
 *
 * @author mcg <mcgoebel@gmail.com>
 * Nov 5, 2012
 */
public class RowColModel extends AgentStateAttribute 
{
	
	private Alignment alignment;
	
	private List<AgentState> accessors;
	
	private WordSemantics accessorSemantic;
	
	private WordSemantics valueSemantic;
	
	private double dim;
	
	/**
	 * Constructor.
	 * @param value
	 * @param owner
	 */
	public RowColModel(String value, List<AgentState> accessors, 
			WordSemantics accessorSemantic, 
			WordSemantics valueSemantic, 
			Alignment alignment, 
			double dim,
			IAgent owner) 
	{
		super(AttributeType.ROW_MODEL, value, owner);
		
		this.alignment = alignment;
		this.accessors = accessors;
		this.valueSemantic = valueSemantic;
		this.accessorSemantic = accessorSemantic;
		this.dim = dim;
	}
	
	public double getDim() {
		return dim;
	}
	
	public WordSemantics getAccessorSemantic() {
		return accessorSemantic;
	}
	
	public WordSemantics getValueSemantic() {
		return valueSemantic;
	}
	
	public Alignment getAlignment() {
		return alignment;
	}
	
	public List<AgentState> getAccessors() {
		return accessors;
	}
}
