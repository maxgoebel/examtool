package at.tuwien.prip.core.model.agent.attributes;

import at.tuwien.prip.core.model.agent.IAgent;

public class TableModel extends AgentStateAttribute
{

	/**
	 * Constructor.
	 * @param value
	 * @param owner
	 */
	public TableModel(String value, IAgent owner) {
		super(AttributeType.TABEL_MODEL, value, owner);
		
	}

}
