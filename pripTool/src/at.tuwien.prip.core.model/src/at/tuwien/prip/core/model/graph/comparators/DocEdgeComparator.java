package at.tuwien.prip.core.model.graph.comparators;

import java.util.Comparator;

import at.tuwien.prip.core.model.graph.DocEdge;

/**
 * DocEdgeComparator.java
 *
 *
 * @author mcg <mcgoebel@gmail.com>
 * @date Jul 5, 2011
 */
public class DocEdgeComparator implements Comparator<DocEdge> 
{

	@Override
	public int compare(DocEdge o1, DocEdge o2) {
		if (o1.getLength()==o2.getLength()) {
			return 0;
		} else if (o1.getLength()<o2.getLength()) {
			return -1;
		}
		return 1;
	}

}
