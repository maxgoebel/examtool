package at.tuwien.prip.core.model.agent.states;

import java.awt.Font;

public class TextContent 
{
	private Font font;

	private String text;

	public TextContent(String text, Font font) 
	{
		this.text = text;
		this.font = font;
	}

	public Font getFont() {
		return font;
	}

	public String getText() {
		return text;
	}

	

}
