package at.tuwien.prip.core.model.agent.states;

import at.tuwien.prip.core.model.agent.IAgent;
import at.tuwien.prip.core.model.agent.labels.LayoutLabel;
import at.tuwien.prip.core.model.graph.DocNode;
import at.tuwien.prip.core.model.graph.ISegmentGraph;

public class LayoutState extends AgentState 
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4870987154475899012L;


	public LayoutState(LayoutLabel ll, IAgent owner, boolean reconsider) {
		super(ll, owner, reconsider);

	}

	public LayoutState(DocNode node) {
		super(node);
	}
	
	public LayoutState(ISegmentGraph graph) {
		super(graph);
	}

	@Override
	public String toString() {
		return "Layout State - "+getSegType()+": "+getTextContent().getText();
	}
}
