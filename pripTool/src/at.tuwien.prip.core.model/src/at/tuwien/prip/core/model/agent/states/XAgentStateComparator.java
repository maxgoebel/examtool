package at.tuwien.prip.core.model.agent.states;


import java.util.Comparator;



/**
 * XDocNodeComparator.java
 *
 *
 * 
 * @author: Max Goebel <mcgoebel@gmail.com>
 * @date: Mar 9, 2011
 */
public class XAgentStateComparator implements Comparator<AgentState> {

	@Override
	public int compare(AgentState s1, AgentState s2) {
		// sorts in x order
		double x1 = s1.getBounds().getMinX();
		double x2 = s2.getBounds().getMinX();

		return (int) (x1 - x2);
	}

	@Override
	public boolean equals(Object obj)
	{
		return obj.equals(this);
	}
	
}
