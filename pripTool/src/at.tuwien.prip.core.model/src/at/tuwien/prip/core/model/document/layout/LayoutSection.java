package at.tuwien.prip.core.model.document.layout;

import at.tuwien.prip.core.model.graph.DocNode;

/**
 * 
 * LayoutSection.java
 *
 *
 * @author mcg <mcgoebel@gmail.com>
 * @date Aug 31, 2011
 */
public class LayoutSection extends LayoutObject 
{
	private String title;
	
	private DocNode sectionHeader;

	/**
	 * Constructor.
	 */
	public LayoutSection() {
		// TODO Auto-generated constructor stub
	}
	
	public void setSectionHeader(DocNode sectionHeader) {
		this.sectionHeader = sectionHeader;
	}

	public DocNode getSectionHeader() {
		return sectionHeader;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getTitle() {
		return title;
	}
	
}
