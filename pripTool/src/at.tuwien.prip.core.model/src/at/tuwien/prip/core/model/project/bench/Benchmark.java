package at.tuwien.prip.core.model.project.bench;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

/**
 * 
 * @author max
 *
 */
@Entity
public class Benchmark
{

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	private String uri;
	@OneToMany
	private List<BenchmarkItem> items;
	
	private String name;
	
	/**
	 * Constructor.
	 */
	public Benchmark()
	{
		items = new ArrayList<BenchmarkItem>();
	}
	
	/**
	 * Constructor.
	 * @param name
	 */
	public Benchmark(String uri) 
	{
		this();
		this.uri = uri;
		this.name = uri;
	}
	
	public String getUri() {
		return uri;
	}
	
	public void setUri(String uri) {
		this.uri = uri;
	}
	
	public List<BenchmarkItem> getItems() {
		return items;
	}
	
	public void setItems(List<BenchmarkItem> items) {
		this.items = items;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	@Override
	public String toString() {
		return name;
	}
}
