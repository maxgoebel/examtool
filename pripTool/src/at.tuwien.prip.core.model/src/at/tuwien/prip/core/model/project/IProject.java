package at.tuwien.prip.core.model.project;

import java.util.List;

import at.tuwien.prip.core.model.project.document.AbstractDocument;

/**
 * IProject.java
 * 
 *
 *
 * @author mcg <mcgoebel@gmail.com>
 * Jul 10, 2012
 */
public interface IProject 
{
	public void addDocument(AbstractDocument document);
	
	public void removeDocument(AbstractDocument document);
	
	public List<AbstractDocument> getDocuments();
	
	public String getName();
	
	public String getProjectResource();

	public void setName(String string);
}
