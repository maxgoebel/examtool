package at.tuwien.prip.core.model.project.selection;

import javax.persistence.Entity;


/**
 * PDFInstruction.java
 * 
 *
 *
 * @author mcg <mcgoebel@gmail.com>
 * Nov 12, 2012
 */
@Entity
public class PDFInstruction extends AbstractSelection
{
//	@Id
//	@GeneratedValue(strategy = GenerationType.IDENTITY)
//	private Long id;
	
	private int instructionID;
	
	private int subinstructionID;
	
	private String text;
	
	/**
	 * Constructor.
	 */
	public PDFInstruction()
	{
		
	}
	
	/**
	 * Constructor.
	 * @param instructionID
	 * @param subinstructionID
	 */
	public PDFInstruction(int instructionID, int subinstructionID) 
	{
		this.instructionID = instructionID;
		this.subinstructionID = subinstructionID;
	}
	
	public int getIndex()
	{
		return instructionID;
	}
	
	public int getSubIndex()
	{
		return subinstructionID;
	}
	
	public String getText() {
		return text;
	}
	
	public void setText(String text) {
		this.text = text;
	}
}
