package at.tuwien.prip.core.model.graph.comparators;

import java.util.Comparator;

import at.tuwien.prip.core.model.graph.DocNode;



/**
 * XDocNodeComparator.java
 *
 *
 * 
 * @author: Max Goebel <mcgoebel@gmail.com>
 * @date: Mar 9, 2011
 */
public class XDocNodeComparator implements Comparator<DocNode> {

	@Override
	public int compare(DocNode o1, DocNode o2) {
		// sorts in x order
		double x1 = o1.getSegX1();
		double x2 = o2.getSegX1();

		return (int) (x1 - x2);
	}

	@Override
	public boolean equals(Object obj)
	{
		return obj.equals(this);
	}
	
}
