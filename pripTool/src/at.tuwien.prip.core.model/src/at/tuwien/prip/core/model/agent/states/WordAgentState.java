package at.tuwien.prip.core.model.agent.states;

import java.awt.Font;

import at.tuwien.prip.core.model.graph.DocNode;

/**
 * WordAgentState.java
 * 
 *
 *
 * @author mcg <mcgoebel@gmail.com>
 * Aug 8, 2012
 */
public class WordAgentState extends AgentState
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private boolean isSalient = false;
	
	private int headingLevel = -1;
	

	
	/**
	 * Constructor.
	 * @param node
	 */
	public WordAgentState(DocNode node) 
	{
		super(node);
		this.font = node.getFont();
		this.textContent = node.getSegText();
	}

	public boolean isSalient() {
		return isSalient;
	}
	
	public void setSalient(boolean salient)
	{
		this.isSalient = true;
	}
	
	public int getHeadingLevel() {
		return headingLevel;
	}
	
	public void setHeadingLevel(int headingLevel) {
		this.headingLevel = headingLevel;
	}
	
	public Font getFont() {
		return font;
	}

}
