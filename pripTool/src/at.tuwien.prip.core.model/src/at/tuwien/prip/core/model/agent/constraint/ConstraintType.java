package at.tuwien.prip.core.model.agent.constraint;

public enum ConstraintType {

	KEEP, IGNORE
}
