package at.tuwien.prip.core.model.document.semantics;


import at.tuwien.prip.common.datastructures.Rank;


/**
 * 
 * WordTypeRank.java
 *
 * An ordered list of word semantics.
 *
 * Created: Jul 9, 2009 8:02:01 AM
 *
 * @author mcg <goebel@gmail.com>
 * @version 1.0
 */
public class WordTypeRank extends Rank<RankedSemanticText> {

	public WordTypeRank(WordSemantics type) {
		super (new RankedSemanticText(type));
	}

	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer("<");
		for (RankedSemanticText e : elements) {
			sb.append(e.toString()+", ");
		}
		sb.replace(sb.length()-2, sb.length(), ">");
		return sb.toString();
	}
	
//	public WordTypeRank(WordSemantics type) {
//		super (new RWordType(type));
//	}
//
//	@Override
//	public String toString() {
//		StringBuffer sb = new StringBuffer("<");
//		for (RWordType e : elements) {
//			sb.append(e.toString()+", ");
//		}
//		sb.replace(sb.length()-2, sb.length(), ">");
//		return sb.toString();
//	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof WordTypeRank) {
			WordTypeRank other = (WordTypeRank) obj;
			return other.toString().equals(toString());
		}
		return false;
	}
}
