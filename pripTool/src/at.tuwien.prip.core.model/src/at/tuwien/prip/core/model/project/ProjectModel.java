package at.tuwien.prip.core.model.project;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


/**
 * 
 * ProjectModel.java
 * 
 *
 *
 * @author mcg <mcgoebel@gmail.com>
 * Jun 10, 2012
 */
@Entity
public class ProjectModel
{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private List<IProject> projects;

	private IProject activeProject;

	public ProjectModel() {
		this.projects = new ArrayList<IProject>();
	}

	public IProject getActiveProject() {
		return activeProject;
	}

	public void setActiveProject(IProject activeProject) {
		this.activeProject = activeProject;
	}

	public List<IProject> getProjects() {
		return projects;
	}

	public void addProject(IProject project) {
		if (!projects.contains(project))
		{
			projects.add(project);
		}
	}

	public void removeProject(IProject project)
	{
		if (projects.contains(project))
		{
			projects.remove(project);
		}
		if (activeProject==project)
		{
			activeProject=null;
		}
	}
}
