package at.tuwien.prip.core.model.attributes.html;

/**
 * Comment.java
 *
 *
 * Created: Mon Jul 28 10:13:10 2003
 *
 * @author Michal Ceresna
 * @version 1.0
 */
public class Comment {

    private final String comment;

    public Comment(String comment) {
        this.comment = comment;
    }

    public String getComment() {
        return comment;
    }

} // Comment
