package at.tuwien.prip.core.model.document.segments.web;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

import at.tuwien.prip.core.model.utils.DOMHelper;
import at.tuwien.prip.core.model.utils.DOMHelper2;


/**
 * HTMLForm.java
 *
 *
 * A container for a HTML form, holding all
 * necessary parameters.
 *
 * Created: May 6, 2009 9:08:55 PM
 *
 * @author mcg <goebel@gmail.com>
 * @version 1.0
 */
public class HTMLForm 
{
	
	private Element root;
	
	private FormMethod method;
	private String action;
	private String name;
	private URL url;
	private List<HTMLFormInput> inputs = new ArrayList<HTMLFormInput>();
	
	private boolean submittable = false;

	/**
	 * Constructor.
	 * 
	 * @param root
	 */
	public HTMLForm (Element root) {
		assert root.getTagName().equals("form");

		this.root = root;
		
		try {
			this.url = new URL(root.getOwnerDocument().getBaseURI());
		} catch (MalformedURLException e) {
			this.url = null;
		}

		NamedNodeMap nnm = root.getAttributes();
		for (int i=0; i<nnm.getLength(); i++) {
			Node a = nnm.item(i);	
			String attName = a.getNodeName();
			if (attName.equalsIgnoreCase("method")) {
				if (a.getNodeValue().equalsIgnoreCase("get")) {
					this.method = FormMethod.GET;
				} else if (a.getNodeValue().equalsIgnoreCase("post")) {
					this.method = FormMethod.POST;
				}
			} else if (attName.equalsIgnoreCase("action")) {
				this.action = a.getNodeValue();
			}else if (attName.equalsIgnoreCase("name")) {
				this.name = a.getNodeValue();
			}
		}

		List<Element> inputElements = 
			DOMHelper.Tree.Descendant.getNamedDescendantsAndSelfElements(root, "INPUT");

		for (Element input : inputElements) {
			HTMLFormInput formInput = new HTMLFormInput(input);
			if (this.method!=null  
					 && this.method.equals(FormMethod.POST)  
//					 && formInput.getType()==InputType.SUBMIT MUST NOT BE TRUE (THINK JAVASCRIPT()
					 ) {
				submittable = true;
			}
			inputs.add(formInput);
		}
	}


	public enum InputType {TEXT, PASSWORD, CHECKBOX, RADIO, SUBMIT, IMAGE, RESET, BUTTON, HIDDEN, FILE};
	enum FormMethod {GET, POST};

	/**
	 * HTMLFormInput
	 * 
	 */
	public class HTMLFormInput {
		
		protected InputType type;
		protected String name;
		protected String value;

		/* for radio & checkbox inputs */
		protected List<String> radioOptions;
		protected int selected;

		/**
		 * Constructor.
		 * 
		 * @param type
		 * @param name
		 * @param radioOptions
		 */
		public HTMLFormInput(InputType type, String name, List<String> radioOptions) {
			this(type,name,"");
			this.radioOptions = radioOptions;
		}

		/**
		 * Constructor.
		 * 
		 * @param type
		 * @param name
		 * @param value
		 */
		public HTMLFormInput(InputType type, String name, String value) {
			this.type = type;
			this.name = name;
			this.value = value;
		}

		/**
		 * Constructor.
		 * 
		 * @param input
		 */
		public HTMLFormInput(Element input) {

			if (input.getTagName().toUpperCase().equals("INPUT")) {

				NamedNodeMap nnm = input.getAttributes();
				for (int i=0; i<nnm.getLength(); i++) {
					Node a = nnm.item(i);	
					String attName = a.getNodeName();
					if (attName.equalsIgnoreCase("type")) {
						if (a.getNodeValue().equalsIgnoreCase("text")) {
							this.type = InputType.TEXT;
						} else if (a.getNodeValue().equalsIgnoreCase("password")) {
							this.type = InputType.PASSWORD;
						} else if (a.getNodeValue().equalsIgnoreCase("checkbox")) {
							this.type = InputType.CHECKBOX;
						} else if (a.getNodeValue().equalsIgnoreCase("radio")) {
							this.type = InputType.RADIO;
						} else if (a.getNodeValue().equalsIgnoreCase("submit")) {
							this.type = InputType.SUBMIT;
						} else if (a.getNodeValue().equalsIgnoreCase("image")) {
							this.type = InputType.IMAGE;
						} else if (a.getNodeValue().equalsIgnoreCase("reset")) {
							this.type = InputType.RADIO;
						} else if (a.getNodeValue().equalsIgnoreCase("button")) {
							this.type = InputType.BUTTON;
						} else if (a.getNodeValue().equalsIgnoreCase("hidden")) {
							this.type = InputType.HIDDEN;
						} else if (a.getNodeValue().equalsIgnoreCase("file")) {
							this.type = InputType.FILE;
						} 
					} else if (attName.equalsIgnoreCase("name")) {
						this.name = a.getNodeValue().toLowerCase();
					} else if (attName.equalsIgnoreCase("value")) {
						this.value = a.getNodeValue();
					}
				} 
			} else {
				List<Element> inputElements = 
					DOMHelper.Tree.Descendant.getNamedDescendantsAndSelfElements(input, "INPUT");
				List<String> radioOptions = new LinkedList<String>();
				for (Element radioIn : inputElements) {
					String inType = radioIn.getAttribute("type");
					if (inType.equalsIgnoreCase("radio") ||
							inType.equalsIgnoreCase("checkbox")) {
						radioOptions.add(radioIn.getTextContent());
					}
				}
				this.radioOptions = radioOptions;
				Node radioRoot = DOMHelper2.getClosestNonSiblingAncestor(input);
				this.name = radioRoot.getTextContent();
			}

		}

		public String getName() {
			return name;
		}

		public InputType getType() {
			return type;
		}

		public String getValue() {
			return value;
		}

		public List<String> getRadioOptions() {
			return radioOptions;
		}

	}//HTMLInputForm

	public boolean isSubmittable() {
		return submittable;
	}

	public FormMethod getMethod() {
		return method;
	}

	public String getName() {
		return name;
	}

	public URL getUrl() {
		return url;
	}

	public String getAction() {
		return action;
	}

	public List<HTMLFormInput> getInputs() {
		return inputs;
	}

	public void setRoot(Element root) {
		this.root = root;
	}

	public Element getRoot() {
		return root;
	}


}//HTMLForm