package at.tuwien.prip.core.model.agent.states;

import at.tuwien.prip.core.model.agent.IAgent;
import at.tuwien.prip.core.model.agent.attributes.AgentStateAttribute;


public interface IAgentState 
{

	public void setBlocked(boolean blocked);
	
	public void setBlockedBy(IAgent agent);
	
	public boolean isBlocked();
	
	public IAgent getOwner();

	public int getStateLevel();

	public boolean isReconsider();

	public void setDirty(boolean b);

	public void setUtility(double utility);
	
	public float getConfidence();
	
//	public List<IAgent> getAcceptors();

	public boolean needsProcessing(IAgent agent);

//	public List<IAgent> getProcessedBy();

	public void addAttribute(AgentStateAttribute attribute);

}
