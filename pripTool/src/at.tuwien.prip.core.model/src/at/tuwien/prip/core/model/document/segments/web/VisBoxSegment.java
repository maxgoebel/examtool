package at.tuwien.prip.core.model.document.segments.web;

import at.tuwien.prip.core.model.document.segments.GenericSegment;

/**
 * VisBoxSegment.java
 *
 *
 * @author mcg <mcgoebel@gmail.com>
 * @date Jul 4, 2011
 */
public class VisBoxSegment extends GenericSegment 
{

	/**
	 * Constructor.
	 * 
	 * @param x1
	 * @param x2
	 * @param y1
	 * @param y2
	 */
	public VisBoxSegment (
			float x1,
			float x2,
			float y1,
			float y2
	)
	{
		super(x1, x2, y1, y2);
	}
	
}
