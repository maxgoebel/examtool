package at.tuwien.prip.core.model.task.actions;

import java.net.URI;

/**
 * GoToTaskAction.java
 *
 *
 * 
 * @author: Max Goebel <mcgoebel@gmail.com>
 * @date: Feb 17, 2011
 */
public class GoToTaskAction extends TaskAction {

	private URI uri;
	
	public URI getUri() {
		return uri;
	}
	public void setUri(URI uri) {
		this.uri = uri;
	}
	@Override
	public boolean execute() {
		// TODO Auto-generated method stub
		return false;
	}

}
