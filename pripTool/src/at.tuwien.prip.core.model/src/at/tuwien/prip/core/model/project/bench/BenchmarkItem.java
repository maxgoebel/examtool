package at.tuwien.prip.core.model.project.bench;

import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import at.tuwien.prip.core.model.project.annotation.Annotation;
import at.tuwien.prip.core.model.project.document.AbstractDocument;
import at.tuwien.prip.core.model.project.document.DocumentFormat;

/**
 * 
 * @author max
 *
 */
@Entity
public class BenchmarkItem extends AbstractDocument
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 6488924500835941107L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@OneToMany
	protected List<Annotation> annotations;
	
	
	/**
	 * Constructor.
	 */
	public BenchmarkItem() 
	{
		this.annotations = new ArrayList<Annotation>();
	}
	
	public List<Annotation> getAnnotations() {
		return annotations;
	}
	
	public void setAnnotations(List<Annotation> annotations) {
		this.annotations = annotations;
	}
		
	@Override
	public String toString() {
		return name;
	}
	
	@Override
	public String getUri() {
		return uri;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public void setName(String name) {
		this.name = name;		
	}

	@Override
	public DocumentFormat getFormat() {
		return format;
	}

	@Override
	public void setFormat(DocumentFormat format) {
		this.format = format;
	}

	@Override
	public double getScale() {
		return scale;
	}

	@Override
	public void setScale(double scale) {
		this.scale = scale;
	}

	@Override
	public Rectangle getBounds() {
		return bounds;
	}

}
