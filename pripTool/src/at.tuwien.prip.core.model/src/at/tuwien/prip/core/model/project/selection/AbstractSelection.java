package at.tuwien.prip.core.model.project.selection;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * AbstractSelection.java
 * 
 *
 *
 * @author mcg <mcgoebel@gmail.com>
 * Nov 12, 2012
 */
@Entity
public class AbstractSelection
{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	protected Long id;

	protected int localId;
	
	public AbstractSelection() {}
	
	public int getId() {
		return localId;
	}

	public void setId(int localId) {
		this.localId = localId;
	}
}
