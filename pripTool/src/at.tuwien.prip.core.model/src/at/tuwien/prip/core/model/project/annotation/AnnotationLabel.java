package at.tuwien.prip.core.model.project.annotation;

/**
 * AnnotationLabel.java
 * 
 *
 *
 * @author mcg <mcgoebel@gmail.com>
 * Nov 12, 2012
 */
public enum AnnotationLabel 
{
	IMAGE(0), 
	HEADING(1), 
	BOILERPLATE(2), 
	LIST(3), REGION(4), 
	SECTION(5), 
	SEMANTIC(6), 
	TABLE(7), 
	KEY_VALUE(8);

	private int code;
	private AnnotationLabel(int code) {

		this.code = code;
	}
	public int getCode() {
		return code;
	}
	public void setCode(int code) {
		this.code = code;
	}
	public static AnnotationLabel valueOf(int i){
		for (AnnotationLabel s : values()){
			if (s.code == i){
				return s;
			}
		}
		throw new IllegalArgumentException("No matching constant for " + i);
	}
}
