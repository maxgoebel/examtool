package at.tuwien.prip.core.model.graph;

import at.tuwien.prip.core.model.document.segments.GenericSegment;

public class Pixel extends GenericSegment {

	private int val = -1;
	
	/**
	 * 
	 * @param x
	 * @param y
	 */
	public Pixel(int x, int y) {
		super(x, y, 1, 1);
	}
	
	/**
	 * 
	 * @param x
	 * @param y
	 */
	public Pixel(int x, int y, int val) 
	{
		super(x, y, 1, 1);
		this.val = val;
	}
	
	public int getVal() {
		return val;
	}
	
	public void setVal(int val) {
		this.val = val;
	}
}
