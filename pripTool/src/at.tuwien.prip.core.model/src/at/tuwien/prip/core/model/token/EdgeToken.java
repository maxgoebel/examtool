package at.tuwien.prip.core.model.token;

import at.tuwien.prip.core.model.graph.base.BaseEdge;

/**
 * DocEdgeToken.java
 *
 *
 * 
 * @author: Max Goebel <mcgoebel@gmail.com>
 * @date: Apr 2, 2011
 */
public class EdgeToken extends GraphToken 
{

	private BaseEdge<?> edge;
	
	public EdgeToken(BaseEdge<?> edge) 
	{
		super(edge.toString());
		this.edge = edge;
	}

	public BaseEdge<?> getEdge() 
	{
		return edge;
	}
	
	@Override
	public boolean equals(Object obj) 
	{
		if (!(obj instanceof EdgeToken)) {
			return false;
		} 

		EdgeToken other = (EdgeToken) obj;
		return this.getEdge().getRelation().equals(other.getEdge().getRelation());
	}
}
