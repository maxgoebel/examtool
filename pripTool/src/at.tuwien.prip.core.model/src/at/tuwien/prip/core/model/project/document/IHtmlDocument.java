package at.tuwien.prip.core.model.project.document;

import org.w3c.dom.Document;

public interface IHtmlDocument extends IDocument
{

	Document getCachedJavaDOM();

}
