package at.tuwien.prip.core.model.project.selection;

import javax.persistence.Entity;

/**
 * TextSelection.java
 * 
 *
 *
 * @author mcg <mcgoebel@gmail.com>
 * Nov 12, 2012
 */
@Entity
public class TextSelection extends AbstractSelection 
{
	protected String text;
	
	public void setText(String text) {
		this.text = text;
	}
	
	public String getText() {
		return text;
	}
}
