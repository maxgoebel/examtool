package at.tuwien.prip.core.model.agent.attributes;

public enum AttributeType {

	HEADING, SALIENT, SEMANTIC, DOMAIN_KEY, DOMAIN_VAL, TOC_LEVEL,
	ROW_MODEL, COL_MODEL, TABEL_MODEL	
}
