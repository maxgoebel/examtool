package at.tuwien.prip.core.model.graph.agent;

import at.tuwien.prip.core.model.agent.IAgent;
import at.tuwien.prip.core.model.agent.relations.AlignmentType;
import at.tuwien.prip.core.model.agent.states.AgentState;
import at.tuwien.prip.core.model.document.layout.Direction;

/**
 * 
 * AlignmentAgentStateEdge.java
 * 
 *
 *
 * @author mcg <mcgoebel@gmail.com>
 * Jul 16, 2012
 */
public class LayoutAlignment
{

	private AlignmentType type;
	 
	private Direction direction;
	
	private IAgent owner;
	
	private AgentState from;
	
	private AgentState to;
	
	/**
	 * Constructor.
	 * @param relation
	 * @param type
	 * @param dir
	 * @param owner
	 */
	public LayoutAlignment(
			AlignmentType type, 
			Direction dir, 
			AgentState from, 
			AgentState to,
			IAgent owner) 
	{
		this.type = type;
		this.direction = dir;
		this.owner = owner;
		this.from = from;
		this.to = to;	
	}

	public AlignmentType getType() {
		return type;
	}
	
	public Direction getDirection() {
		return direction;
	}
	
	public boolean isVertical () 
	{
		if (getType().toString().startsWith("VERT"))
		{
			return true;
		}
		return false;
	}
	
	public boolean isHorizontal () 
	{
		if (getType().toString().startsWith("HORIZ"))
		{
			return true;
		}
		return false;
	}

	public double getLength() {
		return from.getBounds().getLocation().distance(to.getBounds().getLocation());
	}
	
	public AgentState getFrom() {
		return from;
	}
	
	public AgentState getTo() {
		return to;
	}	
	
	public IAgent getOwner() {
		return owner;
	}
	
}
